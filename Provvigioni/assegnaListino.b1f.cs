
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace Provvigioni
{

    [FormAttribute("Provvigioni.assegnaListino_b1f", "assegnaListino.b1f")]
    class assegnaListino_b1f : UserFormBase
    {
        public assegnaListino_b1f()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.Button2 = ((SAPbouiCOM.Button)(this.GetItem("Item_2").Specific));
            this.Button2.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button2_ClickAfter);
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
            this.ActivateAfter += new ActivateAfterHandler(this.Form_ActivateAfter);

        }

        private SAPbouiCOM.StaticText StaticText0;

        private void OnCustomInitialize()
        {
            this.oApp = (SAPbouiCOM.Application)Application.SBO_Application;
            //this.oForm = (SAPbouiCOM.Form)oApp.Forms.ActiveForm;this.UIAPIRawForm.
            this.oForm = this.UIAPIRawForm;
            this.oRecordSet = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.oRecordSetList = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset))); 
            this.oRecordSetDel = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.StaticText0 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_0").Specific));
            this.ComboBox0 = ((SAPbouiCOM.ComboBox)(this.GetItem("Item_1").Specific));
            this.Button0 = ((SAPbouiCOM.Button)(this.GetItem("Item_2").Specific));
            this.StaticText1 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_3").Specific));
            this.EditText0 = ((SAPbouiCOM.EditText)(this.GetItem("Item_4").Specific));
            this.Button0.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.Button1_ClickBefore);
            
            //POPOLO DDL LISTINO
            //ComboBox0.ValidValues.Add("Scegli Listino", "Scegli Listino");
            String qListini = "SELECT * FROM \"OPLN\" order by \"ListNum\" DESC ";
            oRecordSet.DoQuery(qListini);
            while (!oRecordSet.EoF)
            {
                ComboBox0.ValidValues.Add(oRecordSet.Fields.Item("ListNum").Value.ToString(), oRecordSet.Fields.Item("ListName").Value.ToString());
                oRecordSet.MoveNext();
            }

            //ComboBox0.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
            //ComboBox0.Select("Scegli Listino", SAPbouiCOM.BoSearchKey.psk_ByValue);

            //assegno listino attuale
            aggiornaListinoTxt();

        }

        //ASSEGNO LISTINO
        private void Button1_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            String ListNum = this.ComboBox0.Value.Trim();
            String qSelLs = "SELECT * FROM \"OPLN\" WHERE \"ListNum\" =  \'" + ListNum + "\'";
            oRecordSetDel.DoQuery(qSelLs);
            
                String ListName = oRecordSetDel.Fields.Item("ListName").Value.ToString();
            

                try
                {
                    //UPDATO
                    String qUp = "UPDATE \"@FC_PRLS\" SET \"U_ListNum\" = \'" + ListNum + "\' , \"U_ListName\" = \'" + ListName + "\' WHERE \"Name\" = '1' ";
                    oRecordSetDel.DoQuery(qUp);
                }
                catch (System.Runtime.InteropServices.COMException e)
                {
                }
                try
                {
                    //INSERISCO
                    String qIns = "INSERT INTO \"@FC_PRLS\" (\"Code\",\"Name\",\"U_ListNum\",\"U_ListName\") VALUES ('1','1',\'" + ListNum + "\' ,\'" + ListName + "\')";
                    oRecordSetDel.DoQuery(qIns);
                }
                catch (System.Runtime.InteropServices.COMException e)
                {
                }

            
        }
        //AGGIORNO TEXT
        private void Form_ActivateAfter(SAPbouiCOM.SBOItemEventArg pVal)
        {
            aggiornaListinoTxt();
        }
        //FUNZIONA AGGIORNA TESTO
        public void aggiornaListinoTxt()
        {
            String qSel = "SELECT \"U_ListName\" FROM \"@FC_PRLS\" ";
            oRecordSetList.DoQuery(qSel);
            String listinoAttuale = oRecordSetList.Fields.Item("U_ListName").Value.ToString();
            EditText0.Value = listinoAttuale;
        }
        //DICHIARAZIZONI SISTEMA
        public SAPbouiCOM.Application oApp;
        public SAPbouiCOM.IForm oForm;
        public SAPbobsCOM.Company oCompany = Program.oCompany;
        public SAPbobsCOM.Recordset oRecordSet;
        public SAPbobsCOM.Recordset oRecordSetList;
        public SAPbobsCOM.Recordset oRecordSetDel;

        //layout
        private SAPbouiCOM.ComboBox ComboBox0;
        private SAPbouiCOM.Button Button0;
        private SAPbouiCOM.Button Button1;
        private SAPbouiCOM.StaticText StaticText1;
        private SAPbouiCOM.EditText EditText0;
        private SAPbouiCOM.Button Button2;

        private void Button2_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            aggiornaListinoTxt();

        }

        

        
    }
}
