﻿using System;
using System.Collections.Generic;
using SAPbouiCOM.Framework;

namespace Provvigioni
{
    
    class Program
    {
        public static SAPbobsCOM.Company oCompany;

        public static string STR_datiMancanti;
        public static int numeroRegole;
        public static int tipo_DB;

        //DATABASE
        public static int HANA_DB = 0;
        public static int SQL_DB = 1;
        public static String HANA_limit1 = "   ";
        public static String SQL_limit1 = " TOP 1 ";

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                Application oApp = null;
                if (args.Length < 1)
                {
                    oApp = new Application();
                }
                else
                {
                    oApp = new Application(args[0]);
                }
                
                //TIPO DI DATABASE
                //HANA = 0
                //SQL = 1;
                tipo_DB = 1;

                //INIZIALIZZO MENU
                Menu MyMenu = new Menu();
                MyMenu.AddMenuItems();

                //COMPANY
                oCompany = (SAPbobsCOM.Company)Application.SBO_Application.Company.GetDICompany();
                oApp.RegisterMenuEventHandler(MyMenu.SBO_Application_MenuEvent);
                Application.SBO_Application.AppEvent += new SAPbouiCOM._IApplicationEvents_AppEventEventHandler(SBO_Application_AppEvent);
                oApp.Run();

                //NUMERO REGOLE
                numeroRegole = 4;

                //STRINGHE
                STR_datiMancanti = "Attenzione: inserire tutti i campi obbligatori";

     
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }

        static void SBO_Application_AppEvent(SAPbouiCOM.BoAppEventTypes EventType)
        {
            switch (EventType)
            {
                case SAPbouiCOM.BoAppEventTypes.aet_ShutDown:
                    //Exit Add-On
                    System.Windows.Forms.Application.Exit();
                    break;
                case SAPbouiCOM.BoAppEventTypes.aet_CompanyChanged:
                    break;
                case SAPbouiCOM.BoAppEventTypes.aet_FontChanged:
                    break;
                case SAPbouiCOM.BoAppEventTypes.aet_LanguageChanged:
                    break;
                case SAPbouiCOM.BoAppEventTypes.aet_ServerTerminition:
                    break;
                default:
                    break;
            }
        }
    }
}
