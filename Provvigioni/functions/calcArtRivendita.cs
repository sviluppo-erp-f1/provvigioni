﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Provvigioni
{
    class calcArtRivendita
    {
        //dati
        string _addVendite;
        string _tipoArt;
        string _dataDoc;
        double _importo;
        double _qty;
        string _docNum;
        string _prefix;

        //valori iniziali
        private double calcolatoProvvigione = 0;
        private double costoProvvigione;

        public SAPbouiCOM.Application _oApp;

        //variabili SAP
        public SAPbobsCOM.Company oCompany = Program.oCompany;
        public SAPbobsCOM.Recordset oRecordSetGenerale;

        public calcArtRivendita(SAPbouiCOM.Application oApp, string addVendite, string tipoArt, double importo, double qty, String dataDoc, string docNum)
        {
            _addVendite = addVendite.Trim();
            _oApp = oApp;
            _tipoArt = tipoArt;
            _importo = importo;
            _qty = qty;
            _docNum = docNum;
            _dataDoc = dataDoc;
            _prefix = "percTipoItem_%";
        }

        public double calcoloProvvigione()
        {
            //SAP TODAY
            string dataConfronto = DateTime.Now.ToString("yyyy").ToString() + DateTime.Now.ToString("MM").ToString() + DateTime.Now.ToString("dd").ToString();
            String dt_inizio = _dataDoc;
            String annoIni = dt_inizio.Substring(6, 4);
            String meseIni = dt_inizio.Substring(3, 2);
            String giornoIni = dt_inizio.Substring(0, 2);
            String annoIni_db = annoIni + meseIni + giornoIni;
            oRecordSetGenerale = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));


            if (_tipoArt.Equals("B"))
            {
                _tipoArt = "Acquisto";
            }
            else
            {
                _tipoArt = "Produzione";
            }

            String qControlArticolo = "select " + Program.SQL_limit1 + " * from \"@FC_ANAG\" where \"U_dtINI\"<= \'" + annoIni_db + "\' AND \"U_dtEND\">= \'" + annoIni_db + "\' AND \"U_SlpCode\" =  \'" + _addVendite + "\' AND \"U_TipoArt\" =  \'" + _tipoArt + "\' AND \"U_Code\" LIKE \'" + _prefix + "\'  order by \"U_dtEND\" desc " + Program.HANA_limit1 + " ";

            oRecordSetGenerale.DoQuery(qControlArticolo);

            //CONTROLLO SE ESISTE PERCENT PER PROVVIGIONE DEL CASO
            if (!oRecordSetGenerale.Fields.Item("U_percent").Value.ToString().Equals(""))
            {
                try
                {

                    calcolatoProvvigione = double.Parse((oRecordSetGenerale.Fields.Item("U_percent").Value.ToString()).Replace(".", ","));
                    //CALCOLO PROVVISORIO IN BASE ALLA PERCENT
                    costoProvvigione = ((calcolatoProvvigione) * _importo) / 100;

                    //MOLTIPLICO PER LE QUANTITA
                    costoProvvigione = costoProvvigione * _qty;

                }
                catch (InvalidCastException e)
                {
                   
                }
            }
            else
            {
                costoProvvigione = -999999;
            }

            return costoProvvigione;
        }

        public double returnPercent()
        {
            return calcolatoProvvigione;
        }

    }
}
