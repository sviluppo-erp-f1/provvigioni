﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Provvigioni
{
   
    class calcoloProvv
    {
        //DIZIONARIO ORDINE REGOLE
        string[] ordineRegole = new string[4];
        String _calcoloProvv;
        String _tipo;

        //DATI FATTURA
        String addVendite;
        String ItemCode;
        String dataDoc;
        String docNum;
        String docEntry;
        String CardCode;
        String CardName;
        String LineNum;
        String ItemName;
        String SlpName;
        String qFt;
        String nomeRegola;
        String SettoreCode;
        String GrpCode;
        String GrpOmog;
        String tipoItem;
        String businessUnit;
        String BpCode;
        double totImporto;
        double totArtQty;

        double percentDec = 0;
        double costoProvvigione = 0;
        double costoFinaleProvvigione = 0;

        public calcoloProvv(String calcoloProvv,String tipo)
        {
            _calcoloProvv = calcoloProvv;
            _tipo = tipo;
        }

        public double provvigioneCalcolata(String dtIni, String dtEnd , int newNumEla, double rapporto)
        {
            //ORDINE REGOLE PARAMS
            /*
            NOMI REGOLE
            AgenteStandard
            AgenteArticolo
            AgenteCliente
            AgenteArticoloCliente
            AgenteSettore
            AgenteTipoArt
            AgenteGruppoArt
            AgenteGruppoArtCliente
            */
            //int incrRegole = 0;
            //oRecordSetNRegole = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            //String qRegole = "SELECT * FROM \"@FC_PROR\" WHERE \"U_Attiva\" = '1' ORDER BY \"U_Ordine\" ASC";
            //oRecordSetNRegole.DoQuery(qRegole);
            //while (!oRecordSetNRegole.EoF)
            //{
            //    /**** ORDINE CALCOLO PROVVIGIONE ****/
            //    nomeRegola = oRecordSetNRegole.Fields.Item("U_Nome").Value.ToString();
            //    ordineRegole[incrRegole] = nomeRegola;

            //    //NEXT
            //    incrRegole++;
            //    oRecordSetNRegole.MoveNext();
            //}

           

            //DICHIARO TABELLA inserimento riga
            SAPbobsCOM.UserTable oUserTableANAGAA;
            oUserTableANAGAA = oCompany.UserTables.Item("FC_ANRP");

            oRecordSet = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            oRecordSetGenerale = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));


            qFt = " SELECT ";
            qFt += " T0.\"CardCode\" as cliente, ";
            qFt += " T0.\"CardName\" as nome_cliente, ";
            qFt += " T0.\"DocDate\" as data_ft, ";
            qFt += " T0.\"DocNum\" as n_ft, ";
            qFt += " T1.\"DocEntry\" as docentryft, ";
            qFt += " T1.\"LineNum\" as LineNum, ";
            qFt += " T5.\"ItemName\" as nome_art, ";
            qFt += " T0.\"SlpCode\", ";
            qFt += " T4.\"SlpName\", ";
            qFt += " T4.\"U_BpCode\", ";
            qFt += " T0.\"DocDate\", ";
            //qFt += " T2.\"ItmsGrpCod\", ";
            qFt += " T2.\"U_AG_GRPOMOG\", ";
            qFt += " T2.\"PrcrmntMtd\", ";
            qFt += " T2.\"U_AG_OcrCode\", ";
            qFt += " T3.\"IndustryC\", ";
            qFt += " T1.* ";
            if(_tipo.Equals("Ft")){
                qFt += " FROM \"OINV\" T0 ";
                qFt += " INNER JOIN \"INV1\" T1 ON T1.\"DocEntry\" = T0.\"DocEntry\"  ";
            }else{
                qFt += " FROM \"ORIN\" T0 ";
                qFt += " INNER JOIN \"RIN1\" T1 ON T1.\"DocEntry\" = T0.\"DocEntry\"  ";
            }
            qFt += " INNER JOIN \"OITM\" T2 ON T1.\"ItemCode\" = T2.\"ItemCode\" ";
            qFt += " INNER JOIN \"OCRD\" T3 ON T3.\"CardCode\" = T0.\"CardCode\" ";
            qFt += " INNER JOIN \"OSLP\" T4 ON T4.\"SlpCode\" = T0.\"SlpCode\" ";
            qFt += " INNER JOIN \"OITM\" T5 ON T5.\"ItemCode\" = T1.\"ItemCode\" ";
            qFt += " WHERE  ";
            qFt += " T0.\"DocNum\" = \'" + _calcoloProvv + "\' ";
            
            oRecordSet.DoQuery(qFt);

            int incr = 0;

            while(!oRecordSet.EoF){

                costoProvvigione = 0;

                dataDoc = oRecordSet.Fields.Item("data_ft").Value.ToString();
                docNum = oRecordSet.Fields.Item("n_ft").Value.ToString();
                docEntry = oRecordSet.Fields.Item("docentryft").Value.ToString();
                addVendite = oRecordSet.Fields.Item("SlpCode").Value.ToString();
                ItemCode = oRecordSet.Fields.Item("ItemCode").Value.ToString();
                ItemName = oRecordSet.Fields.Item("nome_art").Value.ToString();
                SlpName = oRecordSet.Fields.Item("SlpName").Value.ToString();
                CardCode = oRecordSet.Fields.Item("cliente").Value.ToString();
                CardName = oRecordSet.Fields.Item("nome_cliente").Value.ToString();
                LineNum = oRecordSet.Fields.Item("LineNum").Value.ToString();
                totImporto = double.Parse((oRecordSet.Fields.Item("Price").Value.ToString()).Replace(".", ","));
                totArtQty = double.Parse((oRecordSet.Fields.Item("Quantity").Value.ToString()).Replace(".", ","));
                SettoreCode = oRecordSet.Fields.Item("IndustryC").Value.ToString();
                //GrpCode = oRecordSet.Fields.Item("ItmsGrpCod").Value.ToString();
                GrpOmog = oRecordSet.Fields.Item("U_AG_GRPOMOG").Value.ToString();
                tipoItem = oRecordSet.Fields.Item("PrcrmntMtd").Value.ToString();
                businessUnit = oRecordSet.Fields.Item("U_AG_OcrCode").Value.ToString();
                BpCode = oRecordSet.Fields.Item("U_BpCode").Value.ToString();

                String regolaDom = "";

                String dt_inizio = dataDoc;
                String annoIni = dt_inizio.Substring(6, 4);
                String meseIni = dt_inizio.Substring(3, 2);
                String giornoIni = dt_inizio.Substring(0, 2);
                String annoIni_db = annoIni + meseIni + giornoIni;

                //percentDec = calcoloDec(docNum, annoIni_db);

                //A MONTE CONTROLLO SE ARTICOLO ESCLUSO
                String qEx = "SELECT \"U_startString\" FROM \"@FC_PRIT\" ";
                oRecordSetGenerale.DoQuery(qEx);
                int calcoloEx = 0;
                String artEx = "";
                while (!oRecordSetGenerale.EoF)
                {
                    artEx = oRecordSetGenerale.Fields.Item("U_startString").Value.ToString();
                    int artLenght = artEx.Length;
                    int itemCodeLenght = ItemCode.Length;
                    if (itemCodeLenght >= artLenght)
                    {
                        if (artEx.Equals(ItemCode.Substring(0, artLenght)))
                        {
                            calcoloEx = 1;
                        }
                    }

                    oRecordSetGenerale.MoveNext();
                }
                double percentProvv = 0;
                if (calcoloEx == 0)
                {
    
                    //CICLO L'ORDINE DELLE REGOLE
                    //for (int reg = 0; reg < ordineRegole.Length;reg++ )
                   // {
                        //SAPbouiCOM.Framework.Application.SBO_Application.MessageBox(ordineRegole[reg].ToString() , 1, "Ok", "", "");
                        
                    //}

                    //utilizzo questo per calcolare solo dall'ultima regole ed ottimizzare
                    int regresso_provv = 0;

                    //REGOLA calcAgenteBusinessUnit
                    if(regresso_provv == 0){
                        calcAgenteBusinessUnit calcAgBUnit = new calcAgenteBusinessUnit(oApp, addVendite, businessUnit, totImporto, totArtQty, dataDoc, docNum);
                        double percentAgBUnit = calcAgBUnit.calcoloProvvigione();
                        if (percentAgBUnit > -99999)
                        {
                            percentProvv = calcAgBUnit.returnPercent();
                            costoProvvigione = percentAgBUnit;
                            regresso_provv = 1;
                            regolaDom = "percAgBU";
                        }
                    }
                    //REGOLA calcAgenteClArt
                    if(regresso_provv == 0){
                        calcAgenteClienteArticolo calcAgClArt = new calcAgenteClienteArticolo(oApp, addVendite, ItemCode, CardCode, totImporto, totArtQty, dataDoc, docNum, percentDec);
                        double percentAgClArt = calcAgClArt.calcoloProvvigione();
                        if (percentAgClArt > -99999)
                        {
                            percentProvv = calcAgClArt.returnPercent();
                            costoProvvigione = percentAgClArt;
                            regresso_provv = 1;
                            regolaDom = "percAgClArt";
                        }
                    }
                    //REGOLA calcAgenteGrpArtCliente
                    /*if (regresso_provv == 0)
                    {
                        calcAgenteGrpArtCliente calcAgGrpArtCl = new calcAgenteGrpArtCliente(oApp, addVendite, CardCode, GrpCode, totImporto, totArtQty, dataDoc, docNum);
                        double percentAgGrpArtCl = calcAgGrpArtCl.calcoloProvvigione();
                        if (percentAgGrpArtCl > -99999)
                        {
                            costoProvvigione = percentAgGrpArtCl;
                            regresso_provv = 1;
                        }
                    }*/

                    //REGOLA calcGrpOmogCliente
                    if (regresso_provv == 0)
                    {
                        calcGrpOmogCliente calcGrpOmogCl = new calcGrpOmogCliente(oApp, addVendite, CardCode, GrpOmog, totImporto, totArtQty, dataDoc, docNum);
                        double percentAgGrpArtCl = calcGrpOmogCl.calcoloProvvigione();
                        if (percentAgGrpArtCl > -99999)
                        {
                            percentProvv = calcGrpOmogCl.returnPercent();
                            costoProvvigione = percentAgGrpArtCl;
                            regresso_provv = 1;
                            regolaDom = "percGrpOmogCl";
                        }
                    }

                    //REGOLA calcAgenteCl
                    if (regresso_provv == 0)
                    {
                        calcAgenteCliente calcAgCliente = new calcAgenteCliente(oApp, addVendite, CardCode, ItemCode, totImporto, totArtQty, dataDoc, docNum, percentDec);
                        double percentAgCl = calcAgCliente.calcoloProvvigione();
                        if (percentAgCl > -99999)
                        {
                            percentProvv = calcAgCliente.returnPercent();
                            costoProvvigione = percentAgCl;
                            regresso_provv = 1;
                            regolaDom = "percAgCl";
                        }
                    }
                    //REGOLA calcAGItem
                    if (regresso_provv == 0)
                    {
                        calcAgenteArticolo calcAgArt = new calcAgenteArticolo(oApp, addVendite, ItemCode, totImporto, totArtQty, dataDoc, docNum, percentDec);
                        double percentAgArt = calcAgArt.calcoloProvvigione();
                        if (percentAgArt > -99999)
                        {
                            percentProvv = calcAgArt.returnPercent();
                            costoProvvigione = percentAgArt;
                            regresso_provv = 1;
                            regolaDom = "percAgItem";
                        }
                    }
                    //REGOLA calcGruppopArticoli
                    /*if (regresso_provv == 0)
                    {
                        calcGruppoArticoli calcGrpArt = new calcGruppoArticoli(oApp, addVendite, GrpCode, totImporto, totArtQty, dataDoc, docNum);
                        double percentGrpArt = calcGrpArt.calcoloProvvigione();
                        if (percentGrpArt > -99999)
                        {
                            costoProvvigione = percentGrpArt;
                            regresso_provv = 1;
                        }
                    }*/
                    //REGOLA calcGruppoArticoli
                    if (regresso_provv == 0)
                    {
                        calcGrpOmogeneo calcGrpOmog = new calcGrpOmogeneo(oApp, addVendite, GrpOmog, totImporto, totArtQty, dataDoc, docNum);
                        double percentGrpArt = calcGrpOmog.calcoloProvvigione();
                        if (percentGrpArt > -99999)
                        {
                            percentProvv = calcGrpOmog.returnPercent();
                            costoProvvigione = percentGrpArt;
                            regresso_provv = 1;
                            regolaDom = "percGrpOmogeneo";
                        }
                    }
                    //REGOLA calcArtRivendita
                    if (regresso_provv == 0)
                    {
                        calcArtRivendita calcArtRiv = new calcArtRivendita(oApp, addVendite, tipoItem, totImporto, totArtQty, dataDoc, docNum);
                        double percentArtRiv = calcArtRiv.calcoloProvvigione();
                        if (percentArtRiv > -99999)
                        {
                            percentProvv = calcArtRiv.returnPercent();
                            costoProvvigione = percentArtRiv;
                            regresso_provv = 1;
                            regolaDom = "percTipoItem";
                        }
                    }
                    //REGOLA calcAgenteSettore
                    if (regresso_provv == 0)
                    {
                        calcAgenteSettore calcAgSec = new calcAgenteSettore(oApp, addVendite, SettoreCode, totImporto, totArtQty, dataDoc, docNum);
                        double percentAgSec = calcAgSec.calcoloProvvigione();
                        if (percentAgSec > -99999)
                        {
                            percentProvv = calcAgSec.returnPercent();
                            costoProvvigione = percentAgSec;
                            regresso_provv = 1;
                            regolaDom = "percSettore";
                        }
                    }
                    //REGOLA calcAgenteStd
                    if (regresso_provv == 0)
                    {
                        calcAgenteStd calcAgStd = new calcAgenteStd(oApp, addVendite, ItemCode, totImporto, totArtQty, dataDoc, docNum, percentDec);
                        double percentAgSt = calcAgStd.calcoloProvvigione();
                        if (percentAgSt > -99999)
                        {
                            percentProvv = calcAgStd.returnPercent();
                            costoProvvigione = percentAgSt;
                            regresso_provv = 1;
                            regolaDom = "percStd";
                        }
                    }
                    // SAPbouiCOM.Framework.Application.SBO_Application.MessageBox(costoProvvigione.ToString()+"XXXX", 1, "Ok", "", "");
                }
                
                costoFinaleProvvigione = costoFinaleProvvigione + costoProvvigione;

                //DATI RIGA ELAB.
                String annoDtIni = dtIni.Substring(0, 4);
                String meseDtIni = dtIni.Substring(4, 2);
                String giornoDtIni = dtIni.Substring(6, 2);
                String dtIniDB = giornoIni + "/" + meseIni + "/" + annoDtIni;

                String annoDtEnd = dtEnd.Substring(0, 4);
                String meseDtEnd = dtEnd.Substring(4, 2);
                String giornoDtEnd = dtEnd.Substring(6, 2);
                String dtEndDB = giornoDtEnd + "/" + meseDtEnd + "/" + annoDtEnd;

                double costoProvvigioneEffettivo = (costoProvvigione * rapporto) / 100;

                if (_tipo.Equals("Nc"))
                {
                    costoProvvigione = costoProvvigione * -1;
                }
                if (calcoloEx == 0)
                {
                    //INSERISCO RIGA ELABORAZIONE
                    oUserTableANAGAA.Name = "ela_" + incr + "_" + newNumEla;
                    oUserTableANAGAA.UserFields.Fields.Item("U_SlpCode").Value = addVendite;
                    oUserTableANAGAA.UserFields.Fields.Item("U_DocNum").Value = docNum;
                    oUserTableANAGAA.UserFields.Fields.Item("U_DocEntry").Value = docEntry;
                    oUserTableANAGAA.UserFields.Fields.Item("U_TIPO").Value = _tipo;
                    oUserTableANAGAA.UserFields.Fields.Item("U_DocDate").Value = dataDoc;
                    oUserTableANAGAA.UserFields.Fields.Item("U_ItemCode").Value = ItemCode;
                    oUserTableANAGAA.UserFields.Fields.Item("U_LineNum").Value = LineNum;
                    oUserTableANAGAA.UserFields.Fields.Item("U_TOT_RIGA").Value = Math.Round(totImporto * totArtQty, 2);
                    oUserTableANAGAA.UserFields.Fields.Item("U_TOT_IMP_PROVV").Value = Math.Round(costoProvvigioneEffettivo, 2);
                    oUserTableANAGAA.UserFields.Fields.Item("U_PROVV_GLOBALE_RIGA").Value = Math.Round(costoProvvigione, 2);
                    oUserTableANAGAA.UserFields.Fields.Item("U_NUM_ELA").Value = newNumEla;
                    oUserTableANAGAA.UserFields.Fields.Item("U_DATA_ELA").Value = DateTime.Now.ToString("dd/MM/yyyy");
                    oUserTableANAGAA.UserFields.Fields.Item("U_DATA_INI").Value = dtIniDB;
                    oUserTableANAGAA.UserFields.Fields.Item("U_DATA_END").Value = dtEndDB;
                    oUserTableANAGAA.UserFields.Fields.Item("U_PERCENT").Value = percentProvv;

                    oUserTableANAGAA.UserFields.Fields.Item("U_SlpName").Value = SlpName;
                    oUserTableANAGAA.UserFields.Fields.Item("U_CardCode").Value = CardCode;
                    oUserTableANAGAA.UserFields.Fields.Item("U_CardName").Value = CardName;
                    oUserTableANAGAA.UserFields.Fields.Item("U_ItemName").Value = ItemName;
                    oUserTableANAGAA.UserFields.Fields.Item("U_REGOLA_DOMI").Value = regolaDom;
                    oUserTableANAGAA.UserFields.Fields.Item("U_RAPPORTO").Value = rapporto;
                    oUserTableANAGAA.UserFields.Fields.Item("U_BpCode").Value = BpCode;

                    int result = oUserTableANAGAA.Add();
                }
                incr++;

                oRecordSet.MoveNext();
            }

            if(_tipo.Equals("Nc")){
                costoFinaleProvvigione = costoFinaleProvvigione * -1;
            }
            return costoFinaleProvvigione;
        }
        //CALCOLO DECURTAZIONE
        private double calcoloDec(String docNum, String data)
        {
            double percentDecurt = 0;
            oRecordSetPg = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            oRecordSetFt = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            //DATA PAGAMENTO
            String qPg = "Select orct.\"DocDate\" From \"ORCT\" orct,\"OINV\" oinv where oinv.\"ReceiptNum\"=orct.\"DocNum\" and oinv.\"DocNum\" = \'" + docNum + "\'  AND oinv.\"DocStatus\" = 'C' ";
            oRecordSetPg.DoQuery(qPg);
            /*
            if (!oRecordSetPg.Fields.Item("DocDate").Value.ToString().Equals(""))
            {
                String dt_pag_db = oRecordSetPg.Fields.Item("DocDate").Value.ToString();
                String dt_parse = data.Replace("/1899", "/2099");
                String annoIni = dt_parse.Substring(0, 4);
                String meseIni = dt_parse.Substring(4, 2);
                String giornoIni = dt_parse.Substring(6, 2);
                String annoIni_db = giornoIni + "/" + meseIni + "/" + annoIni;
                DateTime dt_doc = DateTime.Parse(annoIni_db);
                DateTime dt_pag = DateTime.Parse(dt_pag_db);
                double days = (dt_pag - dt_doc).TotalDays;

                //PERCENT DEC
                String qSel = "SELECT * FROM \"@FC_PRDE\" WHERE \"U_inizio\" <=  \'" + days + "\' AND \"U_fine\" >=  \'" + days + "\' ";
                oRecordSetFt.DoQuery(qSel);

                if (double.Parse(oRecordSetFt.Fields.Item("U_incr").Value.ToString()) > 0)
                {
                    percentDecurt = double.Parse(oRecordSetFt.Fields.Item("U_percent").Value.ToString());
                }
            }*/
            return percentDecurt;
        }
        //DICHIARAZIZONI SISTEMA
        public SAPbouiCOM.Application oApp;
        public SAPbobsCOM.Company oCompany = Program.oCompany;
        public SAPbobsCOM.Recordset oRecordSet;
        public SAPbobsCOM.Recordset oRecordSetGenerale;
        public SAPbobsCOM.Recordset oRecordSetPg;
        public SAPbobsCOM.Recordset oRecordSetFt;
        public SAPbobsCOM.Recordset oRecordSetNRegole;
        public SAPbobsCOM.Recordset oRecordSetNumEla;
    }
}
