﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Provvigioni
{
    class calcAgenteClienteArticolo
    {
        //dati
        string _addVendite;
        string _itemCode;
        string _cardCode;
        double _importo;
        double _qty;
        string _docNum;
        string _dataDoc;
        double _percentDecurt;

        //valori iniziali
        private double calcolatoProvvigione = 0;
        private double percentIncr = 0;
        private double percentDecr = 0;
        private double incrDecrRiga = 0;
        private double costoProvvigione;
        private double percentDecurt = 0;
        private double differenzaImporto = 0;

        private Dictionary<string, int>[] _ordineRegole = new Dictionary<string, int>[4];
        public SAPbouiCOM.Application _oApp;

        //variabili SAP
        public SAPbobsCOM.Company oCompany = Program.oCompany;
        public SAPbobsCOM.Recordset oRecordSetGenerale;
        public SAPbobsCOM.Recordset oRecordSetFt;
        public SAPbobsCOM.Recordset oRecordSetPg;


        public calcAgenteClienteArticolo(SAPbouiCOM.Application oApp, string addVendite, string itemCode, string cardCode, double importo, double qty, String dataDoc, string docNum, double percentDec)
        {
            _addVendite = addVendite.Trim();
            _oApp = oApp;
            _cardCode = cardCode;
            _itemCode = itemCode;
            _importo = importo;
            _qty = qty;
            _docNum = docNum;
            _dataDoc = dataDoc; 
            _percentDecurt = percentDec;
        }
        public double calcoloProvvigione()
        {
            //SAP TODAY
            string dataConfronto = DateTime.Now.ToString("yyyy").ToString() + DateTime.Now.ToString("MM").ToString() + DateTime.Now.ToString("dd").ToString();
            String dt_inizio = _dataDoc;
            String annoIni = dt_inizio.Substring(6, 4);
            String meseIni = dt_inizio.Substring(3, 2);
            String giornoIni = dt_inizio.Substring(0, 2);
            String annoIni_db = annoIni + meseIni + giornoIni;
            oRecordSetGenerale = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));

            String qControlArticolo = "select " + Program.SQL_limit1 + " * from \"@FC_ANAG\" where \"U_dtINI\"<= \'" + annoIni_db + "\' AND \"U_dtEND\">= \'" + annoIni_db + "\' AND \"U_SlpCode\" =  \'" + _addVendite + "\' AND \"U_ItemCode\" =  \'" + _itemCode + "\' AND \"U_CardCode\" =  \'" + _cardCode + "\' AND \"U_Code\" LIKE 'percAgClArt_%'  order by \"U_dtEND\" desc " + Program.HANA_limit1 + " ";
            
            oRecordSetGenerale.DoQuery(qControlArticolo);
            //CONTROLLO SE ESISTE PERCENT PER PROVVIGIONE DEL CASO
            if (!oRecordSetGenerale.Fields.Item("U_percent").Value.ToString().Equals(""))
            {
                try
                {

                    calcolatoProvvigione = double.Parse((oRecordSetGenerale.Fields.Item("U_percent").Value.ToString()).Replace(".", ","));
                    if (oRecordSetGenerale.Fields.Item("U_incrPrezzo").Value.ToString().Equals(""))
                    {
                        percentIncr = 0;
                    }
                    else
                    {
                        percentIncr = double.Parse((oRecordSetGenerale.Fields.Item("U_incrPrezzo").Value.ToString()).Replace(".", ","));
                    }
                    if (oRecordSetGenerale.Fields.Item("U_decrPrezzo").Value.ToString().Equals(""))
                    {
                        percentDecr = 0;
                    }
                    else
                    {
                        percentDecr = double.Parse((oRecordSetGenerale.Fields.Item("U_decrPrezzo").Value.ToString()).Replace(".", ","));
                    }
                    if (percentDecr > 0 || percentIncr > 0)
                    {
                        incrDecrRiga = costoIncrDecr(_importo, _qty, percentIncr, percentDecr, annoIni_db, _itemCode);
                    }

                    //CALCOLO PER DECURTAZIONE
                    if (oRecordSetGenerale.Fields.Item("U_decurt").Value.ToString().Equals("si"))
                    {
                        percentDecurt = _percentDecurt;
                    }

                    //CALCOLO PROVVISORIO IN BASE ALLA PERCENT
                    costoProvvigione = ((calcolatoProvvigione + incrDecrRiga - percentDecurt) * _importo) / 100;
                   
                    //MOLTIPLICO PER LE QUANTITA
                    costoProvvigione = costoProvvigione * _qty;

                }
                catch (InvalidCastException e)
                {
                    // SAPbouiCOM.Framework.Application.SBO_Application.MessageBox("Non esiste una provvigione standard per questo agente", 1, "Ok", "", "");
                }
            }
            else
            {
                costoProvvigione = -999999;
            }
            return costoProvvigione;
        }

        public double returnPercent()
        {
            return calcolatoProvvigione;
        }

        //CALCOLO in riga incr/decr
        private double costoIncrDecr(double prezzoOrdine, double qty, double percentIncr, double percentDecr, String data, String ItemCode)
        {
            oRecordSetFt = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));

            double differenzaPercent = 0;
            //RECUPERO LISTINO RIFERIMENTO - SOLO NOVA
            String qLis = "SELECT \"U_ListNum\" FROM \"@FC_PRLS\" ";
            oRecordSetFt.DoQuery(qLis);
            String listNum = oRecordSetFt.Fields.Item("U_ListNum").Value.ToString();
            //RECUPERO DATA ORDINE
            String docDate = "SELECT ordr.\"DocDate\" FROM \"OINV\" oinv JOIN \"INV1\" inv1 ON oinv.\"DocEntry\" = inv1.\"DocEntry\" JOIN \"ORDR\" ordr ON inv1.\"BaseEntry\" = ordr.\"DocEntry\" WHERE oinv.\"DocNum\" = \'" + _docNum + "\' ";
            oRecordSetFt.DoQuery(docDate);
            String dt_inizio = oRecordSetFt.Fields.Item("DocDate").Value.ToString();
            String dt_parse = dt_inizio.Replace("/18", "/20");
            DateTime time = DateTime.Parse(dt_parse);
            DateTime newTime = time.AddDays(1);
            String annoIni = newTime.ToString().Substring(6, 4);
            String meseIni = newTime.ToString().Substring(3, 2);
            String giornoIni = newTime.ToString().Substring(0, 2);
            String annoIni_db = annoIni + meseIni + giornoIni;
            //CONTROLLO IL COSTO DA LISTINO  
            String qIncDec = "select " + Program.SQL_limit1 + " sp.\"Price\" from \"OCRD\" ocrd,\"OINV\" oinv,\"STORICOPREZZI\" sp where ocrd.\"CardCode\"=oinv.\"CardCode\" AND sp.\"PriceList\"=ocrd.\"ListNum\" AND sp.\"ItemCode\" = \'" + ItemCode + "\' " + Program.HANA_limit1 + " ";
            oRecordSetFt.DoQuery(qIncDec);

            if (!oRecordSetFt.Fields.Item("Price").Value.ToString().Equals(""))
            {
                double prezzoListino = double.Parse((oRecordSetFt.Fields.Item("Price").Value.ToString()).Replace(".", ","));
                double differenza = (prezzoOrdine * qty) - (prezzoListino * qty);
                double importoFt = prezzoOrdine * qty;

                //SE DIFFERENZA CON LISTINO AGGIUNGO/TOLGO

                //INCREMENTO
                if (differenza > 0 && prezzoListino > 0)
                {
                    differenzaPercent = ((differenza * percentIncr) / 100) / importoFt;
                }
                //DECREMENTO
                if (differenza < 0 && prezzoListino > 0)
                {
                    differenzaPercent = ((differenza * percentDecr) / 100) / importoFt;
                }
            }
            return differenzaPercent;

        }
    }
}
