﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAPbouiCOM.Framework;

namespace Provvigioni
{
    
    class RegoleCalcolo
    {
        //addetto vendite
        string _addVendite;

        //valori iniziali
        private double valoreProvvigione = 0;
        private double calcolatoProvvigione = 0;
        int inserisco = 0;
        //percentuali
        private double percentCliente = 0;
        private double percentArticolo = 0;
        private double percentVenditore = 0;

        private int _ordineCliente;
        private int _ordineArticolo;
        private int _ordineVenditore;
        private SAPbouiCOM.Matrix _righe;
        private Dictionary<string, int>[] _ordineRegole = new Dictionary<string, int>[4];

        //variabili SAP
        public SAPbobsCOM.Company oCompany = Program.oCompany;
        public SAPbobsCOM.Recordset oRecordSetGenerale;

        public RegoleCalcolo(int ordineCliente, int ordineArticolo, int ordineVenditore, SAPbouiCOM.Matrix righe, Dictionary<string, int>[] ordineRegole,string addVendite)
        {
            _ordineCliente = ordineCliente;
            _ordineArticolo = ordineArticolo;
            _ordineVenditore = ordineVenditore;
            _righe = righe;
            _ordineRegole = ordineRegole;
            _addVendite = addVendite;

            calcoloProvvigione();

            //Application.SBO_Application.MessageBox(_ordineVenditore+" ok", 1, "Ok", "", "");
        }

        public double calcoloProvvigione()
        {

            //recupero tutte le linee in anagrafica valide temporalmente per questo venditore
            //recupero percentuale generica per questo addetto vendite
            oRecordSetGenerale = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            String qControlGenerale = "select A.* from \"@ANAGRAFICA_PROVV\" A , \"OSLP\" O WHERE \"O\".\"SlpCode\" =  \"A\".\"U_SlpCode\" AND \"A\".\"U_SlpCode\" =  \'" + _addVendite + "\' AND \"A\".\"U_CardCodeCliente\"='Tutti' and \"A\".\"U_ItemCode\"='ZTutti012345' ";
            oRecordSetGenerale.DoQuery(qControlGenerale);
            if (oRecordSetGenerale.RecordCount > 0)
            {
                percentVenditore = double.Parse((oRecordSetGenerale.Fields.Item("U_percentuale").Value.ToString()).Replace(".", ","));
                //Application.SBO_Application.MessageBox("Tutti", 1, "Ok", "", "");
            }
            else
            {
                inserisco = 0;
            }

            //CICLO LINEE FATTURA
            /*int numRighe = _righe.RowCount;
            for (int c = 0; c < numRighe - 1; c++)
            {
                calcolatoProvvigione = 0;

                int indexRiga = c + 1;
                String ItemCode = ((SAPbouiCOM.EditText)(_righe.Columns.Item("1").Cells.Item(indexRiga).Specific)).Value.ToString();
                String totArt = ((SAPbouiCOM.EditText)(_righe.Columns.Item("21").Cells.Item(indexRiga).Specific)).Value.ToString();
                int eur = (totArt.Length) - 4;
                totArt = totArt.Remove(eur, 4);
                double totArtCalc = double.Parse(totArt);

                foreach (var key in _ordineRegole)
                {
                    foreach (KeyValuePair<string, int> regola in key)
                    {
                        //CICLO ORDINE REGOLE
                        int valoreRegola = regola.Value;
                        switch (valoreRegola)
                        {
                            case 1:
                                calcoloClienteArticolo("prova", "prova", totArtCalc);
                                break;
                            case 2:
                                calcoloCliente(percentCliente, totArtCalc);
                                break;
                            case 3:
                                calcoloArticolo(percentArticolo, totArtCalc);
                                break;
                            case 4:
                                calcoloVenditore(percentVenditore, totArtCalc);
                                break;
                        }
                        Application.SBO_Application.MessageBox(regola.Value.ToString(), 1, "Ok", "", "");
                    }
                }

                //SOMMO valoreProvvigione per ogni riga fattura
                valoreProvvigione = valoreProvvigione + calcolatoProvvigione;

            }*/
            return valoreProvvigione;
        }

        /*
        1 = CLIENTE/ARTICOLO
        2 = CLIENTE
        3 = ARTICOLO
        4 = VEDNITORE
        */

        public void calcoloClienteArticolo(string itemCode, string clientCode, double tot)
        {
        }

        public void calcoloCliente(double percentCliente,double tot)
        {
        }

        public void calcoloArticolo(double percentArticolo, double tot)
        {

        }

        public void calcoloVenditore(double _percentVenditore, double tot)
        {
            if(_percentVenditore>0){
                calcolatoProvvigione = (_percentVenditore*tot) / 100;
            }
        }

    }
}
