﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Provvigioni
{
    class calcoloLineaProvv
    {
        //DATI FATTURA
        String _addVendite;
        String _CardCode;
        String _ItemCode;
        String _dataDoc;
        String _docNum;
        double _totImporto;
        double _totArtQty;
        double costoProvvigione = 0;
        double costoFinaleProvvigione = 0;

        public calcoloLineaProvv(String addVendite,String CardCode,String ItemCode,String dataDoc,double totImporto,double totArtQty)
        {
            _addVendite = addVendite;
            _CardCode = CardCode;
            _ItemCode = ItemCode;
            _dataDoc = dataDoc;
            _totImporto = totImporto;
            _totArtQty = totArtQty;
        }

        public double provvigioneCalcolata()
        {
            /*
            //REGOLA calcAgenteStd
            calcAgenteStd calcAgStd = new calcAgenteStd(oApp, _addVendite, _ItemCode, _totImporto, _totArtQty, _dataDoc,_docNum);
            double percentAgSt = calcAgStd.calcoloProvvigione();
            if (percentAgSt > 0)
            {
                costoFinaleProvvigione = percentAgSt;
            }

            //SAPbouiCOM.Framework.Application.SBO_Application.MessageBox(costoProvvigione.ToString() + "XXXX", 1, "Ok", "", "");
            

            //REGOLA calcAGItem
            calcAgenteArticolo calcAgArt = new calcAgenteArticolo(oApp, _addVendite, _ItemCode, _totImporto, _totArtQty, _dataDoc, _docNum);
            double percentAgArt = calcAgArt.calcoloProvvigione();
            if (percentAgArt > 0)
            {
                costoFinaleProvvigione = percentAgArt;
            }

            //REGOLA calcAgenteCl
            calcAgenteCliente calcAgCliente = new calcAgenteCliente(oApp, _addVendite, _CardCode, _ItemCode, _totImporto, _totArtQty, _dataDoc, _docNum);
            double percentAgCl = calcAgStd.calcoloProvvigione();
            if (percentAgCl > 0)
            {
                costoFinaleProvvigione = percentAgCl;
            }

            //REGOLA calcAgenteClArt
            calcAgenteClienteArticolo calcAgClArt = new calcAgenteClienteArticolo(oApp, _addVendite, _ItemCode, _CardCode, _totImporto, _totArtQty, _dataDoc, _docNum);
            double percentAgClArt = calcAgStd.calcoloProvvigione();
            if (percentAgClArt > 0)
            {
                costoFinaleProvvigione = percentAgClArt;
            }

            // SAPbouiCOM.Framework.Application.SBO_Application.MessageBox(costoProvvigione.ToString()+"XXXX", 1, "Ok", "", "");
            */
            return costoFinaleProvvigione;
        }

        //DICHIARAZIZONI SISTEMA
        public SAPbouiCOM.Application oApp;
        public SAPbobsCOM.Company oCompany = Program.oCompany;
        public SAPbobsCOM.Recordset oRecordSet;

    }
}
