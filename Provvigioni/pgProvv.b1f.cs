
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace Provvigioni
{

    [FormAttribute("Provvigioni.pgProvv_b1f", "pgProvv.b1f")]
    class pgProvv_b1f : UserFormBase
    {
        public pgProvv_b1f()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.oApp = (SAPbouiCOM.Application)Application.SBO_Application;
            this.oForm = this.UIAPIRawForm;
            this.dataIni = ((SAPbouiCOM.EditText)(this.GetItem("Item_0").Specific));
            this.dataEnd = ((SAPbouiCOM.EditText)(this.GetItem("Item_5").Specific));
            this.totSel = ((SAPbouiCOM.EditText)(this.GetItem("Item_6").Specific));
            this.selectAgente = ((SAPbouiCOM.ComboBox)(this.GetItem("selectAg").Specific));
            this.listaDettaglio = ((SAPbouiCOM.Grid)(this.GetItem("Item_8").Specific));
            this.Button0 = ((SAPbouiCOM.Button)(this.GetItem("btnCerca").Specific));
            this.Button0.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.Button0_ClickBefore);
            this.listaProvvGrid = ((SAPbouiCOM.Grid)(this.GetItem("listaProvv").Specific));
            this.selectLiquid = ((SAPbouiCOM.ComboBox)(this.GetItem("Item_10").Specific));
            this.listaProvvGrid.ClickAfter += new SAPbouiCOM._IGridEvents_ClickAfterEventHandler(this.listaProvvGrid_ClickAfter);
            this.oRecordSet = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.oRecordSetAgList = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.oRecordSetPerc = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.oRecordSetDettaglio = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.oRecordSetFt = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.oRecordSetNumEla = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.btnLiquida = ((SAPbouiCOM.Button)(this.GetItem("Item_13").Specific));
            this.btnLiquida.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.btnLiquida_ClickBefore);
            //      ASSEGNO ELEMENTI FORM
            //   this.elementiForm();
            //      ASSEGNO LA DATA EFFETTIVA DI CALCOLO PROVVIGIONE
            //   this.assegnaDtEffettiva();
            this.Button1 = ((SAPbouiCOM.Button)(this.GetItem("Item_12").Specific));
            this.Button1.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.Button1_ClickBefore);

            //aeesgno a tot sel 0
            totSel.Value = "0";

            this.OnCustomInitialize();

        }

        //SEL TUTTO
        private void Button1_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            this.UIAPIRawForm.Freeze(true);
            oForm = (SAPbouiCOM.IForm)oApp.Forms.ActiveForm;
            SAPbouiCOM.DataTable riga = listaProvvGrid.DataTable;
            int numRows = listaProvvGrid.Rows.Count;
            double tntot = 0;
            for (int i = 0; i < numRows; i++)
            {
                this.listaProvvGrid.DataTable.SetValue(0, i, "Y");

                
                //SOMMO I VALORI
                
                double tnrow = double.Parse(this.listaProvvGrid.DataTable.GetValue("Valore Provv.", listaProvvGrid.GetDataTableRowIndex(i)).ToString().Replace('.', ','));

                if (!double.IsNaN(tnrow) && !Double.IsInfinity(tnrow))
                {
                    tntot = tntot + tnrow;

                    totSel.Value = tntot.ToString();
                }
                
            }

            this.UIAPIRawForm.Freeze(false);

        }

        //BOTTONE CERCA
        private void Button0_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {


            BubbleEvent = true;
            this.UIAPIRawForm.Freeze(true);
            oForm = (SAPbouiCOM.IForm)oApp.Forms.ActiveForm;
            //FILTRO AGENTE
            String SlpCode = this.selectAgente.Value.Trim();
            if (!SlpCode.Equals("Scegli Agente"))
            {
                filtro_agente = " AND fc_prft.\"U_SlpCode\"= '" + SlpCode + "' ";
            }
            else
            {
                filtro_agente = "";
            }


            //FILTRO DATA INI
            String dtIni = this.dataIni.Value.Trim();

            if (!dtIni.Equals(""))
            {
                String annoIni = dtIni.Substring(0, 4);
                String meseIni = dtIni.Substring(4, 2);
                String giornoIni = dtIni.Substring(6, 2);
                String dtIniDB = annoIni + meseIni + giornoIni;
                filtro_dtIni = " AND fc_prft.\"U_dataFt\" >= '" + dtIniDB + "' ";
            }
            else
            {
                filtro_dtIni = "";
            }

            //FILTRO DATA END
            String dtEnd = this.dataEnd.Value.Trim();

            if (!dtEnd.Equals(""))
            {
                String annoEnd = dtEnd.Substring(0, 4);
                String meseEnd = dtEnd.Substring(4, 2);
                String giornoEnd = dtEnd.Substring(6, 2);
                String dtEndDB = annoEnd + meseEnd + giornoEnd;
                filtro_dtEnd = " AND fc_prft.\"U_dataFt\" <= '" + dtEndDB + "' ";
            }
            else
            {
                filtro_dtEnd = "";
            }
            try
            {
                filtroData = oForm.DataSources.DataTables.Add("filtro");
            }
            catch (System.Runtime.InteropServices.COMException e)
            {
                //Application.SBO_Application.MessageBox(e.ToString(), 1, "Ok", "", "");
            }

            cercaProvvigioni();

            //ASSEGNO LA DATA EFFETTIVA DI CALCOLO PROVVIGIONE
            assegnaDtEffettiva();

            this.UIAPIRawForm.Freeze(false);

            /*double totale_provvigione = 0;
            oRecordSet.DoQuery(queryFiltro);
            while(!oRecordSet.EoF){
                totale_provvigione = totale_provvigione + double.Parse((oRecordSet.Fields.Item("Costo").Value.ToString()).Replace(".", ","));

                oRecordSet.MoveNext();
            }*/

            //AGGIORNO I TOTALI
            //totProvv.Value = totale_provvigione.ToString();
        }

        //BOTTONE AGGIORNA
        private void btnAggiorna_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            oForm = (SAPbouiCOM.IForm)oApp.Forms.ActiveForm;

        }

        //BOTTONE LIQUIDA
        private void btnLiquida_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            this.UIAPIRawForm.Freeze(true);
            oForm = (SAPbouiCOM.IForm)oApp.Forms.ActiveForm;
            SAPbouiCOM.DataTable riga = listaProvvGrid.DataTable;

            int numRows = listaProvvGrid.Rows.Count;

            string[] righeIDS_dettaglio = new string[numRows];
            string[] righeCostoGlobale_dettaglio = new string[numRows];
            string[] righeCosto_dettaglio = new string[numRows];
            string[] righeDocEntry_dettaglio = new string[numRows];
            string[] righeAgente_dettaglio = new string[numRows];
            string[] righeLiquidato_dettaglio = new string[numRows];
            string[] righeTipo_dettaglio = new string[numRows];
            string[] righeCheck_dettaglio = new string[numRows];

            for (int i = 0; i < numRows; i++)
            {
                String nDoc = listaProvvGrid.DataTable.GetValue("N° Doc.", i).ToString();
                String select = listaProvvGrid.DataTable.GetValue("Selezione", i).ToString();
                String costo_globale = listaProvvGrid.DataTable.GetValue("Provv. Globale", i).ToString();
                String costo = listaProvvGrid.DataTable.GetValue("Valore Provv.", i).ToString();
                String agenteD = listaProvvGrid.DataTable.GetValue("Agente", i).ToString();
                String docEntryD = listaProvvGrid.DataTable.GetValue("DocEntry", i).ToString();
                String liquidatoD = listaProvvGrid.DataTable.GetValue("Liquidato", i).ToString();
                String tipoD = listaProvvGrid.DataTable.GetValue("Tipo", i).ToString();
                if (select.Equals("Y"))
                {
                    righeIDS_dettaglio[i] = nDoc;
                    righeCostoGlobale_dettaglio[i] = costo_globale;
                    righeCosto_dettaglio[i] = costo;
                    righeLiquidato_dettaglio[i] = liquidatoD;
                    righeDocEntry_dettaglio[i] = docEntryD;
                    righeAgente_dettaglio[i] = agenteD;
                    righeTipo_dettaglio[i] = tipoD;
                    righeCheck_dettaglio[i] = "y";
                }
                else
                {
                    righeCheck_dettaglio[i] = "n";
                }

            }
            liquidaProvv_b1f activeForm = new liquidaProvv_b1f(righeIDS_dettaglio, righeDocEntry_dettaglio, righeCostoGlobale_dettaglio, righeLiquidato_dettaglio, righeCosto_dettaglio, righeAgente_dettaglio, righeTipo_dettaglio, righeCheck_dettaglio);
            activeForm.Show();

            this.dataEnd.Value = dtEnd;

            queryFiltro = qProvv();

            //Application.SBO_Application.MessageBox(queryFiltro, 1, "Ok", "", "");
            oForm.DataSources.DataTables.Item("filtro").ExecuteQuery(queryFiltro);
            //oForm.DataSources.Columns.Add("#", SAPbouiCOM.BoFieldsType.ft_Text, 20);
            listaProvvGrid.DataTable = oForm.DataSources.DataTables.Item("filtro");


            //ASSEGNO ELEMENTI FORM
            //elementiForm();

            //ASSEGNO LA DATA EFFETTIVA DI CALCOLO PROVVIGIONE
            //assegnaDtEffettiva();

            this.UIAPIRawForm.Freeze(false);


        }
        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
            this.ActivateAfter += new ActivateAfterHandler(this.Form_ActivateAfter);

        }

        private void OnCustomInitialize()
        {
            totSel.Value = "0";

            //ASSEGNO ELEMENTI FORM
            // elementiForm();

            //POPOLO DDL AGENTI
            selectAgente.ValidValues.Add("Scegli Agente", "Scegli Agente");
            selectAgente.ValidValues.Add("Tutti gli Agenti", "Tutti gli Agenti");
            String qAgenti = "SELECT * FROM \"OSLP\" order by \"SlpName\" ASC ";
            oRecordSetAgList.DoQuery(qAgenti);
            while (!oRecordSetAgList.EoF)
            {
                selectAgente.ValidValues.Add(oRecordSetAgList.Fields.Item("SlpCode").Value.ToString(), oRecordSetAgList.Fields.Item("SlpName").Value.ToString());
                oRecordSetAgList.MoveNext();
            }

            selectAgente.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
            selectAgente.Select("Scegli Agente", SAPbouiCOM.BoSearchKey.psk_ByValue);

            //POPOLO DDL TIPO LIQUIDAZIONE
            selectLiquid.ValidValues.Add("tot", "Totale incassato");
            selectLiquid.ValidValues.Add("par", "Parziale incassato");
            selectLiquid.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
        }

        //SELEZIONE E SOMMA DEI CHECK
        private void listaProvvGrid_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            this.UIAPIRawForm.Freeze(true);
            oForm = (SAPbouiCOM.IForm)oApp.Forms.ActiveForm;
            //SOMMO I VALORI
            if (pVal.ColUID == "Selezione")
            {
                if (pVal.Row != -1)
                {
                    //var valore = this.listaProvvGrid.DataTable.GetValue(0, pVal.Row).ToString();
                   // double tntot = double.Parse(totSel.Value.Replace('.', ','));
                   // double tnrow = double.Parse(this.listaProvvGrid.DataTable.GetValue(9, pVal.Row).ToString().Replace('.', ','));
                   // if (valore == "Y")
                   // {
                  //      tntot = tntot + tnrow;
                  //  }
                  //  else
                 //   {
                  //      tntot = tntot - tnrow;
                  //  }
                  //  totSel.Value = tntot.ToString();
                }
            }
            //VISUALIZZO IL DETTAGLIO
            if (pVal.ColUID == "Dettaglio")
            {
                if (pVal.Row != -1)
                {
                    var valore = this.listaProvvGrid.DataTable.GetValue(12, pVal.Row).ToString();
                    if (valore == "Y")
                    {

                        try
                        {
                            oForm.DataSources.DataTables.Add("filtroDettaglioXYZ");
                        }
                        catch (System.Runtime.InteropServices.COMException e)
                        {
                            //Application.SBO_Application.MessageBox(e.ToString(), 1, "Ok", "", "");
                        }
                        String docNumFattura = this.listaProvvGrid.DataTable.GetValue(4, pVal.Row).ToString();
                        String tipoDoc = this.listaProvvGrid.DataTable.GetValue(5, pVal.Row).ToString();
                        String dataFattura = this.listaProvvGrid.DataTable.GetValue(6, pVal.Row).ToString();
                        double rapporto_imp_liquid = 0;
                        try
                        {
                           rapporto_imp_liquid = double.Parse(listaProvvGrid.DataTable.GetValue("Rapporto", pVal.Row).ToString().Replace('.', ','));
                        }
                        catch (Exception e)
                        {
                            //Application.SBO_Application.MessageBox(e.ToString(), 1, "Ok", "", "");
                        }
                        

                        
                        

                        String qD = null;

                        qD = " SELECT  ";
                        qD += " T0.\"SlpCode\" as \"Cod.Ag.\", ";
                        qD += " T2.\"SlpName\" as \"Nome Ag.\", ";
                        qD += " T0.\"CardCode\" as \"Cod.Cl.\", ";
                        qD += " T0.\"DocDate\" as \"Data Ord.\", ";
                        qD += " T0.\"DocNum\" as \"N° Fattura\", ";
                        qD += " T1.\"ItemCode\" as \"ItemCode\", ";
                        qD += " T3.\"ItemName\", ";
                        //qD += " 'PREZZO_LISTINO' as  \"Prz. Listino\", ";
                        qD += " T4.\"IndustryC\" as \"Settore\", ";
                        qD += " T3.\"PrcrmntMtd\" as \"Tipo\", ";
                        //qD += " T3.\"ItmsGrpCod\" as \"Gruppo Art.\", ";
                        qD += " T3.\"U_AG_GRPOMOG\" as \"Gruppo Omog.\", ";
                        qD += " T3.\"U_AG_OcrCode\" as \"B.Unit\", ";
                        qD += " T1.\"Price\" as \"Prz. Fattura\", ";
                        qD += " T1.\"Quantity\" as \"Q.tà\", ";
                        qD += " 'PERCENT1' as \"% Std.\", ";
                        qD += " 'PERCENT5' as \"% Sett.\", ";
                        qD += " 'PERCENT6' as \"% Tipo\", ";
                        //qD += " 'PERCENT7' as \"% Grp.Art.\", ";
                        qD += " 'PERCENT7' as \"% Grp.Omog.\", ";
                        qD += " 'PERCENT2' as \"% Art.\", ";
                        qD += " 'PERCENT3' as \"% Cliente\", ";
                        //qD += " 'PERCENT8' as \"% Grp.Art. Cl.\", ";
                        qD += " 'PERCENT8' as \"% Grp.Omog. Cl.\", ";
                        qD += " 'PERCENT4' as \"% Art. Cl.\", ";
                        qD += " 'PERCENT9' as \"% B.Unit\", ";
                        //qD += " --'DEC' as \"Decurt.\", ";
                        //qD += " --'percent_dec' as \"% Dec.\", ";
                        // qD += " --'valore_decurtazione' as \"Valore Dec.\", ";
                        //qD += " 'COSTO_RIGA' as \"Valore Provv.\", ";
                        //qD += " --'PERCENTUALE_INCREMENTO_RIGA' as \"% Incr./Decr.\", ";
                        qD += " 'IMPONIBILE_LIQUID' as \"% Imp./Liq.\", ";
                        qD += " 'percent_finale_provvigione' as \"% Tot.\", ";
                        qD += " 'COSTO_FINE_XXX_YYY_ZZZ' as \"Totale\"  ";

                        qD += " FROM ";

                        if (tipoDoc.Equals("Ft"))
                        {
                            qD += " \"OINV\" T0 ";
                            qD += " INNER JOIN \"INV1\" T1 ON T1.\"DocEntry\"=T0.\"DocEntry\" "; 
                        }
                        else
                        {
                            qD += " \"ORIN\" T0 ";
                            qD += " INNER JOIN \"RIN1\" T1 ON T1.\"DocEntry\"=T0.\"DocEntry\" ";
                        }
                        qD += " INNER JOIN \"OSLP\" T2 ON T2.\"SlpCode\" = T0.\"SlpCode\"  ";
                        qD += " INNER JOIN \"OITM\" T3 ON T3.\"ItemCode\"=T1.\"ItemCode\"  ";
                        qD += " INNER JOIN \"OCRD\" T4 ON T4.\"CardCode\" = T0.\"CardCode\" ";

                        qD += " WHERE ";

                        qD += " T0.\"DocNum\" = \'" + docNumFattura + "\' ";

                        oForm.DataSources.DataTables.Item("filtroDettaglioXYZ").ExecuteQuery(qD);

                        listaDettaglio.DataTable = oForm.DataSources.DataTables.Item("filtroDettaglioXYZ");

                        String percent1 = "";
                        String percent2 = "";
                        String percent3 = "";
                        String percent4 = "";
                        String percent5 = "";
                        String percent6 = "";
                        String percent7 = "";
                        String percent8 = "";
                        String percent9 = "";


                        //SETTO PERCENTUALI DINAMICHE
                        int numRows = listaDettaglio.Rows.Count;

                        for (int i = 0; i < numRows; i++)
                        {
                            int calcoloEx = 0;
                            double costoRiga = 0;
                            double totaleRiga = 0;
                            double percentFinale = 0;

       
                            String ItemCode = listaDettaglio.DataTable.GetValue("ItemCode", i).ToString();
                            String SlpCode = listaDettaglio.DataTable.GetValue("Cod.Ag.", i).ToString();
                            String CardCode = listaDettaglio.DataTable.GetValue("Cod.Cl.", i).ToString();
                            String docNum = listaDettaglio.DataTable.GetValue("N° Fattura", i).ToString();
                            String dtOrdine = listaDettaglio.DataTable.GetValue("Data Ord.", i).ToString();
                            String settore = listaDettaglio.DataTable.GetValue("Settore", i).ToString();
                            String tipoArt = listaDettaglio.DataTable.GetValue("Tipo", i).ToString();
                            //String gruppoArt = listaDettaglio.DataTable.GetValue("Gruppo Art.", i).ToString();
                            String gruppoOmog = listaDettaglio.DataTable.GetValue("Gruppo Omog.", i).ToString();
                            String BU = listaDettaglio.DataTable.GetValue("B.Unit", i).ToString();
                            String dt_inizio = dtOrdine;
                            DateTime time = DateTime.Parse(dt_inizio);
                            DateTime newTime = time.AddDays(1);
                            String annoIni = newTime.ToString().Substring(6, 4);
                            String meseIni = newTime.ToString().Substring(3, 2);
                            String giornoIni = newTime.ToString().Substring(0, 2);
                            String dt_db = annoIni + meseIni + giornoIni;
                            //A MONTE CONTROLLO SE ARTICOLO ESCLUSO
                            String qEx = "SELECT \"U_startString\" FROM \"@FC_PRIT\" ";
                            oRecordSet.DoQuery(qEx);
                            String artEx = "";
                            while (!oRecordSet.EoF)
                            {
                                artEx = oRecordSet.Fields.Item("U_startString").Value.ToString();
                                int artLenght = artEx.Length;
                                if (artEx.Equals(ItemCode.Substring(0, artLenght)))
                                {
                                    calcoloEx = 1;
                                }
                                oRecordSet.MoveNext();
                            }
                            
                            if (calcoloEx == 0)
                            {
                                //String prezzoListino_str = "0";
                                //listaDettaglio.DataTable.SetValue("Prz. Listino", i, prezzoListino_str);

                                //double prezzoListino = double.Parse(listaDettaglio.DataTable.GetValue("Prz. Listino", i).ToString());
                                double quantitaRiga = double.Parse(listaDettaglio.DataTable.GetValue("Q.tà", i).ToString());
                                double prezzoOrdine = double.Parse(listaDettaglio.DataTable.GetValue("Prz. Fattura", i).ToString());

                                //AGENTE STANDARD
                                String qSt = "SELECT " + Program.SQL_limit1 + " * FROM \"@FC_ANAG\" WHERE \"U_dtINI\" <= \'" + dt_db + "\' AND \"U_dtEND\" >= \'" + dt_db + "\' and \"U_SlpCode\" =\'" + SlpCode.ToString() + "\' AND \"U_Code\" LIKE \'percStd_%\' " + Program.HANA_limit1 + " ";
                                //Application.SBO_Application.MessageBox(qSt, 1, "Ok", "", "");
                                oRecordSetPerc.DoQuery(qSt);
                                percent1 = "";
                                if (oRecordSetPerc.Fields.Item("U_percent").Value.ToString().Equals(""))
                                {
                                    percent1 = "/";
                                }
                                else
                                {
                                    percent1 = oRecordSetPerc.Fields.Item("U_percent").Value.ToString();
                                    costoRiga = costoPercentRiga(double.Parse(percent1), prezzoOrdine, quantitaRiga) * quantitaRiga;
                                    totaleRiga = costoPercentRiga(double.Parse(percent1) , prezzoOrdine, quantitaRiga) * quantitaRiga;
                                    percentFinale = double.Parse(percent1);
                                }

                                //AGENTE SETTORE
                                String qStSett = "SELECT " + Program.SQL_limit1 + " * FROM \"@FC_ANAG\" WHERE \"U_dtINI\" <= \'" + dt_db + "\' AND \"U_dtEND\" >= \'" + dt_db + "\' and \"U_SlpCode\" =\'" + SlpCode.ToString() + "\' and \"U_SettoreCode\" =\'" + settore + "\' AND \"U_Code\" LIKE \'percSettore_%\' " + Program.HANA_limit1 + " ";
                                //Application.SBO_Application.MessageBox(qSt, 1, "Ok", "", "");
                                oRecordSetPerc.DoQuery(qStSett);
                                percent5 = "";
                                if (oRecordSetPerc.Fields.Item("U_percent").Value.ToString().Equals(""))
                                {
                                    percent5 = "/";
                                }
                                else
                                {
                                    percent5 = oRecordSetPerc.Fields.Item("U_percent").Value.ToString();
                                    costoRiga = costoPercentRiga(double.Parse(percent5), prezzoOrdine,  quantitaRiga) * quantitaRiga;
                                    totaleRiga = costoPercentRiga(double.Parse(percent5), prezzoOrdine, quantitaRiga) * quantitaRiga;
                                    percentFinale = double.Parse(percent5);
                                }

                                //AGENTE TIPO ARTICOLO
                                if(tipoArt.Equals("B")){
                                    tipoArt = "Acquisto";
                                }
                                else
                                {
                                    tipoArt = "Produzione";
                                }
                                String qStTipoArt = "SELECT " + Program.SQL_limit1 + " * FROM \"@FC_ANAG\" WHERE \"U_dtINI\" <= \'" + dt_db + "\' AND \"U_dtEND\" >= \'" + dt_db + "\' and \"U_SlpCode\" =\'" + SlpCode.ToString() + "\' and \"U_TipoArt\" =\'" + tipoArt + "\' AND \"U_Code\" LIKE \'percTipoItem_%\' " + Program.HANA_limit1 + " ";
                                //Application.SBO_Application.MessageBox(qSt, 1, "Ok", "", "");
                                oRecordSetPerc.DoQuery(qStTipoArt);
                                percent6 = "";
                                if (oRecordSetPerc.Fields.Item("U_percent").Value.ToString().Equals(""))
                                {
                                    percent6 = "/";
                                }
                                else
                                {
                                    percent6 = oRecordSetPerc.Fields.Item("U_percent").Value.ToString();
                                    costoRiga = costoPercentRiga(double.Parse(percent6), prezzoOrdine, quantitaRiga) * quantitaRiga;
                                    totaleRiga = costoPercentRiga(double.Parse(percent6), prezzoOrdine, quantitaRiga) * quantitaRiga;
                                    percentFinale = double.Parse(percent6);
                                }

                                //AGENTE GRUPPO ARTICOLI
                                /*String qStGrpArt = "SELECT " + Program.SQL_limit1 + " * FROM \"@FC_ANAG\" WHERE \"U_dtINI\" <= \'" + dt_db + "\' AND \"U_dtEND\" >= \'" + dt_db + "\' and \"U_SlpCode\" =\'" + SlpCode.ToString() + "\' and \"U_GrpCode\" =\'" + gruppoArt + "\' AND \"U_Code\" LIKE \'percGrpItem_%\' " + Program.HANA_limit1 + " ";
                                //Application.SBO_Application.MessageBox(qSt, 1, "Ok", "", "");
                                oRecordSetPerc.DoQuery(qStGrpArt);
                                percent7 = "";
                                if (oRecordSetPerc.Fields.Item("U_percent").Value.ToString().Equals(""))
                                {
                                    percent7 = "/";
                                }
                                else
                                {
                                    percent7 = oRecordSetPerc.Fields.Item("U_percent").Value.ToString();
                                    costoRiga = costoPercentRiga(double.Parse(percent7), prezzoOrdine, quantitaRiga) * quantitaRiga;
                                    totaleRiga = costoPercentRiga(double.Parse(percent7), prezzoOrdine, quantitaRiga) * quantitaRiga;
                                    percentFinale = double.Parse(percent7);
                                }*/

                                //AGENTE GRUPPO OMOGENEO
                                String qStGrpOmog = "SELECT " + Program.SQL_limit1 + " * FROM \"@FC_ANAG\" WHERE \"U_dtINI\" <= \'" + dt_db + "\' AND \"U_dtEND\" >= \'" + dt_db + "\' and \"U_SlpCode\" =\'" + SlpCode.ToString() + "\'  AND \"U_CardCode\" =  \'\' and \"U_GrpOmog\" =\'" + gruppoOmog + "\' AND \"U_Code\" LIKE \'percGrpOmogeneo_%\' " + Program.HANA_limit1 + " ";
                                //Application.SBO_Application.MessageBox(qSt, 1, "Ok", "", "");
                                oRecordSetPerc.DoQuery(qStGrpOmog);
                                percent7 = "";
                                if (oRecordSetPerc.Fields.Item("U_percent").Value.ToString().Equals(""))
                                {
                                    percent7 = "/";
                                }
                                else
                                {
                                    percent7 = oRecordSetPerc.Fields.Item("U_percent").Value.ToString();
                                    costoRiga = costoPercentRiga(double.Parse(percent7), prezzoOrdine, quantitaRiga) * quantitaRiga;
                                    totaleRiga = costoPercentRiga(double.Parse(percent7), prezzoOrdine, quantitaRiga) * quantitaRiga;
                                    percentFinale = double.Parse(percent7);
                                }

                                //AGENTE ARTICOLO
                                String qItem = "select " + Program.SQL_limit1 + " * from \"@FC_ANAG\" where \"U_dtINI\" <= \'" + dt_db + "\' AND \"U_dtEND\"> \'" + dt_db + "\' AND \"U_SlpCode\" =  \'" + SlpCode.ToString() + "\'  AND \"U_CardCode\" =  \'\'  AND \"U_ItemCode\" =  \'" + ItemCode + "\' AND \"U_Code\" LIKE 'percAgItem_%'  order by \"U_dtEND\" desc" + Program.HANA_limit1 + " ";
                                //Application.SBO_Application.MessageBox(qItem, 1, "Ok", "", "");
                                oRecordSetPerc.DoQuery(qItem);
                                percent2 = "";
                                if (oRecordSetPerc.Fields.Item("U_percent").Value.ToString().Equals(""))
                                {
                                    percent2 = "/";
                                }
                                else
                                {
                                    percent2 = oRecordSetPerc.Fields.Item("U_percent").Value.ToString();
                                    costoRiga = costoPercentRiga(double.Parse(percent2), prezzoOrdine, quantitaRiga) * quantitaRiga;
                                    totaleRiga = costoPercentRiga(double.Parse(percent2), prezzoOrdine, quantitaRiga) * quantitaRiga;
                                    percentFinale = double.Parse(percent2);
                                }

                                //AGENTE CLIENTE
                                String qCl = "select " + Program.SQL_limit1 + " * from \"@FC_ANAG\" where \"U_dtINI\" <= \'" + dt_db + "\' AND \"U_dtEND\"> \'" + dt_db + "\' AND \"U_SlpCode\" =  \'" + SlpCode.ToString() + "\' AND \"U_ItemCode\" =  \'\' AND \"U_CardCode\" =  \'" + CardCode + "\' AND \"U_Code\" LIKE 'percAgCl_%'   order by \"U_dtEND\" desc " + Program.HANA_limit1 + " ";
                                //Application.SBO_Application.MessageBox(qCl, 1, "Ok", "", "");
                                oRecordSetPerc.DoQuery(qCl);
                                percent3 = "";
                                if (oRecordSetPerc.Fields.Item("U_percent").Value.ToString().Equals(""))
                                {
                                    percent3 = "/";
                                }
                                else
                                {
                                    percent3 = oRecordSetPerc.Fields.Item("U_percent").Value.ToString();
                                    
                                    costoRiga = costoPercentRiga(double.Parse(percent3), prezzoOrdine,  quantitaRiga) * quantitaRiga;
                                    totaleRiga = costoPercentRiga(double.Parse(percent3) , prezzoOrdine,  quantitaRiga) * quantitaRiga;
                                    percentFinale = double.Parse(percent3);
                                }

                                //AGENTE CLIENTE GRUPPO ARTICOLI
                                /*String qClGrpArt = "select " + Program.SQL_limit1 + " * from \"@FC_ANAG\" where \"U_dtEND\"> \'" + dt_db + "\' AND \"U_SlpCode\" =  \'" + SlpCode.ToString() + "\' and \"U_GrpCode\" =\'" + gruppoArt + "\' AND \"U_CardCode\" =  \'" + CardCode + "\' AND \"U_Code\" LIKE 'percGrpArtCliente_%'  order by \"U_dtEND\" desc " + Program.HANA_limit1 + " ";
                                //Application.SBO_Application.MessageBox(qClItem, 1, "Ok", "", "");
                                oRecordSetPerc.DoQuery(qClGrpArt);
                                percent8 = "";
                                if (oRecordSetPerc.Fields.Item("U_percent").Value.ToString().Equals(""))
                                {
                                    percent8 = "/";
                                }
                                else
                                {
                                    percent8 = oRecordSetPerc.Fields.Item("U_percent").Value.ToString();
                                    costoRiga = costoPercentRiga(double.Parse(percent8), prezzoOrdine, quantitaRiga) * quantitaRiga;
                                    totaleRiga = costoPercentRiga(double.Parse(percent8), prezzoOrdine, quantitaRiga) * quantitaRiga;
                                    percentFinale = double.Parse(percent8);
                                }*/

                                //AGENTE CLIENTE GRUPPO OMOGENEO
                                String qClGrpOmog = "select " + Program.SQL_limit1 + " * from \"@FC_ANAG\" where \"U_dtINI\" <= \'" + dt_db + "\' AND \"U_dtEND\"> \'" + dt_db + "\' AND \"U_SlpCode\" =  \'" + SlpCode.ToString() + "\' and \"U_GrpOmog\" =\'" + gruppoOmog + "\' AND \"U_CardCode\" =  \'" + CardCode + "\' AND \"U_Code\" LIKE 'percGrpOmogCl_%'  order by \"U_dtEND\" desc " + Program.HANA_limit1 + " ";
                                //Application.SBO_Application.MessageBox(qClItem, 1, "Ok", "", "");
                                oRecordSetPerc.DoQuery(qClGrpOmog);
                                percent8 = "";
                                if (oRecordSetPerc.Fields.Item("U_percent").Value.ToString().Equals(""))
                                {
                                    percent8 = "/";
                                }
                                else
                                {
                                    percent8 = oRecordSetPerc.Fields.Item("U_percent").Value.ToString();
                                    costoRiga = costoPercentRiga(double.Parse(percent8), prezzoOrdine, quantitaRiga) * quantitaRiga;
                                    totaleRiga = costoPercentRiga(double.Parse(percent8), prezzoOrdine, quantitaRiga) * quantitaRiga;
                                    percentFinale = double.Parse(percent8);
                                }

                                //AGENTE CLIENTE ARTICOLO
                                String qClItem = "select " + Program.SQL_limit1 + " * from \"@FC_ANAG\" where \"U_dtINI\" <= \'" + dt_db + "\' AND \"U_dtEND\"> \'" + dt_db + "\' AND \"U_SlpCode\" =  \'" + SlpCode.ToString() + "\' AND \"U_ItemCode\" =  \'" + ItemCode + "\' AND \"U_CardCode\" =  \'" + CardCode + "\' AND \"U_Code\" LIKE 'percAgClArt_%'  order by \"U_dtEND\" desc " + Program.HANA_limit1 + " ";
                                //Application.SBO_Application.MessageBox(qClItem, 1, "Ok", "", "");
                                oRecordSetPerc.DoQuery(qClItem);
                                percent4 = "";
                                if (oRecordSetPerc.Fields.Item("U_percent").Value.ToString().Equals(""))
                                {
                                    percent4 = "/";
                                }
                                else
                                {
                                    percent4 = oRecordSetPerc.Fields.Item("U_percent").Value.ToString();
                                    costoRiga = costoPercentRiga(double.Parse(percent4), prezzoOrdine,  quantitaRiga) * quantitaRiga;
                                    totaleRiga = costoPercentRiga(double.Parse(percent4), prezzoOrdine, quantitaRiga) * quantitaRiga;
                                    percentFinale = double.Parse(percent4);
                                }

                                //AGENTE BUSINESS UNIT
                                String qStBU = "SELECT " + Program.SQL_limit1 + " * FROM \"@FC_ANAG\" WHERE \"U_dtINI\" <= \'" + dt_db + "\' AND \"U_dtEND\" >= \'" + dt_db + "\' and \"U_SlpCode\" =\'" + SlpCode.ToString() + "\' and \"U_BUnit\" =\'" + BU + "\' AND \"U_Code\" LIKE \'percAgBU_%\' " + Program.HANA_limit1 + " ";
                                //Application.SBO_Application.MessageBox(qSt, 1, "Ok", "", "");
                                oRecordSetPerc.DoQuery(qStBU);
                                percent9 = "";
                                if (oRecordSetPerc.Fields.Item("U_percent").Value.ToString().Equals(""))
                                {
                                    percent9 = "/";
                                }
                                else
                                {
                                    percent9 = oRecordSetPerc.Fields.Item("U_percent").Value.ToString();
                                    costoRiga = costoPercentRiga(double.Parse(percent9), prezzoOrdine, quantitaRiga) * quantitaRiga;
                                    totaleRiga = costoPercentRiga(double.Parse(percent9), prezzoOrdine, quantitaRiga) * quantitaRiga;
                                    percentFinale = double.Parse(percent9);
                                }

                                //CONTROLLO IL COSTO DA LISTINO  
                                //query commentata eccezione nova
                                //String qIncDec = "select sp.\"Price\" from \"OCRD\" ocrd,\"OINV\" oinv,\"STORICOPREZZI\" sp where ocrd.\"CardCode\"=oinv.\"CardCode\" AND sp.\"PriceList\"=ocrd.\"ListNum\" AND sp.\"ItemCode\" = \'" + ItemCode + "\' limit 1";

                            }//CHIUDO IF CALCOLATO EX
                            //AGGIORNO I FORM DOPO I DATI
                            try
                            {
                                if (tipoDoc.Equals("Nc"))
                                {
                                    totaleRiga = totaleRiga * -1;
                                }

                                //Calcolo costo parziale
                                double costo_finale_riga = ((rapporto_imp_liquid * totaleRiga) / 100);

                                listaDettaglio.DataTable.SetValue("% Std.", i, percent1);
                                listaDettaglio.DataTable.SetValue("% Sett.", i, percent5);
                                listaDettaglio.DataTable.SetValue("% Tipo", i, percent6);
                                listaDettaglio.DataTable.SetValue("% Grp.Omog.", i, percent7);
                                listaDettaglio.DataTable.SetValue("% Art.", i, percent2);
                                listaDettaglio.DataTable.SetValue("% Cliente", i, percent3);
                                listaDettaglio.DataTable.SetValue("% Grp.Omog. Cl.", i, percent8);
                                listaDettaglio.DataTable.SetValue("% Art. Cl.", i, percent4);
                                listaDettaglio.DataTable.SetValue("% B.Unit", i, percent9);
                                //listaDettaglio.DataTable.SetValue("Valore Dec.", i, Math.Round(costoRiga - totaleRiga, 3).ToString());
                                //listaDettaglio.DataTable.SetValue("Valore Provv.", i, Math.Round(costoRiga, 3).ToString());
                                listaDettaglio.DataTable.SetValue("% Imp./Liq.", i, rapporto_imp_liquid.ToString());
                                listaDettaglio.DataTable.SetValue("% Tot.", i, percentFinale.ToString());
                                listaDettaglio.DataTable.SetValue("Totale", i, Math.Round(costo_finale_riga, 3).ToString());

                            }
                            catch (System.Runtime.InteropServices.COMException e)
                            {
                                Application.SBO_Application.MessageBox(e.ToString(), 1, "Ok", "", "");
                            }


                        }
                        listaDettaglio.AutoResizeColumns();

                        elementiFormDettaglio();
                    }
                    else
                    {
                        listaDettaglio.DataTable.Clear();
                    }
                }
            }
            this.UIAPIRawForm.Freeze(false);
        }

        //ELEMENTI FORM TESTATA
        private void elementiForm()
        {
            //DISABLE COLONNE
            listaProvvGrid.Columns.Item(1).Editable = false;
            listaProvvGrid.Columns.Item(2).Editable = false;
            listaProvvGrid.Columns.Item(3).Editable = false;
            listaProvvGrid.Columns.Item(4).Editable = false;
            listaProvvGrid.Columns.Item(5).Editable = false;
            listaProvvGrid.Columns.Item(6).Editable = false;
            listaProvvGrid.Columns.Item(7).Editable = false;
            listaProvvGrid.Columns.Item(8).Editable = false;
            listaProvvGrid.Columns.Item(9).Editable = false;
            listaProvvGrid.Columns.Item(10).Editable = false;
            listaProvvGrid.Columns.Item(11).Editable = false;
            listaProvvGrid.Columns.Item(13).Editable = false;
            listaProvvGrid.Columns.Item(13).Visible = false;
            listaProvvGrid.Columns.Item(14).Visible = false;

            //COLONNE CUSTOM
            listaProvvGrid.Columns.Item(0).TitleObject.Sortable = true;
            listaProvvGrid.Columns.Item(0).Type = SAPbouiCOM.BoGridColumnType.gct_CheckBox;

           
            listaProvvGrid.Columns.Item(12).Type = SAPbouiCOM.BoGridColumnType.gct_CheckBox;

            //data effettiva
           // listaProvvGrid.Columns.Item(8).TitleObject.Sortable = true;
           // listaProvvGrid.Columns.Item(8).Type = SAPbouiCOM.BoGridColumnType.gct_EditText;

            //costo aggiornato
           // listaProvvGrid.Columns.Item(10).TitleObject.Sortable = true;
           // listaProvvGrid.Columns.Item(10).Type = SAPbouiCOM.BoGridColumnType.gct_EditText;




            //SETTO VALORE INIZIALE UPDATE COSTO
            int numRows = listaProvvGrid.Rows.Count;

            string[] righeIDS = new string[numRows];


            //RECUPERO IL NUM PER elaborazione riga
            int newNumEla = 0;
            String qLastCodeA = "SELECT " + Program.SQL_limit1 + " \"U_NUM_ELA\" FROM \"@FC_ANRP\"  ORDER BY CAST(\"Code\" AS INT )  DESC " + Program.HANA_limit1 + " ";
            oRecordSetNumEla = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            oRecordSetNumEla.DoQuery(qLastCodeA);
            if (oRecordSetNumEla.Fields.Item("U_NUM_ELA").Value.ToString().Equals(""))
            {
                newNumEla = 1;
            }
            else
            {
                newNumEla = Int32.Parse(oRecordSetNumEla.Fields.Item("U_NUM_ELA").Value.ToString());
                newNumEla = newNumEla + 1;
            }

            for (int i = 0; i < numRows; i++)
            {

                try
                {
                   

                    //CALCOLO il rapporto tra imponibile e liquidato
                    double Imponibile = double.Parse(listaProvvGrid.DataTable.GetValue("Imponibile", i).ToString());
                    double Liquidato = double.Parse(listaProvvGrid.DataTable.GetValue("Liquidato", i).ToString());

                    double rapporto_pagato = (Liquidato * 100) / Imponibile;

                    //assegno il rapporto in % tra imponibile e liquidato
                    listaProvvGrid.DataTable.SetValue("Rapporto", i, Math.Round(rapporto_pagato, 2));

                    //CALCOLO IL COSTO
                    String DocNum = listaProvvGrid.DataTable.GetValue("N° Doc.", i).ToString();
                    String tipoDoc = listaProvvGrid.DataTable.GetValue("Tipo", i).ToString();
                    calcoloProvv cp = new calcoloProvv(DocNum, tipoDoc);
                    double costo_parziale = cp.provvigioneCalcolata(dtIni, dtEnd, newNumEla, rapporto_pagato);

                    double costo_finale = ((rapporto_pagato * costo_parziale) / 100);

                    if(tipoDoc.Equals("Nc")){
                        costo_finale = costo_finale * -1;
                    }

                    //String costo = listaProvvGrid.DataTable.GetValue("Costo", i).ToString();
                    //try
                    //{
                    listaProvvGrid.DataTable.SetValue("Provv. Globale", i, Math.Round(costo_parziale, 3).ToString());
                    if (double.IsNaN(costo_finale) || Double.IsInfinity(costo_finale))
                    {
                        costo_finale = 0;
                    }
                    listaProvvGrid.DataTable.SetValue("Valore Provv.", i, Math.Round(costo_finale, 3).ToString());
                        //listaProvvGrid.DataTable.SetValue("Agg. Valore", i, Math.Round(costo_finale, 3));

                    //}
                    //catch (System.Runtime.InteropServices.COMException e)
                    //{
                        //Application.SBO_Application.MessageBox(e.ToString(), 1, "Ok", "", "");
                    //}
                }
                catch (System.Runtime.InteropServices.COMException e)
                {
                    Application.SBO_Application.MessageBox("Errore numerico, impossibile calcolare i costi provvigionali", 1, "Ok", "", "");
                }

            }

            //listaProvvGrid.Columns.Item(11).TitleObject.Sortable = true;
            //listaProvvGrid.Columns.Item(11).Type = SAPbouiCOM.BoGridColumnType.gct_CheckBox;

            //assegnaDtEffettiva();
            listaProvvGrid.AutoResizeColumns();

        }

        //ELEMENTI FORM DETTAGLIO
        private void elementiFormDettaglio()
        {
            //DISABLE COLONNE
            listaDettaglio.Columns.Item(0).Editable = false;
            listaDettaglio.Columns.Item(1).Editable = false;
            listaDettaglio.Columns.Item(2).Editable = false;
            listaDettaglio.Columns.Item(3).Editable = false;
            listaDettaglio.Columns.Item(4).Editable = false;
            listaDettaglio.Columns.Item(5).Editable = false;
            listaDettaglio.Columns.Item(6).Editable = false;
            listaDettaglio.Columns.Item(7).Editable = false;
            listaDettaglio.Columns.Item(8).Editable = false;
            listaDettaglio.Columns.Item(9).Editable = false;
            listaDettaglio.Columns.Item(10).Editable = false;
            listaDettaglio.Columns.Item(11).Editable = false;
            listaDettaglio.Columns.Item(12).Editable = false;
            listaDettaglio.Columns.Item(13).Editable = false;
            listaDettaglio.Columns.Item(14).Editable = false;
            listaDettaglio.Columns.Item(15).Editable = false;
            listaDettaglio.Columns.Item(16).Editable = false;
            listaDettaglio.Columns.Item(17).Editable = false;
            listaDettaglio.Columns.Item(18).Editable = false;
            listaDettaglio.Columns.Item(19).Editable = false;
            listaDettaglio.Columns.Item(20).Editable = false;
            listaDettaglio.Columns.Item(21).Editable = false;
            listaDettaglio.Columns.Item(22).Editable = false;
            listaDettaglio.Columns.Item(23).Editable = false;
            listaDettaglio.Columns.Item(24).Editable = false;
        }

        //DATA IN BASE A METODO PG
        private void assegnaDtEffettiva()
        {
            SAPbouiCOM.DataTable riga = listaProvvGrid.DataTable;

            int numRows = listaProvvGrid.Rows.Count;

            string[] righeIDS = new string[numRows];
            listaProvvGrid.Columns.Item(7).Editable = true;
            for (int i = 0; i < numRows; i++)
            {
                try
                {
                    if (numRows > 0)
                    {

                        String n_ft = listaProvvGrid.DataTable.GetValue("N° Doc.", i).ToString();
                        if (!n_ft.Equals("0"))
                        {


                            String metodo_pagamento = listaProvvGrid.DataTable.GetValue("Metodo Pag.", i).ToString();
                            String dt_inizio = listaProvvGrid.DataTable.GetValue("Data Ft.", i).ToString().Substring(0, 10);
                            String annoIni = dt_inizio.Substring(6, 4);
                            String meseIni = dt_inizio.Substring(3, 2);
                            String giornoIni = dt_inizio.Substring(0, 2);
                            String dt_value = annoIni + meseIni + giornoIni;

                            //String q_ft = "Select orct.\"DocDate\" as \"data_pag\",oinv.\"DocDueDate\" as \"data_ft\",oinv.\"DocStatus\" From \"ORCT\" orct,\"OINV\" oinv where oinv.\"ReceiptNum\"=orct.\"DocNum\" and oinv.\"DocNum\" = \'" + n_ft + "\' AND oinv.\"DocStatus\" = 'C' ";
                            //ECCEZIONE PER LEADING PER PESCARE LA DATA DALLA Ttabella delle fatture liquidate
                            String q_ft = "Select provv_ag.\"Data Pagamento\" as \"data_pag\" From \"PROVVIGIONI_AGENTE\" provv_ag where provv_ag.\"Numero Fattura\" = \'" + n_ft + "\'  ";
                            //CERCO LA DATA I BASE A CHIUSUSRA FATTURA
                            //String q_ft = "SELECT * FROM \"OINV\" WHERE \"DocNum\" = \'" + n_ft + "\' ";
                            oRecordSetFt.DoQuery(q_ft);
                            /*String dtUpdate = oRecordSetFt.Fields.Item("data_ft").Value.ToString();
                            String annoIniUp = dtUpdate.Substring(6, 4);
                            String meseIniUp = dtUpdate.Substring(3, 2);
                            String giornoIniUp = dtUpdate.Substring(0, 2);
                            String dt_valueUp = annoIniUp + meseIniUp + giornoIniUp;*/

                            String dtPag = oRecordSetFt.Fields.Item("data_pag").Value.ToString();
                            String dtSubConf = dtPag.Substring(0, 10);
                            if (!dtSubConf.Equals("30/12/1899"))
                            {
                                /*String annoIniUpPag = dtPag.Substring(6, 4);
                                String meseIniUpPag = dtPag.Substring(3, 2);
                                String giornoIniUpPag = dtPag.Substring(0, 2);
                                String dt_valueUpPag = annoIniUp + meseIniUp + giornoIniUp;*/
                                dtPag = dtPag.Substring(0, 11);

                                //listaProvvGrid.DataTable.SetValue("Data Effettiva", i, dt_inizio);
                                listaProvvGrid.DataTable.SetValue("Data Pag.", i, dtPag);
                                listaProvvGrid.DataTable.SetValue("Data Effettiva", i, dtPag);
                            }
                            else
                            {
                                listaProvvGrid.DataTable.SetValue("Data Pag.", i, "/");
                                listaProvvGrid.DataTable.SetValue("Data Effettiva", i, "/");
                            }


                        }
                    }

                }
                catch (System.Runtime.InteropServices.COMException e)
                {
                    //Application.SBO_Application.MessageBox(e.ToString(), 1, "Ok", "", "");
                }


            }
            listaProvvGrid.Columns.Item(7).Editable = false;
        }

        //CALCOLO RIGA DALLA PERCENTUALE
        private double costoPercentRiga(double percent, double prezzoOrdine, double qty)
        {
            double costoProvvigione = 0;

            //CALCOLO PROVVISORIO IN BASE ALLA PERCENT
            costoProvvigione = (percent * prezzoOrdine) / 100;

            return costoProvvigione;

        }

        //CALCOLO in riga incr/decr
        private double costoIncrDecr(double prezzoOrdine, double qty, double percentIncr, double percentDecr, String data, String ItemCode, String docNum)
        {

            double differenzaPercent = 0;
            //PREZZO LISTINO
            String qLis = "SELECT \"U_ListNum\" FROM \"@FC_PRLS\" ";
            oRecordSetFt.DoQuery(qLis);
            String listNum = oRecordSetFt.Fields.Item("U_ListNum").Value.ToString();
            //RECUPERO DATA ORDINE
            String docDate = "SELECT ordr.\"DocDate\" FROM \"OINV\" oinv JOIN \"INV1\" inv1 ON oinv.\"DocEntry\" = inv1.\"DocEntry\" JOIN \"ORDR\" ordr ON inv1.\"BaseEntry\" = ordr.\"DocEntry\" WHERE oinv.\"DocNum\" = \'" + docNum + "\' ";
            oRecordSetFt.DoQuery(docDate);
            String dt_inizio = oRecordSetFt.Fields.Item("DocDate").Value.ToString();
            String dt_parse = dt_inizio.Replace("/18", "/20");
            DateTime time = DateTime.Parse(dt_parse);
            DateTime newTime = time.AddDays(1);
            String annoIni = newTime.ToString().Substring(6, 4);
            String meseIni = newTime.ToString().Substring(3, 2);
            String giornoIni = newTime.ToString().Substring(0, 2);
            String annoIni_db = annoIni + meseIni + giornoIni;
            //CONTROLLO IL COSTO DA LISTINO  
            /*String qIncDec = "select sp.\"Price\" from \"OCRD\" ocrd,\"OINV\" oinv,\"STORICOPREZZI\" sp where ocrd.\"CardCode\"=oinv.\"CardCode\" AND sp.\"PriceList\"=ocrd.\"ListNum\" AND sp.\"ItemCode\" = \'" + ItemCode + "\' limit 1";
            //SE LISTINO DI RIFERIMENTO
            //String qIncDec = "select sp.\"Price\" from \"STORICOPREZZI\" sp where sp.\"ItemCode\" = \'" + ItemCode + "\' AND sp.\"PriceList\" = \'" + listNum + "\'  AND sp.\"DataInizio\" <= \'" + annoIni_db + "\' limit 1";
            oRecordSetFt.DoQuery(qIncDec);

            if (!oRecordSetFt.Fields.Item("Price").Value.ToString().Equals(""))
            {
                double prezzoListino = double.Parse((oRecordSetFt.Fields.Item("Price").Value.ToString()).Replace(".", ","));
                double differenza = (prezzoOrdine * qty) - (prezzoListino * qty);
                double importoFt = prezzoOrdine * qty;

                //SE DIFFERENZA CON LISTINO AGGIUNGO/TOLGO

                //INCREMENTO
                if (differenza > 0 && prezzoListino > 0)
                {
                    differenzaPercent = ((differenza * percentIncr) / 100) / importoFt;
                }
                //DECREMENTO
                if (differenza < 0 && prezzoListino > 0)
                {
                    differenzaPercent = ((differenza * percentDecr) / 100) / importoFt;
                }
            }*/
            return differenzaPercent;

        }

        //CALCOLO DECURTAZIONE
        private double calcoloDec(String docNum, String data)
        {
            double percentDecurt = 0;
            oRecordSetPg = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            //DATA PAGAMENTO
            String qPg = "Select orct.\"DocDate\" From \"ORCT\" orct,\"OINV\" oinv where oinv.\"ReceiptNum\"=orct.\"DocNum\" and oinv.\"DocNum\" = \'" + docNum + "\' AND oinv.\"DocStatus\" = 'C' ";
            oRecordSetPg.DoQuery(qPg);
            if (!oRecordSetPg.Fields.Item("DocDate").Value.ToString().Equals(""))
            {
                String dt_pag_db = oRecordSetPg.Fields.Item("DocDate").Value.ToString();
                String dt_parse = data.Replace("/1899", "/2099");
                DateTime dt_doc = DateTime.Parse(dt_parse);
                DateTime dt_pag = DateTime.Parse(dt_pag_db);
                double days = (dt_pag - dt_doc).TotalDays;

                //PERCENT DEC
                String qSel = "SELECT * FROM \"@FC_PRDE\" WHERE \"U_inizio\" <=  \'" + days + "\' AND \"U_fine\" >=  \'" + days + "\' ";
                oRecordSetFt.DoQuery(qSel);

                if (double.Parse(oRecordSetFt.Fields.Item("U_incr").Value.ToString()) > 0)
                {
                    percentDecurt = double.Parse(oRecordSetFt.Fields.Item("U_percent").Value.ToString());
                }
            }
            return percentDecurt;
        }

        //QUERY RICERCA PROVVIGIONI
        public void cercaProvvigioni()
        {
            this.oForm = (SAPbouiCOM.IForm)oApp.Forms.ActiveForm;
            // AGENTE
            String SlpCode = this.selectAgente.Value.Trim();
            if (!SlpCode.Equals("Scegli Agente"))
            {
                queryFiltro = qProvv();

                try
                {
                    oForm.DataSources.DataTables.Add("filtro");
                }
                catch (System.Runtime.InteropServices.COMException e)
                {
                    //Application.SBO_Application.MessageBox(e.ToString(), 1, "Ok", "", "");
                }

                //Application.SBO_Application.MessageBox(queryFiltro, 1, "Ok", "", "");
                oForm.DataSources.DataTables.Item("filtro").ExecuteQuery(queryFiltro);
                //oForm.DataSources.Columns.Add("#", SAPbouiCOM.BoFieldsType.ft_Text, 20);
                listaProvvGrid.DataTable = oForm.DataSources.DataTables.Item("filtro");

                //ASSEGNO ELEMENTI FORM
                elementiForm();

                //ASSEGNO LA DATA EFFETTIVA DI CALCOLO PROVVIGIONE
                assegnaDtEffettiva();

                this.dataEnd.Value = dtEnd;
            }
        }

        //QUERY BASE DATI PROVVIGIONI
        public string qProvv()
        {

            String qP = "";

            //FILTRO LIQUIDAZIONE
            String tipoLiquid = this.selectLiquid.Value.Trim();

            //FILTRO AGENTE
            String SlpCode = this.selectAgente.Value.Trim();
            if (!SlpCode.Equals("Tutti gli Agenti"))
            {
                filtro_agente = SlpCode;
            }
            else
            {
                filtro_agente = "%%";
            }


            //FILTRO DATA INI
            dtIni = this.dataIni.Value.Trim();

            if (!dtIni.Equals(""))
            {
                String annoIni = dtIni.Substring(0, 4);
                String meseIni = dtIni.Substring(4, 2);
                String giornoIni = dtIni.Substring(6, 2);
                String dtIniDB = annoIni + meseIni + giornoIni;
                filtro_dtIni = dtIniDB;
            }
            else
            {
                filtro_dtIni = "";
            }
            //FILTRO DATA END
            dtEnd = this.dataEnd.Value.Trim();

            if (!dtEnd.Equals(""))
            {
                String annoEnd = dtEnd.Substring(0, 4);
                String meseEnd = dtEnd.Substring(4, 2);
                String giornoEnd = dtEnd.Substring(6, 2);
                String dtEndDB = annoEnd + meseEnd + giornoEnd;
                filtro_dtEnd = dtEndDB;
            }
            else
            {
                filtro_dtEnd = "";
            }

      

            if (filtro_dtIni.Equals("") || filtro_dtEnd.Equals("") )
            {
                Application.SBO_Application.MessageBox("Inserire i campi di ricerca", 1, "Ok", "", "");
            }
            else
            {
                //TOTALE
                if (tipoLiquid.Equals("tot"))
                {
                    if (Program.HANA_DB == 1)
                    {
                        qP = "call FO_datiTestateProvvigioni('" + filtro_agente + "','" + filtro_dtIni + "','" + filtro_dtEnd + "','20150101','" + tipoLiquid + "');";
                    }
                    if (Program.SQL_DB == 1)
                    {
                        qP = "exec FO_datiTestateProvvigioni @agente = '" + filtro_agente + "',@dataIni ='" + filtro_dtIni + "',@dataEnd  ='" + filtro_dtEnd + "',@dataDoc  ='20150101',@tipoLiquid = '" + tipoLiquid + "';";
                    }
                }

                //PARZIALE
                if (tipoLiquid.Equals("par"))
                {
                    if (Program.HANA_DB == 1)
                    {
                        qP = "call FO_datiTestateProvvigioniParziale('" + filtro_agente + "','" + filtro_dtIni + "','" + filtro_dtEnd + "','20150101','" + tipoLiquid + "');"; 
                    }
                    if (Program.SQL_DB == 1)
                    {
                        qP = "exec FO_datiTestateProvvigioniParziale @agente = '" + filtro_agente + "',@dataIni ='" + filtro_dtIni + "',@dataEnd  ='" + filtro_dtEnd + "',@dataDoc  ='20150101',@tipoLiquid = '" + tipoLiquid + "';";
                    }
                   
                }

                
            }


            return qP;
        }
        
        private void Form_ActivateAfter(SAPbouiCOM.SBOItemEventArg pVal)
        {

            this.UIAPIRawForm.Freeze(true);
            //cercaProvvigioni();

            this.UIAPIRawForm.Freeze(false);

        }
        //DICHIARAZIZONI SISTEMA
        public SAPbouiCOM.Application oApp;
        public SAPbouiCOM.IForm oForm;
        public SAPbobsCOM.Company oCompany = Program.oCompany;
        public SAPbobsCOM.Recordset oRecordSet;
        public SAPbobsCOM.Recordset oRecordSetAgList;
        public SAPbobsCOM.Recordset oRecordSetDettaglio;
        public SAPbobsCOM.Recordset oRecordSetPerc;
        public SAPbobsCOM.Recordset oRecordSetNumEla;
        public SAPbobsCOM.Recordset oRecordSetFt;
        public SAPbobsCOM.Recordset oRecordSetPg;
        public SAPbouiCOM.DataTable filtroData;

        //layout
        private SAPbouiCOM.ComboBox selectAgente;
        public SAPbouiCOM.Grid listaProvvGrid;
        private SAPbouiCOM.EditText dataIni;
        private SAPbouiCOM.EditText dataEnd;
        private SAPbouiCOM.EditText totSel;
        private SAPbouiCOM.Grid listaDettaglio;
        private SAPbouiCOM.Button Button0;
        private SAPbouiCOM.Button btnLiquida;
        private SAPbouiCOM.ComboBox selectLiquid;

        //FILTRI
        String queryFiltro = "";
        String filtro_agente = "";
        String filtro_dtIni = "";
        String filtro_dtEnd = "";
        String dtIni = "";
        String dtEnd = "";
        private SAPbouiCOM.Button Button1;
        

    }
}
