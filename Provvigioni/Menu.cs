﻿using System;
using System.Collections.Generic;
using System.Text;
using SAPbouiCOM.Framework;

namespace Provvigioni
{
    class Menu
    {
        public void AddMenuItems()
        {
            SAPbouiCOM.Menus oMenus = null;
            SAPbouiCOM.MenuItem oMenuItem = null;

            oMenus = Application.SBO_Application.Menus;

            SAPbouiCOM.MenuCreationParams oCreationPackage = null;
            oCreationPackage = ((SAPbouiCOM.MenuCreationParams)(Application.SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)));
            oMenuItem = Application.SBO_Application.Menus.Item("43520"); // moudles'

            oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
            oCreationPackage.UniqueID = "Provvigioni";
            oCreationPackage.String = "Provvigioni";
            oCreationPackage.Enabled = true;
            oCreationPackage.Position = -1;

            oMenus = oMenuItem.SubMenus;

            try
            {
                //  If the manu already exists this code will fail
                oMenus.AddEx(oCreationPackage);
            }
            catch (Exception e)
            {

            }

            try
            {
                // Get the menu collection of the newly added pop-up item
                oMenuItem = Application.SBO_Application.Menus.Item("2048");
                oMenus = oMenuItem.SubMenus;

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "Gestione_Provvigioni";
                oCreationPackage.String = "Provvigioni";
                oCreationPackage.Enabled = true;
                //oCreationPackage.Image = sPath + @"\EIM_Payroll_icon.bmp";
                oMenus.AddEx(oCreationPackage);

                oMenuItem = Application.SBO_Application.Menus.Item("Gestione_Provvigioni");
                oMenus = oMenuItem.SubMenus;

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "Anag_Provvigioni";
                oCreationPackage.String = "Anag. Provvigioni";
                oCreationPackage.Enabled = true;
                //oCreationPackage.Image = sPath + @"\EIM_Payroll_icon.bmp";
                oMenus.AddEx(oCreationPackage);

                oMenuItem = Application.SBO_Application.Menus.Item("Anag_Provvigioni");
                oMenus = oMenuItem.SubMenus;

                // Create s sub menu
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;

                //oCreationPackage.UniqueID = "Provvigioni.collegamentoAnagrafica";
                //oCreationPackage.String = "Collegamento Agente/BP";
                //oMenus.AddEx(oCreationPackage);

                oCreationPackage.UniqueID = "Provvigioni.addStandardAgente";
                oCreationPackage.String = "Percentuale Standard Agente";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.UniqueID = "Provvigioni.addAgenteCliente";
                oCreationPackage.String = "Percentuale Agente/Cliente";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.UniqueID = "Provvigioni.addAgenteSettore";
                oCreationPackage.String = "Percentuale Agente/Settore";
                oMenus.AddEx(oCreationPackage);
                
                oCreationPackage.UniqueID = "Provvigioni.addArtRivendita";
                oCreationPackage.String = "Percentuale Agente/Art. Rivendita/Prod.";
                oMenus.AddEx(oCreationPackage);
                
                oCreationPackage.UniqueID = "Provvigioni.addGruppoArticoli";
                oCreationPackage.String = "Percentuale Agente/Gruppo Articoli";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.UniqueID = "Provvigioni.addGrpArtCliente";
                oCreationPackage.String = "Percentuale Agente/Gruppo Art./Clienti";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.UniqueID = "Provvigioni.addGrpOmogeneo";
                oCreationPackage.String = "Percentuale Agente/Gruppo Omogeneo";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.UniqueID = "Provvigioni.addGrpOmogCliente";
                oCreationPackage.String = "Percentuale Agente/Gruppo Omog./Clienti";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.UniqueID = "Provvigioni.addAgenteArticolo";
                oCreationPackage.String = "Percentuale Agente/Articolo";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.UniqueID = "Provvigioni.addAgenteClienteArticolo";
                oCreationPackage.String = "Percentuale Agente/Cliente/Articolo";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.UniqueID = "Provvigioni.addBusinessUnit";
                oCreationPackage.String = "Percentuale Agente/Business Unit";
                oMenus.AddEx(oCreationPackage);


                //ENASARCO / FIRR
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "Anag_EnaFirr";
                oCreationPackage.String = "Anag. Ena./Firr";
                oCreationPackage.Enabled = true;
                //oCreationPackage.Image = sPath + @"\EIM_Payroll_icon.bmp";
                oMenus.AddEx(oCreationPackage);

                oMenuItem = Application.SBO_Application.Menus.Item("Anag_EnaFirr");
                oMenus = oMenuItem.SubMenus;

                // Create s sub menu
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;

                /*oCreationPackage.UniqueID = "Provvigioni.Enasarco";
                oCreationPackage.String = "Enasarco";
                oMenus.AddEx(oCreationPackage);*/

                oCreationPackage.UniqueID = "Provvigioni.paramFirr";
                oCreationPackage.String = "Parametri Firr";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.UniqueID = "Provvigioni.paramEnasarco";
                oCreationPackage.String = "Parametri Ena.";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.UniqueID = "Provvigioni.Firr";
                oCreationPackage.String = "Calcolo Firr";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.UniqueID = "Provvigioni.Enasarco";
                oCreationPackage.String = "Calcolo Ena.";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.UniqueID = "Provvigioni.firrLiquidato";
                oCreationPackage.String = "Firr Liquidato";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.UniqueID = "Provvigioni.Enasarco Liquidato";
                oCreationPackage.String = "Enasarco Liquidato";
                oMenus.AddEx(oCreationPackage);

                oMenuItem = Application.SBO_Application.Menus.Item("Gestione_Provvigioni");
                oMenus = oMenuItem.SubMenus;

                //oCreationPackage.UniqueID = "Provvigioni.selDecurtazione";
                //oCreationPackage.String = "Percentuali/Periodi Decurtazione";
                //oMenus.AddEx(oCreationPackage);

                /*oCreationPackage.UniqueID = "Provvigioni.anagrafica";
                oCreationPackage.String = "Anagrafica Provvigione";
                oMenus.AddEx(oCreationPackage);*/

                

                oCreationPackage.UniqueID = "Provvigioni.rimuoviArticoloX";
                oCreationPackage.String = "Rimuovi Articoli";
                oMenus.AddEx(oCreationPackage);

                //COMMENTATO PER LEADING
                /*oCreationPackage.UniqueID = "Provvigioni.assegnaListino";
                oCreationPackage.String = "Listino di Riferimento";
                oMenus.AddEx(oCreationPackage);*/

                oCreationPackage.UniqueID = "Provvigioni.listaProvvigioni";
                oCreationPackage.String = "Lista Provvigioni";
                oMenus.AddEx(oCreationPackage);
                /*
                oCreationPackage.UniqueID = "Provvigioni.pagamentoProvvigioniVeloce";
                oCreationPackage.String = "Pagamento Provvigioni";
                oMenus.AddEx(oCreationPackage);
                */
                oCreationPackage.UniqueID = "Provvigioni.pgProvv";
                oCreationPackage.String = "Dettaglio Pg. Provvigioni";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.UniqueID = "Provvigioni.provvElaborate";
                oCreationPackage.String = "Provvigioni Liquidate";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.UniqueID = "Provvigioni.provvMaturande";
                oCreationPackage.String = "Emesse";
                oMenus.AddEx(oCreationPackage);

                
                
            }
            catch (Exception er)
            { //  Menu already exists
                //Application.SBO_Application.MessageBox(er.ToString(), 1, "Ok", "", "");
                Application.SBO_Application.SetStatusBarMessage("Menu Already Exists", SAPbouiCOM.BoMessageTime.bmt_Short, true);
            }
        }

        public void SBO_Application_MenuEvent(ref SAPbouiCOM.MenuEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;

            try
            {
                //Please replace following 3 "Form1" with real form class in current project
                /*if (pVal.BeforeAction && pVal.MenuUID == "Provvigioni.anagrafica")
                {
                    Anagrafica activeForm = new Anagrafica();
                    activeForm.Show();
                }*/
                if (pVal.BeforeAction && pVal.MenuUID == "Provvigioni.listaProvvigioni")
                {
                    listaProvvigioni_b1f activeForm = new listaProvvigioni_b1f();
                    activeForm.Show();
                }
                //if (pVal.BeforeAction && pVal.MenuUID == "Provvigioni.collegamentoAnagrafica")
                //{
                //    collegamentoAnagrafica_b1f activeForm = new collegamentoAnagrafica_b1f();
                //    activeForm.Show();
                //}
                if (pVal.BeforeAction && pVal.MenuUID == "Provvigioni.addStandardAgente")
                {
                    addStandardAgente_b1f activeForm = new addStandardAgente_b1f();
                    activeForm.Show();
                }
                if (pVal.BeforeAction && pVal.MenuUID == "Provvigioni.addAgenteSettore")
                {
                    addAgenteSettore_b1f activeForm = new addAgenteSettore_b1f();
                    activeForm.Show();
                }
                if (pVal.BeforeAction && pVal.MenuUID == "Provvigioni.addArtRivendita")
                {
                    addArtRivendita_b1f activeForm = new addArtRivendita_b1f();
                    activeForm.Show();
                }
                if (pVal.BeforeAction && pVal.MenuUID == "Provvigioni.addGruppoArticoli")
                {
                    addGruppoArticoli_b1f activeForm = new addGruppoArticoli_b1f();
                    activeForm.Show();
                }
                if (pVal.BeforeAction && pVal.MenuUID == "Provvigioni.addAgenteCliente")
                {
                    addAgenteCliente_b1f activeForm = new addAgenteCliente_b1f();
                    activeForm.Show();
                }
                if (pVal.BeforeAction && pVal.MenuUID == "Provvigioni.addAgenteArticolo")
                {
                    addAgenteArticolo_b1f activeForm = new addAgenteArticolo_b1f();
                    activeForm.Show();
                }
                if (pVal.BeforeAction && pVal.MenuUID == "Provvigioni.addGrpArtCliente")
                {
                    addGrpArtCliente_b1f activeForm = new addGrpArtCliente_b1f();
                    activeForm.Show();
                }
                if (pVal.BeforeAction && pVal.MenuUID == "Provvigioni.addAgenteClienteArticolo")
                {
                    addAgenteClienteArticolo_b1f activeForm = new addAgenteClienteArticolo_b1f();
                    activeForm.Show();
                }
                if (pVal.BeforeAction && pVal.MenuUID == "Provvigioni.addGrpOmogeneo")
                {
                    addGrpOmogeneo_b1f activeForm = new addGrpOmogeneo_b1f();
                    activeForm.Show();
                }
                if (pVal.BeforeAction && pVal.MenuUID == "Provvigioni.addGrpOmogCliente")
                {
                    addGrpOmogCliente_b1f activeForm = new addGrpOmogCliente_b1f();
                    activeForm.Show();
                }
                if (pVal.BeforeAction && pVal.MenuUID == "Provvigioni.addBusinessUnit")
                {
                    addBusinessUnit_b1f activeForm = new addBusinessUnit_b1f();
                    activeForm.Show();
                }
                /*if (pVal.BeforeAction && pVal.MenuUID == "Provvigioni.pagamentoProvvigioniVeloce")
                {
                    pagamentoProvvigioniVeloce_b1f activeForm = new pagamentoProvvigioniVeloce_b1f();
                    activeForm.Show();
                }*/
                if (pVal.BeforeAction && pVal.MenuUID == "Provvigioni.pgProvv")
                {
                    pgProvv_b1f activeForm = new pgProvv_b1f();
                    activeForm.Show();
                }
                if (pVal.BeforeAction && pVal.MenuUID == "Provvigioni.provvElaborate")
                {
                    provvElaborate_b1f activeForm = new provvElaborate_b1f();
                    activeForm.Show();
                }
                //COMMENTATO PER LEADING
                /*if (pVal.BeforeAction && pVal.MenuUID == "Provvigioni.assegnaListino")
                {
                    assegnaListino_b1f activeForm = new assegnaListino_b1f();
                    activeForm.Show();
                }*/
                if (pVal.BeforeAction && pVal.MenuUID == "Provvigioni.rimuoviArticoloX")
                {
                    rimuoviArticoliPr_b1f activeForm = new rimuoviArticoliPr_b1f();
                    activeForm.Show();
                }
                /*if (pVal.BeforeAction && pVal.MenuUID == "Provvigioni.Enasarco")
                {
                    Enasarco_b1f activeForm = new Enasarco_b1f();
                    activeForm.Show();
                }*/
                if (pVal.BeforeAction && pVal.MenuUID == "Provvigioni.paramFirr")
                {
                    paramFirr_b1f activeForm = new paramFirr_b1f();
                    activeForm.Show();
                }
                if (pVal.BeforeAction && pVal.MenuUID == "Provvigioni.paramEnasarco")
                {
                    paramEnasarco_b1f activeForm = new paramEnasarco_b1f();
                    activeForm.Show();
                }
                if (pVal.BeforeAction && pVal.MenuUID == "Provvigioni.Firr")
                {
                    Firr_b1f activeForm = new Firr_b1f();
                    activeForm.Show();
                }
                if (pVal.BeforeAction && pVal.MenuUID == "Provvigioni.Enasarco")
                {
                    Enasarco_b1f activeForm = new Enasarco_b1f();
                    activeForm.Show();
                }
                if (pVal.BeforeAction && pVal.MenuUID == "Provvigioni.firrLiquidato")
                {
                    firrLiquidato_b1f activeForm = new firrLiquidato_b1f();
                    activeForm.Show();
                }
                if (pVal.BeforeAction && pVal.MenuUID == "Provvigioni.Enasarco Liquidato")
                {
                    Enasarco_Liquidato_b1f activeForm = new Enasarco_Liquidato_b1f();
                    activeForm.Show();
                }
                if (pVal.BeforeAction && pVal.MenuUID == "Provvigioni.provvMaturande")
                {
                    provvMaturande_b1f activeForm = new provvMaturande_b1f();
                    activeForm.Show();
                }

                //if (pVal.BeforeAction && pVal.MenuUID == "Provvigioni.selDecurtazione")
                //{
                //    selDecurtazione_b1f activeForm = new selDecurtazione_b1f();
                //    activeForm.Show();
                //}
            }

            catch (Exception ex)
            {
                Application.SBO_Application.MessageBox(ex.ToString(), 1, "Ok", "", "");
            }
        }

    }
}
