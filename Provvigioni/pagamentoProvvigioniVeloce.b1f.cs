
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace Provvigioni
{

    [FormAttribute("Provvigioni.pagamentoProvvigioniVeloce_b1f", "pagamentoProvvigioniVeloce.b1f")]
    class pagamentoProvvigioniVeloce_b1f : UserFormBase
    {
        public pagamentoProvvigioniVeloce_b1f()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }
        private void OnCustomInitialize()
        {
            this.oApp = (SAPbouiCOM.Application)Application.SBO_Application;
            this.oForm = this.UIAPIRawForm;
            this.oRecordSet = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.oRecordSetAgList = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.oRecordSetPerc = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.oRecordSetDettaglio = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.selectAgente = ((SAPbouiCOM.ComboBox)(this.GetItem("selectAg").Specific));
            this.dataIni = ((SAPbouiCOM.EditText)(this.GetItem("dataIni").Specific));
            this.dataEnd = ((SAPbouiCOM.EditText)(this.GetItem("dataEnd").Specific));

            //LOGICA LAYOUT
            //POPOLO DDL AGENTI
            selectAgente.ValidValues.Add("Scegli Agente", "Scegli Agente");
            String qAgenti = "SELECT * FROM \"OSLP\" order by \"SlpName\" ASC ";
            oRecordSetAgList.DoQuery(qAgenti);
            while (!oRecordSetAgList.EoF)
            {
                selectAgente.ValidValues.Add(oRecordSetAgList.Fields.Item("SlpCode").Value.ToString(), oRecordSetAgList.Fields.Item("SlpName").Value.ToString());
                oRecordSetAgList.MoveNext();
            }

            selectAgente.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
            selectAgente.Select("Scegli Agente", SAPbouiCOM.BoSearchKey.psk_ByValue);

        }
        //DICHIARAZIZONI SISTEMA
        public SAPbouiCOM.Application oApp;
        public SAPbouiCOM.IForm oForm;
        public SAPbobsCOM.Company oCompany = Program.oCompany;
        public SAPbobsCOM.Recordset oRecordSet;
        public SAPbobsCOM.Recordset oRecordSetAgList;
        public SAPbobsCOM.Recordset oRecordSetDettaglio;
        public SAPbobsCOM.Recordset oRecordSetPerc;

        //layout
        private SAPbouiCOM.ComboBox selectAgente;
        private SAPbouiCOM.EditText dataIni;
        private SAPbouiCOM.EditText dataEnd;

        //FILTRI
        String queryFiltro = "";
        String filtro_agente = "";
        String filtro_dtIni = "";
        String filtro_dtEnd = "";

    }
}
