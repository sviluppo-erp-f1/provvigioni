
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace Provvigioni
{

    [FormAttribute("Provvigioni.liquidaProvv_b1f", "popForms/liquidaProvv.b1f")]
    class liquidaProvv_b1f : UserFormBase
    {
        string[] _righeIDS;
        string[] _righeDocEntry;
        string[] _righeCostoGlobale;
        string[] _righeCosto;
        string[] _righeLiquidato;
        string[] _righeAgente;
        string[] _righeTipo;
        string[] _righeCheck;
        public liquidaProvv_b1f(string[] righeIDS, string[] righeDocEntry, string[] righeCostoGlobale, string[] righeLiquidato, string[] righeCosto, string[] righeAgente, string[] righeTipo, string[] righeCheck)
        {
            _righeIDS = righeIDS;
            _righeDocEntry = righeDocEntry;
            _righeCostoGlobale = righeCostoGlobale;
            _righeCosto = righeCosto;
            _righeLiquidato = righeLiquidato;
            _righeAgente = righeAgente;
            _righeTipo = righeTipo;
            _righeCheck = righeCheck;
            oApp = (SAPbouiCOM.Application)Application.SBO_Application;
            this.oForm = ((SAPbouiCOM.Form)(this.oApp.Forms.ActiveForm));
            this.totale = ((SAPbouiCOM.EditText)(this.GetItem("Item_2").Specific));
            this.btnLiquida = ((SAPbouiCOM.Button)(this.GetItem("Item_4").Specific));
            this.btnAnnulla = ((SAPbouiCOM.Button)(this.GetItem("Item_5").Specific));

            //creo colonne griglia
            int ll = _righeIDS.Length;
            //popolo la griglia
            //griglia.DataTable.Columns.Add("N° Doc.", SAPbouiCOM.BoFieldsType.ft_Text, 20);
            double totProvv = 0;
            for (int k = 0; k < _righeIDS.Length; k++)
            {
                if (_righeCheck[k].ToString().Equals("y"))
                {
                    double costo_riga = double.Parse((_righeCosto[k].ToString()).Replace(".", ","));
                    totProvv = totProvv + costo_riga;
                }
            }

            totale.Value = totProvv.ToString();
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }

        private void OnCustomInitialize()
        {
            this.oApp = (SAPbouiCOM.Application)Application.SBO_Application;
            //this.oForm = (SAPbouiCOM.Form)oApp.Forms.ActiveForm;this.UIAPIRawForm.
            this.oForm = this.UIAPIRawForm;
            this.totale = ((SAPbouiCOM.EditText)(this.GetItem("Item_2").Specific));
            this.btnLiquida = ((SAPbouiCOM.Button)(this.GetItem("Item_4").Specific));
            this.btnAnnulla = ((SAPbouiCOM.Button)(this.GetItem("Item_5").Specific));
            this.btnLiquida.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.Button0_ClickBefore);
            this.btnAnnulla.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.Button1_ClickBefore);
            this.AnnoEla = ((SAPbouiCOM.EditText)(this.GetItem("Item_8").Specific));
            this.PeriodoEla = ((SAPbouiCOM.EditText)(this.GetItem("Item_9").Specific));
            this.NumEla = ((SAPbouiCOM.EditText)(this.GetItem("Item_11").Specific));
            this.dtInizio = ((SAPbouiCOM.EditText)(this.GetItem("Item_14").Specific));
            this.dtFine = ((SAPbouiCOM.EditText)(this.GetItem("Item_15").Specific));
            this.ddlTrimestri = ((SAPbouiCOM.ComboBox)(this.GetItem("Item_17").Specific));

            aggiornaDati();
        }
        //LIQUIDO
        private void Button0_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            this.oForm = (SAPbouiCOM.IForm)oApp.Forms.ActiveForm;
            oRecordSet = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            oRecordSetAg = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            oRecordSetFt = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));

            this.AnnoEla = ((SAPbouiCOM.EditText)(this.GetItem("Item_8").Specific));
            this.PeriodoEla = ((SAPbouiCOM.EditText)(this.GetItem("Item_9").Specific));
            this.NumEla = ((SAPbouiCOM.EditText)(this.GetItem("Item_11").Specific));
            String numeroEla = AnnoEla.Value.ToString() + "_" + PeriodoEla.Value.ToString() + "_" + NumEla.Value.ToString();
            String periodo = ddlTrimestri.Value.ToString();
            //CONTROLLO DATI
            if (PeriodoEla.Value.ToString().Equals(""))
            {
                //manca periodo ela
                Application.SBO_Application.MessageBox("Inserire un periodo per il numero di elaborazione.", 1, "Ok", "", "");
            }
            else
            {
               
                for (int i = 0; i < _righeIDS.Length; i++)
                {
                    try
                    {
                        if (_righeCheck[i].ToString().Equals("y"))
                        {
                            
                            int newCode = 0;
                            int incr = 0;
                            

                            //RECUPERO E CONVERTO ULTIMO Code
                            String qLastCodeB = "SELECT " + Program.SQL_limit1 + " * FROM \"@FC_ANTP\"  ORDER BY \"U_incr\" DESC " + Program.HANA_limit1 + " ";
                            oRecordSet.DoQuery(qLastCodeB);
                            if (oRecordSet.Fields.Item("Code").Value.ToString().Equals(""))
                            {
                                newCode = 1000001;
                                incr = 1;
                            }
                            else
                            {
                                newCode = Int32.Parse(oRecordSet.Fields.Item("Code").Value.ToString());
                                newCode = newCode + 1;
                                incr = Int32.Parse(oRecordSet.Fields.Item("U_incr").Value.ToString()) + 1;
                            }
                            String newCodeDb = newCode.ToString();
                            String oggi = DateTime.Now.ToString("dd/MM/yyyy");
                            String slpName = _righeAgente[i].ToString();

                            //SLPCODE
                            String qAg = "SELECT \"SlpCode\" FROM \"OSLP\"  WHERE \"SlpName\" = '" + slpName + "' ";
                            oRecordSetAg.DoQuery(qAg);

                            //TOTALE DOCUMENTO
                            String qFt = "";
                            if (_righeTipo[i].ToString().Equals("Ft"))
                            {
                                qFt = "SELECT \"DocTotal\",\"DocEntry\" FROM \"OINV\" WHERE \"DocNum\" = '" + _righeIDS[i].ToString() + "' ";
                            }
                            else
                            {
                                qFt = "SELECT \"DocTotal\",\"DocEntry\" FROM \"ORIN\" WHERE \"DocNum\" = '" + _righeIDS[i].ToString() + "' ";
                            }
                            
                            oRecordSetFt.DoQuery(qFt);

                            //DATE PERIODO
                            String annoIni_db = "";
                            String dt_inizio = this.dtInizio.Value;
                            if (!dt_inizio.Equals(""))
                            {
                                String annoIni = dt_inizio.Substring(0, 4);
                                String meseIni = dt_inizio.Substring(4, 2);
                                String giornoIni = dt_inizio.Substring(6, 2);
                                annoIni_db = giornoIni + "/" + meseIni + "/" + annoIni;
                                String annoIni_confronto = annoIni + meseIni + giornoIni;
                            }
                            String annoEnd_db = "";
                            String dt_fine = this.dtFine.Value;
                            if (!dt_fine.Equals(""))
                            {
                                String annoEnd = dt_fine.Substring(0, 4);
                                String meseEnd = dt_fine.Substring(4, 2);
                                String giornoEnd = dt_fine.Substring(6, 2);
                                annoEnd_db = giornoEnd + "/" + meseEnd + "/" + annoEnd;
                                String annoEnd_confronto = annoEnd + meseEnd + giornoEnd;
                            }
                            
                            
                            String totFattura = oRecordSetFt.Fields.Item("DocTotal").Value.ToString().Replace(",",".");
                            SAPbobsCOM.UserTable oUserTableANAG;
                            oUserTableANAG = oCompany.UserTables.Item("FC_ANTP");
                            oUserTableANAG.Code = newCodeDb;
                            oUserTableANAG.Name = "elaborazione_" + newCodeDb;
                            oUserTableANAG.UserFields.Fields.Item("U_n_fattura").Value = _righeIDS[i].ToString();
                            oUserTableANAG.UserFields.Fields.Item("U_fatturaDocEntry").Value = _righeDocEntry[i].ToString();
                            oUserTableANAG.UserFields.Fields.Item("U_costo_str").Value = _righeCosto[i].ToString();
                            oUserTableANAG.UserFields.Fields.Item("U_tipo").Value = _righeTipo[i].ToString();
                            oUserTableANAG.UserFields.Fields.Item("U_totaleLiq").Value = _righeLiquidato[i].ToString().Replace(",", ".");
                            oUserTableANAG.UserFields.Fields.Item("U_SlpName").Value = _righeAgente[i].ToString();
                            oUserTableANAG.UserFields.Fields.Item("U_SlpCode").Value = oRecordSetAg.Fields.Item("SlpCode").Value.ToString();
                            oUserTableANAG.UserFields.Fields.Item("U_data_pagamento").Value = oggi;
                            oUserTableANAG.UserFields.Fields.Item("U_incr").Value = incr;
                            oUserTableANAG.UserFields.Fields.Item("U_numEla").Value = numeroEla;
                            oUserTableANAG.UserFields.Fields.Item("U_periodoEla").Value = periodo;
                            oUserTableANAG.UserFields.Fields.Item("U_annoEla").Value = AnnoEla.Value.ToString();
                            oUserTableANAG.UserFields.Fields.Item("U_progEla").Value = NumEla.Value.ToString();
                            oUserTableANAG.UserFields.Fields.Item("U_totaleEla").Value = totale.Value.ToString();
                            oUserTableANAG.UserFields.Fields.Item("U_totaleFt").Value = totFattura;
                            oUserTableANAG.UserFields.Fields.Item("U_data_inizio").Value = annoIni_db;
                            oUserTableANAG.UserFields.Fields.Item("U_data_fine").Value = annoEnd_db;
                            oUserTableANAG.UserFields.Fields.Item("U_totaleImpProvv").Value = _righeCosto[i].ToString().Replace(",", ".");
                            oUserTableANAG.UserFields.Fields.Item("U_provvGlobale").Value = _righeCostoGlobale[i].ToString().Replace(",", ".");
                            int result = oUserTableANAG.Add();
                        }
                    }
                    catch (System.Runtime.InteropServices.COMException e)
                    {
                        //Application.SBO_Application.MessageBox(e.ToString(), 1, "Ok", "", "");
                    }

                }
                SAPbouiCOM.Framework.Application.SBO_Application.MessageBox("Registrazione liquidazione provvigioni elaborata.", 1, "Ok", "", "");
                oForm = (SAPbouiCOM.Form)oApp.Forms.ActiveForm;
                oForm.Close();
            }
            

        }

        
        //ANNULLO
        private void Button1_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            this.oForm = (SAPbouiCOM.IForm)oApp.Forms.ActiveForm;
            oForm.Close();

        }

        //DATI STATICI
        private void aggiornaDati()
        {
            this.oForm = (SAPbouiCOM.IForm)oApp.Forms.ActiveForm;
            oRecordSet = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.AnnoEla = ((SAPbouiCOM.EditText)(this.GetItem("Item_8").Specific));
            this.PeriodoEla = ((SAPbouiCOM.EditText)(this.GetItem("Item_9").Specific));
            this.NumEla = ((SAPbouiCOM.EditText)(this.GetItem("Item_11").Specific));

            string anno = DateTime.Now.ToString("yyyy").ToString();
            AnnoEla.Value = anno;

            //RECUPERO E CONVERTO ULTIMO Code

            int newCode = 0;
            String qLastCodeB = "SELECT " + Program.SQL_limit1 + " * FROM \"@FC_ANTP\"  ORDER BY \"U_incr\" DESC " + Program.HANA_limit1 + " ";
            oRecordSet.DoQuery(qLastCodeB);
            if (oRecordSet.Fields.Item("U_progEla").Value.ToString().Equals(""))
            {
                newCode = 1000001;
            }
            else
            {
                newCode = Int32.Parse(oRecordSet.Fields.Item("U_progEla").Value.ToString());
                newCode = newCode + 1;
            }

            NumEla.Value = newCode.ToString();

        }
        //DICHIARAZIZONI SISTEMA
        public SAPbouiCOM.Application oApp;
        public SAPbouiCOM.IForm oForm;
        public SAPbobsCOM.Company oCompany = Program.oCompany;
        public SAPbobsCOM.Recordset oRecordSet;
        public SAPbobsCOM.Recordset oRecordSetAg;
        public SAPbobsCOM.Recordset oRecordSetFt;
        
        //layout
        private SAPbouiCOM.EditText totale;
        private SAPbouiCOM.Button btnLiquida;
        private SAPbouiCOM.Button btnAnnulla;
        private SAPbouiCOM.Button Button0;
        private SAPbouiCOM.Button Button1;
        private SAPbouiCOM.EditText AnnoEla;
        private SAPbouiCOM.EditText PeriodoEla;
        private SAPbouiCOM.EditText NumEla;
        private SAPbouiCOM.EditText dtInizio;
        private SAPbouiCOM.EditText dtFine;
        private SAPbouiCOM.ComboBox ddlTrimestri;
        

    }
}
