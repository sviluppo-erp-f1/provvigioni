
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace Provvigioni
{

    [FormAttribute("Provvigioni.eliminaProvvigione_b1f", "popForms/eliminaProvvigione.b1f")]
    class eliminaProvvigione_b1f : UserFormBase
    {
        string[] _righeIDS;
        public eliminaProvvigione_b1f(string[] righeIDS)
        {
            _righeIDS = righeIDS; 
            this.oApp = (SAPbouiCOM.Application)Application.SBO_Application;
            //this.oForm = (SAPbouiCOM.Form)oApp.Forms.ActiveForm;this.UIAPIRawForm.
            this.oForm = this.UIAPIRawForm;
            oRecordSet = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.Button2 = ((SAPbouiCOM.Button)(this.GetItem("Item_2").Specific));
            this.Button2.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.Button2_ClickBefore);
            this.Button3 = ((SAPbouiCOM.Button)(this.GetItem("Item_1").Specific));
            this.Button3.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.Button3_ClickBefore);
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }

        
        //ELIMINA
        private void Button3_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            oForm = (SAPbouiCOM.Form)oApp.Forms.ActiveForm;
            oRecordSet = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            for (int i = 0; i < _righeIDS.Length;i++ )
            {
                
                String qDati = "DELETE FROM \"@FC_ANAG\" WHERE  \"Code\" = \'" + _righeIDS[i] + "\' ";
                oRecordSet.DoQuery(qDati);
                
            }
            oForm = (SAPbouiCOM.IForm)oApp.Forms.ActiveForm;
            oForm.Close();
        }

        //ANNULLA
        private void Button2_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            oForm = (SAPbouiCOM.IForm)oApp.Forms.ActiveForm;
            oForm.Close();

        }

        private void OnCustomInitialize()
        {

        }

        //DICHIARAZIZONI SISTEMA
        public SAPbouiCOM.Application oApp;
        public SAPbouiCOM.IForm oForm;
        public SAPbobsCOM.Company oCompany = Program.oCompany;
        public SAPbobsCOM.Recordset oRecordSet;

        //TEMPLATE FORM
        private SAPbouiCOM.Button Button2;
        private SAPbouiCOM.Button Button3;

    }
}
