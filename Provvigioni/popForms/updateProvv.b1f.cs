
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace Provvigioni
{

    [FormAttribute("Provvigioni.updateProvv_b1f", "popForms/updateProvv.b1f")]
    class updateProvv_b1f : UserFormBase
    {
        String _code = "";
        String _dataProvv = "";
        String _tabella = "";
        String annoIni_db = "";
        public updateProvv_b1f(String code, String dataProvv, String tabella)
        {
            _code = code;
            _dataProvv = dataProvv;
            _tabella = tabella;
            this.oApp = (SAPbouiCOM.Application)Application.SBO_Application;
            //this.oForm = (SAPbouiCOM.Form)oApp.Forms.ActiveForm;this.UIAPIRawForm.
            this.oForm = this.UIAPIRawForm;
            this.btnAdd = ((SAPbouiCOM.Button)(this.GetItem("btnAdd").Specific));
            this.btnStop = ((SAPbouiCOM.Button)(this.GetItem("btnStop").Specific));
            this.strPerc = ((SAPbouiCOM.StaticText)(this.GetItem("strPerc").Specific));
            this.strDataPre = ((SAPbouiCOM.StaticText)(this.GetItem("strDataPre").Specific));
            this.strDataAft = ((SAPbouiCOM.StaticText)(this.GetItem("strDataAft").Specific));
            oForm = (SAPbouiCOM.Form)oApp.Forms.ActiveForm;
            oRecordSet = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            String qDati = "SELECT * FROM \"@FC_ANAG\" WHERE  \"Code\" = \'" + _code + "\' ";
            //Application.SBO_Application.MessageBox(qDati, 1, "Ok", "", "");
            oRecordSet.DoQuery(qDati);
            String perc = oRecordSet.Fields.Item("U_percent").Value.ToString();
            String dataPrima = oRecordSet.Fields.Item("U_dtEND").Value.ToString();
            String dataDopo = _dataProvv;
            String annoIni = dataDopo.Substring(6, 4);
            String meseIni = dataDopo.Substring(3, 2);
            String giornoIni = dataDopo.Substring(0, 2);
            
            annoIni_db = annoIni+meseIni+giornoIni;
            //Application.SBO_Application.MessageBox(dataDopo + " - " + annoIni_db, 1, "Ok", "", "");
            strPerc.Caption = perc + "%";
            strDataPre.Caption = dataPrima.Substring(0,10);
            strDataAft.Caption = dataDopo.Substring(0, 10);

            //PREMO OK PER AGGIUNGERE COLLEGAMENTO
            this.btnAdd.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.btnOk_ClickBefore);

            //CHIUDO FORM
            this.btnStop.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.btnStop_ClickBefore);
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            //   INIZIALIZZO
            this.OnCustomInitialize();

        }

        //AGGIUNGO COLLEGAMENTO AGENTE/BP
        private void btnOk_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            this.oForm = (SAPbouiCOM.IForm)oApp.Forms.ActiveForm;
            oRecordSet = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            String qUpdate = "UPDATE \""+_tabella+"\" SET \"U_dtEND\" = \'" + annoIni_db + "\' , \"U_attivo\"='no'  WHERE  \"Code\" = \'" + _code + "\' ";
            oRecordSet.DoQuery(qUpdate);
            oForm = (SAPbouiCOM.Form)oApp.Forms.ActiveForm;
            oForm.Close();
        }

        //DISMISS
        private void btnStop_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            this.oForm = (SAPbouiCOM.IForm)oApp.Forms.ActiveForm;
            oForm.Close();
        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }

        private void OnCustomInitialize()
        {

        }

        //DICHIARAZIZONI SISTEMA
        public SAPbouiCOM.Application oApp;
        public SAPbouiCOM.IForm oForm;
        public SAPbobsCOM.Company oCompany = Program.oCompany;
        public SAPbobsCOM.Recordset oRecordSet;

        //TEMPLATE FORM
        private SAPbouiCOM.Button btnAdd;
        private SAPbouiCOM.Button btnStop;
        private SAPbouiCOM.StaticText strPerc;
        private SAPbouiCOM.StaticText strDataPre;
        private SAPbouiCOM.StaticText strDataAft;
        private SAPbouiCOM.StaticText StaticText6;

    }
}
