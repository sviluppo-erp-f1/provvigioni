
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace Provvigioni
{

    [FormAttribute("Provvigioni.addGrpOmogCliente_b1f", "params/addGrpOmogCliente.b1f")]
    class addGrpOmogCliente_b1f : UserFormBase
    {
        public addGrpOmogCliente_b1f()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.OnCustomInitialize();
        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }


        private void OnCustomInitialize()
        {
            this.oApp = (SAPbouiCOM.Application)Application.SBO_Application;
            this.oForm = this.UIAPIRawForm;
            this.oRecordSet = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.oRecordSetInsert = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.oRecordSetDati = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));

            this.selectAg = ((SAPbouiCOM.EditText)(this.GetItem("Item_5").Specific));
            this.selectPerc = ((SAPbouiCOM.EditText)(this.GetItem("Item_7").Specific));
            this.dtInizio = ((SAPbouiCOM.EditText)(this.GetItem("Item_8").Specific));
            this.dtFine = ((SAPbouiCOM.EditText)(this.GetItem("Item_9").Specific));
            this.selectCliente = ((SAPbouiCOM.EditText)(this.GetItem("Item_13").Specific));
            this.btnAdd = ((SAPbouiCOM.Button)(this.GetItem("Item_10").Specific));
            this.btnStop = ((SAPbouiCOM.Button)(this.GetItem("Item_11").Specific));
            this.selectGrpOmog = ((SAPbouiCOM.ComboBox)(this.GetItem("Item_6").Specific));

            //setto data fine ad infinito
            dtFine.Value = "29991231";

            // DDL SETTORI
            this.ddlGrpOmog();

            //         PREMO OK PER AGGIUNGERE COLLEGAMENTO
            this.btnAdd.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.btnOk_ClickBefore);
            //         CHIUDO FORM
            this.btnStop.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.btnStop_ClickBefore);
        }

        //AGGIUNGO COLLEGAMENTO AGENTE/SETTORE
        private void btnOk_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            oForm = (SAPbouiCOM.Form)oApp.Forms.ActiveForm;
            oRecordSet = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));

            int inserisco = 1;
            int newCode = 0;
            int newCodeANAG = 0;
            String prefix = "percGrpOmogCl_";
            if (oForm.Mode == SAPbouiCOM.BoFormMode.fm_ADD_MODE)
            {
                try
                {
                    String perc = this.selectPerc.Value;

                    //CONTROLLI INTEGRITA DATI
                    //controllo se perc è numerico
                    float controllo_perc;
                    bool isNumeric = float.TryParse(perc, out controllo_perc);

                    //dati obbligatori
                    if (this.selectAg.Value.Equals("") || this.selectPerc.Value.Equals("") || this.dtInizio.Value.Equals("") || this.selectGrpOmog.Value.Equals("") || this.selectGrpOmog.Value.Equals(gruppo_base) || isNumeric == false)
                    {
                        //dati mancanti
                        Application.SBO_Application.MessageBox("Inserire i dati obbligatori e controllare che la percentuale sia numerica", 1, "Ok", "", "");
                    }
                    else
                    {
                        String ag = this.selectAg.Value;
                        String grpOmog = this.selectGrpOmog.Value;
                        String cliente = this.selectCliente.Value;

                        String dt_inizio = this.dtInizio.Value;
                        String annoIni = dt_inizio.Substring(0, 4);
                        String meseIni = dt_inizio.Substring(4, 2);
                        String giornoIni = dt_inizio.Substring(6, 2);
                        String annoIni_db = giornoIni + "/" + meseIni + "/" + annoIni;
                        String annoIni_confronto = annoIni + meseIni + giornoIni;
                        DateTime enteredDate = DateTime.Parse(annoIni_db);
                        enteredDate = enteredDate.AddDays(-1);
                        String dataIeri = enteredDate.ToString();
                        String annoIniIeri = dataIeri.Substring(0, 4);
                        String meseIniIeri = dataIeri.Substring(4, 2);
                        String giornoIniIeri = dataIeri.Substring(6, 2);
                        String annoIni_Ieri = dataIeri;
                        String dt_fine = this.dtFine.Value;
                        String annoEnd = "2999";
                        String meseEnd = "12";
                        String giornoEnd = "31";
                        String annaEnd_db = giornoEnd + "/" + meseEnd + "/" + annoEnd;

                        //CONTROLLI INTEGRITA DATI
                        //addetto vendite
                        String qControlAddVendite = "SELECT * FROM \"OSLP\" WHERE  \"SlpName\" = \'" + ag + "\' ";
                        oRecordSet.DoQuery(qControlAddVendite);
                        String returnAddVendite = oRecordSet.Fields.Item("SlpName").Value.ToString();
                        String txtSlpCode = oRecordSet.Fields.Item("SlpCode").Value.ToString();
                        int SlpCode = Int32.Parse(txtSlpCode);

                        //CONTROLLO VALIDITA DATE
                        String qControlDate = "SELECT " + Program.SQL_limit1 + " * FROM \"@FC_ANAG\" WHERE \"U_dtEND\" >= \'" + annoIni_confronto + "\' and \"U_SlpCode\" =\'" + SlpCode.ToString() + "\' and \"U_CardCode\" =\'" + cliente + "\' AND \"U_attivo\" = 'si'  and \"U_GrpOmog\" =\'" + grpOmog + "\' AND \"U_Code\" LIKE \'" + prefix + "%\' " + Program.HANA_limit1 + " ";
                        //Application.SBO_Application.MessageBox(qControlDate, 1, "Ok", "", "");
                        oRecordSet.DoQuery(qControlDate);
                        String dateCode = oRecordSet.Fields.Item("Code").Value.ToString();
                        if (!dateCode.Equals(""))
                        {
                            updateProvv_b1f activeForm = new updateProvv_b1f(dateCode, annoIni_Ieri, "@FC_ANAG");
                            activeForm.Show();
                            inserisco = 0;
                        }
                        //INSERIMENTO
                        if (inserisco == 1)
                        {

                            //INSERISCO TESTATA ANAG.
                            //RECUPERO E CONVERTO ULTIMO Code anag
                            String qLastCodeA = "SELECT " + Program.SQL_limit1 + " * FROM \"@FC_ANAG\"  ORDER BY \"U_incr\" DESC " + Program.HANA_limit1 + " ";
                            oRecordSet.DoQuery(qLastCodeA);
                            if (oRecordSet.Fields.Item("Code").Value.ToString().Equals(""))
                            {
                                newCodeANAG = 1;
                            }
                            else
                            {
                                newCodeANAG = Int32.Parse(oRecordSet.Fields.Item("Code").Value.ToString());
                                newCodeANAG = newCodeANAG + 1;
                            }

                            String newCodeDbANAG = newCodeANAG.ToString();
                            SAPbobsCOM.UserTable oUserTableANAGIT;
                            oUserTableANAGIT = oCompany.UserTables.Item("FC_ANAG");
                            oUserTableANAGIT.Code = newCodeDbANAG;
                            oUserTableANAGIT.Name = "testata_" + newCodeDbANAG;
                            oUserTableANAGIT.UserFields.Fields.Item("U_SlpCode").Value = SlpCode.ToString();
                            oUserTableANAGIT.UserFields.Fields.Item("U_NomeRegola").Value = "Grp. Omog. / Cliente";
                            oUserTableANAGIT.UserFields.Fields.Item("U_Code").Value = prefix + newCodeANAG;
                            oUserTableANAGIT.UserFields.Fields.Item("U_GrpOmog").Value = grpOmog;
                            oUserTableANAGIT.UserFields.Fields.Item("U_SettoreCode").Value = "";
                            oUserTableANAGIT.UserFields.Fields.Item("U_CardCode").Value = cliente;
                            oUserTableANAGIT.UserFields.Fields.Item("U_ItemCode").Value = "";
                            oUserTableANAGIT.UserFields.Fields.Item("U_GrpCode").Value = "0";
                            oUserTableANAGIT.UserFields.Fields.Item("U_TipoArt").Value = "";
                            oUserTableANAGIT.UserFields.Fields.Item("U_BUnit").Value = "";
                            oUserTableANAGIT.UserFields.Fields.Item("U_percent").Value = perc.Replace(".", ",");
                            oUserTableANAGIT.UserFields.Fields.Item("U_dtINI").Value = annoIni_db;
                            oUserTableANAGIT.UserFields.Fields.Item("U_dtEND").Value = annaEnd_db;
                            oUserTableANAGIT.UserFields.Fields.Item("U_attivo").Value = "si";
                            oUserTableANAGIT.UserFields.Fields.Item("U_incr").Value = newCodeANAG;
                            int result = oUserTableANAGIT.Add();
                            if (result != 0)
                            {
                                Application.SBO_Application.MessageBox(oCompany.GetLastErrorDescription() + " - INSERT", 1, "Ok", "", "");
                            }
                            else
                            {
                                Application.SBO_Application.MessageBox("Provvigione aggiunta correttamente", 1, "Ok", "", "");
                                //CHIUDO IL FORM NEL MOMENTO IN CUI INSERISCO LA PROVVIGIONE
                                oForm.Close();
                            }
                        }

                    }
                }
                catch (InvalidCastException e)
                {
                    Application.SBO_Application.MessageBox("Non è stato possibile collegare l'agente alla provvigione", 1, "Ok", "", "");
                }

            }
        }

        //DDL GRUPPI
        public void ddlGrpOmog()
        {
            selectGrpOmog.ValidValues.Add(gruppo_base, gruppo_base);
            String qSettori = " select \"U_AG_GRPOMOG\" from \"OITM\" where \"U_AG_GRPOMOG\"<>'' Group by \"U_AG_GRPOMOG\" ORDER BY \"U_AG_GRPOMOG\" asc  ";
            oRecordSetDati.DoQuery(qSettori);
            while (!oRecordSetDati.EoF)
            {
                selectGrpOmog.ValidValues.Add(oRecordSetDati.Fields.Item("U_AG_GRPOMOG").Value.ToString(), oRecordSetDati.Fields.Item("U_AG_GRPOMOG").Value.ToString());
                oRecordSetDati.MoveNext();
            }

            selectGrpOmog.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
            selectGrpOmog.Select(gruppo_base, SAPbouiCOM.BoSearchKey.psk_ByValue);
        }

        //DISMISS
        private void btnStop_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            oForm = (SAPbouiCOM.Form)oApp.Forms.ActiveForm;
            oForm.Close();
        }

        //DICHIARAZIZONI SISTEMA
        public SAPbouiCOM.Application oApp;
        public SAPbouiCOM.IForm oForm;
        public SAPbobsCOM.Company oCompany = Program.oCompany;
        public SAPbobsCOM.Recordset oRecordSet;
        public SAPbobsCOM.Recordset oRecordSetInsert;
        public SAPbobsCOM.Recordset oRecordSetDati;

        //TEMPLATE FORM
        private SAPbouiCOM.EditText selectAg;
        private SAPbouiCOM.EditText selectPerc;
        private SAPbouiCOM.EditText dtInizio;
        private SAPbouiCOM.EditText dtFine;
        private SAPbouiCOM.Button btnAdd;
        private SAPbouiCOM.Button btnStop;
        private SAPbouiCOM.EditText selectCliente;
        private SAPbouiCOM.ComboBox selectGrpOmog;

        //PARAMETRI
        String gruppo_base = "Scegli un Gruppo...";
    }
}
