
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace Provvigioni
{

    [FormAttribute("Provvigioni.selDecurtazione_b1f", "params/selDecurtazione.b1f")]
    class selDecurtazione_b1f : UserFormBase
    {
        public selDecurtazione_b1f()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }

        private void OnCustomInitialize()
        {
            this.oApp = (SAPbouiCOM.Application)Application.SBO_Application;
            //this.oForm = (SAPbouiCOM.Form)oApp.Forms.ActiveForm;this.UIAPIRawForm.
            this.oForm = this.UIAPIRawForm;
            oRecordSet = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.EditText0 = ((SAPbouiCOM.EditText)(this.GetItem("Item_3").Specific));
            this.EditText1 = ((SAPbouiCOM.EditText)(this.GetItem("Item_4").Specific));
            this.EditText2 = ((SAPbouiCOM.EditText)(this.GetItem("Item_6").Specific));
            this.Button0 = ((SAPbouiCOM.Button)(this.GetItem("Item_7").Specific));
            this.Grid0 = ((SAPbouiCOM.Grid)(this.GetItem("Item_10").Specific));
            this.Button1 = ((SAPbouiCOM.Button)(this.GetItem("Item_11").Specific));
            this.Button2 = ((SAPbouiCOM.Button)(this.GetItem("Item_12").Specific));

            this.Button0.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.Button3_ClickBefore);
            this.Button1.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.elimina_ClickBefore);
            this.Button2.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.aggiorna_ClickBefore);

            //aggiornaGriglia();
            elementiForm();
        }

        //INSERISCO DECURTAZIONE
        private void Button3_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            this.oForm = (SAPbouiCOM.IForm)oApp.Forms.ActiveForm;
            String giorniIni = this.EditText0.Value;
            String giorniEnd = this.EditText1.Value;
            String percent = this.EditText2.Value;

            //CONTROLLO DATI
            if (int.Parse(giorniEnd) <= int.Parse(giorniIni))
            {
                //fine maggiore di inizio
                Application.SBO_Application.MessageBox("Periodo finale minore di quello iniziale.", 1, "Ok", "", "");
            }
            else
            {
                if (giorniIni.Equals("") || giorniEnd.Equals("") || percent.Equals(""))
                {
                    //mancano dati
                    Application.SBO_Application.MessageBox("Inserire i dati obbligatori", 1, "Ok", "", "");
                }
                else
                {
                    //dati completi
                    //CONTROLLO CHE NON ESISTANO DEC PRECDENTi
                    String controlPeriodo = "SELECT * FROM \"@FC_PRDE\" WHERE \"U_fine\">= \'" + giorniIni + "\'";
                    oRecordSet.DoQuery(controlPeriodo);
                    String iniCtrl = oRecordSet.Fields.Item("Code").Value.ToString();
                    if (!iniCtrl.Equals(""))
                    {
                        //esiste gia un periodo
                        Application.SBO_Application.MessageBox("Non è possibile inserire questa percentuale di decurtazione perchè esiste già un inserimento che copre, parte o per intero, lo stesso periodo.", 1, "Ok", "", "");
                    }
                    else
                    {
                        //inserisco il periodo
                        //RECUPERO E CONVERTO ULTIMO Code anag
                        String qLastCodeA = "SELECT " + Program.SQL_limit1 + " * FROM \"@FC_PRDE\"  ORDER BY \"U_incr\" DESC " + Program.HANA_limit1 + " ";
                        int newCode = 0;
                        oRecordSet.DoQuery(qLastCodeA);
                        if (oRecordSet.Fields.Item("Code").Value.ToString().Equals(""))
                        {
                            newCode = 1;
                        }
                        else
                        {
                            newCode = Int32.Parse(oRecordSet.Fields.Item("Code").Value.ToString());
                            newCode = newCode + 1;
                        }
                        percent = percent.Replace('.',',');
                        //INSERISCO
                        String newCodeDbANAG = newCode.ToString();
                        SAPbobsCOM.UserTable oUserTable;
                        oUserTable = oCompany.UserTables.Item("FC_PRDE");
                        oUserTable.Code = newCodeDbANAG;
                        oUserTable.Name = "testata_" + newCodeDbANAG;
                        oUserTable.UserFields.Fields.Item("U_inizio").Value = giorniIni;
                        oUserTable.UserFields.Fields.Item("U_fine").Value = giorniEnd;
                        oUserTable.UserFields.Fields.Item("U_percent").Value = percent;
                        oUserTable.UserFields.Fields.Item("U_incr").Value = newCode;
                        int result = oUserTable.Add();
                        if (result != 0)
                        {
                            Application.SBO_Application.MessageBox("Errore nell'inserimento, riavviare i servizi e riprovare.", 1, "Ok", "", "");
                        }
                    }
                }
            }
            aggiornaGriglia();
        }
        //ELIMINO DECURTAZIONE
        private void elimina_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            SAPbouiCOM.DataTable riga = Grid0.DataTable;


            int numRows = Grid0.Rows.Count;

            string[] righeIDS = new string[numRows];

            for (int i = 0; i < numRows; i++)
            {
                String idRiga = Grid0.DataTable.GetValue("ID", i).ToString();
                String select = Grid0.DataTable.GetValue("Selezione", i).ToString();
                if (select.Equals("Y"))
                {
                    String qDel = "DELETE FROM \"@FC_PRDE\" WHERE \"Code\" = \'" + idRiga + "\' ";
                    oRecordSet.DoQuery(qDel);
                }

            }
            aggiornaGriglia();
        }
        //AGGIORNO DECURTAZIONE
        private void aggiorna_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            SAPbouiCOM.DataTable riga = Grid0.DataTable;

            int numRows = Grid0.Rows.Count;

            for (int i = 0; i < numRows; i++)
            {
                String idRiga = Grid0.DataTable.GetValue("ID", i).ToString();
                String select = Grid0.DataTable.GetValue("Selezione", i).ToString();
                String daDT = Grid0.DataTable.GetValue("Da", i).ToString();
                String aDT = Grid0.DataTable.GetValue("A", i).ToString();
                String percent = Grid0.DataTable.GetValue("%", i).ToString().Replace('.',',');;
                if (select.Equals("Y"))
                {
                    String qUp = "UPDATE \"@FC_PRDE\" SET \"U_percent\" = \'" + percent + "\',\"U_inizio\" = \'" + daDT + "\',\"U_fine\" = \'" + aDT + "\' WHERE \"Code\" = \'" + idRiga + "\' ";
                    oRecordSet.DoQuery(qUp);
                }

            }


            oForm = (SAPbouiCOM.Form)oApp.Forms.ActiveForm;
            

            aggiornaGriglia();
        }
        //AGGIORNO DATI GRIGLIA
        private void aggiornaGriglia()
        {
            this.oForm = (SAPbouiCOM.IForm)oApp.Forms.ActiveForm;
            this.UIAPIRawForm.Freeze(true);
            String queryFiltro = "SELECT \"Code\" AS \"ID\",\"U_inizio\" AS \"Da\",\"U_fine\" AS \"A\",\"U_percent\" as \"%\",'N' AS \"Selezione\" FROM \"@FC_PRDE\" ORDER BY \"U_inizio\" ASC ";
            //Application.SBO_Application.MessageBox(queryFiltro, 1, "Ok", "", "");
            //AGGIUNGO ID LISTA AL FORM
            try
            {
                oForm.DataSources.DataTables.Add("listaDecPercX");
            }
            catch (System.Runtime.InteropServices.COMException e)
            {
                //Application.SBO_Application.MessageBox(e.ToString(), 1, "Ok", "", "");
            }
            oForm.DataSources.DataTables.Item("listaDecPercX").ExecuteQuery(queryFiltro);
            //oForm.DataSources.Columns.Add("#", SAPbouiCOM.BoFieldsType.ft_Text, 20);
            Grid0.DataTable = oForm.DataSources.DataTables.Item("listaDecPercX");

            //assegno elemtni grid
            elementiForm();

            //RESIZE
            Grid0.AutoResizeColumns();
            this.UIAPIRawForm.Freeze(false);
        }
        //COLONNE GRID
        private void elementiForm()
        {
            //DISABLE COLONNE
            Grid0.Columns.Item(0).Editable = false;

            //COLONNE CUSTOM
            Grid0.Columns.Item(4).TitleObject.Sortable = true;
            Grid0.Columns.Item(4).Type = SAPbouiCOM.BoGridColumnType.gct_CheckBox;
        }
        //DICHIARAZIZONI SISTEMA
        public SAPbouiCOM.Application oApp;
        public SAPbouiCOM.IForm oForm;
        public SAPbobsCOM.Company oCompany = Program.oCompany;
        public SAPbobsCOM.Recordset oRecordSet;

        //layout
        private SAPbouiCOM.EditText EditText0;
        private SAPbouiCOM.EditText EditText1;
        private SAPbouiCOM.EditText EditText2;
        private SAPbouiCOM.Button Button0;
        private SAPbouiCOM.Grid Grid0;
        private SAPbouiCOM.Button Button1;
        private SAPbouiCOM.Button Button2;
        private SAPbouiCOM.Button Button3;
        private SAPbouiCOM.EditText EditText3;
        private SAPbouiCOM.StaticText StaticText1;

        
    }
}
