
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace Provvigioni
{

    [FormAttribute("Provvigioni.addStandardAgente_b1f", "params/addStandardAgente.b1f")]
    class addStandardAgente_b1f : UserFormBase
    {
        public addStandardAgente_b1f()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.btnAdd = ((SAPbouiCOM.Button)(this.GetItem("btnAdd").Specific));
            this.btnStop = ((SAPbouiCOM.Button)(this.GetItem("btnStop").Specific));
            this.selectAg = ((SAPbouiCOM.EditText)(this.GetItem("selectAg").Specific));
            this.selectDec = ((SAPbouiCOM.CheckBox)(this.GetItem("selectDec").Specific));
            this.selectPerc = ((SAPbouiCOM.EditText)(this.GetItem("selectPerc").Specific));
            this.dtInizio = ((SAPbouiCOM.EditText)(this.GetItem("dtInizio").Specific));
            this.dtFine = ((SAPbouiCOM.EditText)(this.GetItem("dtFine").Specific));

            //setto data fine ad infinito
            dtFine.Value = "29991231";

            //metto il focus sul primo campo
            selectAg.Value = "";

            this.prezzoIncr = ((SAPbouiCOM.EditText)(this.GetItem("prezzoIncr").Specific));
            this.prezzoDecr = ((SAPbouiCOM.EditText)(this.GetItem("prezzoDecr").Specific));
            
            // PREMO OK PER AGGIUNGERE COLLEGAMENTO
            this.btnAdd.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.btnOk_ClickBefore);
            // CHIUDO FORM
            this.btnStop.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.btnStop_ClickBefore);
            //  INIZIALIZZO
            this.StaticText0 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_9").Specific));
            
            this.OnCustomInitialize();

        }
        //AGGIUNGO COLLEGAMENTO AGENTE/BP
        private void btnOk_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true; 
            this.oForm = (SAPbouiCOM.IForm)oApp.Forms.ActiveForm;
            oRecordSet = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));

            int inserisco = 1;
            int newCode = 0;
            String msg = "";
            int newCodeANAG = 0;
            String prefix = "percStd_";
            if (oForm.Mode == SAPbouiCOM.BoFormMode.fm_ADD_MODE)
            {
                msg = "Non è stato possibile aggiungere questa provvigione";
                try
                {
                    //CONTROLLI INTEGRITA DATI

                    //dati obbligatori
                    if (this.selectAg.Value.Equals("") || this.selectPerc.Value.Equals("") || this.dtInizio.Value.Equals(""))
                    {
                        //dati mancanti
                        Application.SBO_Application.MessageBox("Inserire i dati obbligatori", 1, "Ok", "", "");
                    }
                    else
                    {
                        String ag = this.selectAg.Value;
                        String perc = this.selectPerc.Value;
                        String dt_inizio = this.dtInizio.Value;
                        String annoIni = dt_inizio.Substring(0, 4);
                        String meseIni = dt_inizio.Substring(4, 2);
                        String giornoIni = dt_inizio.Substring(6, 2);
                        String annoIni_db = giornoIni + "/" + meseIni + "/" + annoIni;
                        String annoIni_confronto = annoIni + meseIni + giornoIni;
                        DateTime enteredDate = DateTime.Parse(annoIni_db);
                        enteredDate = enteredDate.AddDays(-1);
                        String dataIeri = enteredDate.ToString();
                        String annoIniIeri = dataIeri.Substring(0, 4);
                        String meseIniIeri = dataIeri.Substring(4, 2);
                        String giornoIniIeri = dataIeri.Substring(6, 2);
                        String annoIni_Ieri = dataIeri;
                        String dt_fine = this.dtFine.Value;
                        String annoEnd = "2999";
                        String meseEnd = "12";
                        String giornoEnd = "31";
                        String annoEnd_db = giornoEnd + "/" + meseEnd + "/" + annoEnd;
                        String incrementoPrezzo = this.prezzoIncr.Value;
                        String decrementoPrezzo = this.prezzoDecr.Value;
                        String decurt = "no";
                        if (selectDec.Checked == true)
                        {
                            decurt = "si";
                       
                        }

                        //addetto vendite
                        String qControlAddVendite = "SELECT * FROM \"OSLP\" WHERE  \"SlpName\" = \'" + ag + "\' ";
                        oRecordSet.DoQuery(qControlAddVendite);
                        String returnAddVendite = oRecordSet.Fields.Item("SlpName").Value.ToString();
                        String txtSlpCode = oRecordSet.Fields.Item("SlpCode").Value.ToString();
                        int SlpCode = Int32.Parse(txtSlpCode);
                        

                        //CONTROLLO VALIDITA DATE
                        String qControlDate = "SELECT " + Program.SQL_limit1 + " * FROM \"@FC_ANAG\" WHERE \"U_dtEND\" >= \'" + annoIni_confronto + "\' and \"U_SlpCode\" =\'" + SlpCode.ToString() + "\' AND \"U_attivo\" = 'si'  AND \"U_Code\" LIKE \'" + prefix + "%\' " + Program.HANA_limit1 + " ";
                        //Application.SBO_Application.MessageBox(qControlDate, 1, "Ok", "", "");
                        oRecordSet.DoQuery(qControlDate);
                        String dateCode = oRecordSet.Fields.Item("Code").Value.ToString();
                        if (!dateCode.Equals(""))
                        {
                            updateProvv_b1f activeForm = new updateProvv_b1f(dateCode, annoIni_Ieri, "@FC_ANAG");
                            activeForm.Show();
                            inserisco = 0;
                        }
                        //INSERIMENTO
                        if (inserisco == 1)
                        {
                           
                            //INSERISCO TESTATA ANAG.
                            //RECUPERO E CONVERTO ULTIMO Code anag
                            String qLastCodeA = "SELECT " + Program.SQL_limit1 + " * FROM \"@FC_ANAG\"  ORDER BY \"U_incr\" DESC " + Program.HANA_limit1 + " ";
                            oRecordSet.DoQuery(qLastCodeA);
                            if (oRecordSet.Fields.Item("Code").Value.ToString().Equals(""))
                            {
                                newCodeANAG = 1;
                            }
                            else
                            {
                                newCodeANAG = Int32.Parse(oRecordSet.Fields.Item("Code").Value.ToString());
                                newCodeANAG = newCodeANAG + 1;
                            }

                            String newCodeDbANAG = newCodeANAG.ToString();
                            SAPbobsCOM.UserTable oUserTableANAG;
                            oUserTableANAG = oCompany.UserTables.Item("FC_ANAG");
                            oUserTableANAG.Code = newCodeDbANAG;
                            oUserTableANAG.Name = "testata_" + newCodeDbANAG;
                            oUserTableANAG.UserFields.Fields.Item("U_SlpCode").Value = SlpCode.ToString();
                            oUserTableANAG.UserFields.Fields.Item("U_NomeRegola").Value = "Standard";
                            oUserTableANAG.UserFields.Fields.Item("U_Code").Value = prefix + newCodeANAG;
                            oUserTableANAG.UserFields.Fields.Item("U_CardCode").Value = "";
                            oUserTableANAG.UserFields.Fields.Item("U_ItemCode").Value = "";
                            oUserTableANAG.UserFields.Fields.Item("U_GrpCode").Value = "0";
                            oUserTableANAG.UserFields.Fields.Item("U_GrpOmog").Value = "";
                            oUserTableANAG.UserFields.Fields.Item("U_TipoArt").Value = "";
                            oUserTableANAG.UserFields.Fields.Item("U_SettoreCode").Value = "0";
                            oUserTableANAG.UserFields.Fields.Item("U_BUnit").Value = "";
                            oUserTableANAG.UserFields.Fields.Item("U_percent").Value = perc.Replace(".", ",");
                            oUserTableANAG.UserFields.Fields.Item("U_dtINI").Value = annoIni_db;
                            oUserTableANAG.UserFields.Fields.Item("U_dtEND").Value = annoEnd_db;
                            oUserTableANAG.UserFields.Fields.Item("U_decurt").Value = decurt;
                            oUserTableANAG.UserFields.Fields.Item("U_incrPrezzo").Value = incrementoPrezzo.ToString();
                            oUserTableANAG.UserFields.Fields.Item("U_decrPrezzo").Value = decrementoPrezzo.ToString();
                            oUserTableANAG.UserFields.Fields.Item("U_attivo").Value = "si";
                            oUserTableANAG.UserFields.Fields.Item("U_incr").Value = newCodeANAG;
                            int result = oUserTableANAG.Add();
                            if (result != 0)
                            {
                                Application.SBO_Application.MessageBox(oCompany.GetLastErrorDescription() + " - INSERT", 1, "Ok", "", "");
                            }
                            else
                            {
                                Application.SBO_Application.MessageBox("Provvigione aggiunta correttamente", 1, "Ok", "", "");
                                //SBANCO I CAMPI
                                /*selectAg.Value = "";
                                selectPerc.Value = "";
                                dtInizio.Value = "";
                                prezzoIncr.Value = "";
                                prezzoDecr.Value = "";
                                selectDec.Checked = false;*/

                                //NON CHIUDO IL FORM NEL MOMENTO IN CUI INSERISCO LA PROVVIGIONE
                                oForm.Close();
                            }
                        }
                        else
                        {
                            //Application.SBO_Application.MessageBox("Attenzione! Esiste già una provvigione che copre questo periodo", 1, "Ok", "", "");
                        }
                    }//fine else dati mancanti
                }
                catch (InvalidCastException e)
                {
                    Application.SBO_Application.MessageBox(msg, 1, "Ok", "", "");
                }

            }
        }

        //DISMISS
        private void btnStop_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            this.oForm = (SAPbouiCOM.IForm)oApp.Forms.ActiveForm;
            oForm.Close();
        }
        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }
        private void OnCustomInitialize()
        {
            this.LoadAfter += new LoadAfterHandler(this.Form_LoadAfter);
            this.oApp = (SAPbouiCOM.Application)Application.SBO_Application;
            //this.oForm = (SAPbouiCOM.Form)oApp.Forms.ActiveForm;this.UIAPIRawForm.
            this.oForm = this.UIAPIRawForm;
        }

        private void Form_LoadAfter(SAPbouiCOM.SBOItemEventArg pVal)
        {
            // throw new System.NotImplementedException();

        }

        //DICHIARAZIZONI SISTEMA
        public SAPbouiCOM.Application oApp;
        public SAPbouiCOM.IForm oForm;
        public SAPbobsCOM.Company oCompany = Program.oCompany;
        public SAPbobsCOM.Recordset oRecordSet;

        //TEMPLATE FORM
        private SAPbouiCOM.Button btnAdd;
        private SAPbouiCOM.Button btnStop;
        private SAPbouiCOM.EditText selectAg;
        private SAPbouiCOM.EditText selectPerc;
        private SAPbouiCOM.EditText dtInizio;
        private SAPbouiCOM.CheckBox selectDec;
        private SAPbouiCOM.EditText prezzoIncr;
        private SAPbouiCOM.EditText prezzoDecr;
        private SAPbouiCOM.StaticText StaticText0;
        private SAPbouiCOM.EditText dtFine;
    }
}
