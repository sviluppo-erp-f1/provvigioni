
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace Provvigioni
{

    [FormAttribute("Provvigioni.addAgenteClienteArticolo_b1f", "params/addAgenteClienteArticolo.b1f")]
    class addAgenteClienteArticolo_b1f : UserFormBase
    {
        public addAgenteClienteArticolo_b1f()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.selectAg = ((SAPbouiCOM.EditText)(this.GetItem("selectAg").Specific));
            this.selectCl = ((SAPbouiCOM.EditText)(this.GetItem("selectCl").Specific));
            this.selectCl.ChooseFromListAfter += new SAPbouiCOM._IEditTextEvents_ChooseFromListAfterEventHandler(this.selectCl_ChooseFromListAfter);
            this.selectItem = ((SAPbouiCOM.EditText)(this.GetItem("selectItem").Specific));
            this.selectItem.ChooseFromListAfter += new SAPbouiCOM._IEditTextEvents_ChooseFromListAfterEventHandler(this.selectItem_ChooseFromListAfter);
            this.dtInizio = ((SAPbouiCOM.EditText)(this.GetItem("dt_inizio").Specific));
            this.selectPerc = ((SAPbouiCOM.EditText)(this.GetItem("percent").Specific));
            this.dtFine = ((SAPbouiCOM.EditText)(this.GetItem("dt_fine").Specific));
            this.selectDec = ((SAPbouiCOM.CheckBox)(this.GetItem("decurt").Specific));
            this.btnAdd = ((SAPbouiCOM.Button)(this.GetItem("btnAdd").Specific));
            this.btnStop = ((SAPbouiCOM.Button)(this.GetItem("btnStop").Specific));
            this.prezzoIncr = ((SAPbouiCOM.EditText)(this.GetItem("Item_5").Specific));
            this.prezzoDecr = ((SAPbouiCOM.EditText)(this.GetItem("Item_9").Specific));

            //setto data fine ad infinito
            dtFine.Value = "29991231";

            //metto il focus sul primo campo
            selectAg.Value = "";

            //      PREMO OK PER AGGIUNGERE COLLEGAMENTO
            this.btnAdd.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.btnOk_ClickBefore);
            //      CHIUDO FORM
            this.btnStop.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.btnStop_ClickBefore);
            // INIZIALIZZO
            
            this.OnCustomInitialize();

        }
        //AGGIUNGO COLLEGAMENTO AGENTE/BP
        private void btnOk_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            oForm = (SAPbouiCOM.Form)oApp.Forms.ActiveForm;
            oRecordSet = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));

            int inserisco = 1;
            int newCode = 0;
            int newCodeANAG = 0;
            String prefix = "percAgClArt_";
            if (oForm.Mode == SAPbouiCOM.BoFormMode.fm_ADD_MODE)
            {
                try
                {
                    //CONTROLLI INTEGRITA DATI

                    //dati obbligatori
                    if (this.selectAg.Value.Equals("") || this.selectPerc.Value.Equals("") || this.dtInizio.Value.Equals("") || this.selectItem.Value.Equals("") || this.selectCl.Value.Equals(""))
                    {
                        //dati mancanti
                        Application.SBO_Application.MessageBox("Inserire i dati obbligatori", 1, "Ok", "", "");
                    }
                    else
                    {
                        String ag = this.selectAg.Value;
                        String cl = this.selectCl.Value;
                        String item = this.selectItem.Value;
                        String perc = this.selectPerc.Value;
                        String dt_inizio = this.dtInizio.Value;
                        String annoIni = dt_inizio.Substring(0, 4);
                        String meseIni = dt_inizio.Substring(4, 2);
                        String giornoIni = dt_inizio.Substring(6, 2);
                        String annoIni_db = giornoIni + "/" + meseIni + "/" + annoIni;
                        String annoIni_confronto = annoIni + meseIni + giornoIni;
                        DateTime enteredDate = DateTime.Parse(annoIni_db);
                        enteredDate = enteredDate.AddDays(-1);
                        String dataIeri = enteredDate.ToString();
                        String annoIniIeri = dataIeri.Substring(0, 4);
                        String meseIniIeri = dataIeri.Substring(4, 2);
                        String giornoIniIeri = dataIeri.Substring(6, 2);
                        String annoIni_Ieri = dataIeri;
                        String dt_fine = this.dtFine.Value;
                        String annoEnd = "2999";
                        String meseEnd = "12";
                        String giornoEnd = "31";
                        String annaEnd_db = giornoEnd + "/" + meseEnd + "/" + annoEnd;
                        String decurt = "no";
                        String incrementoPrezzo = this.prezzoIncr.Value;
                        String decrementoPrezzo = this.prezzoDecr.Value;
                        if (selectDec.Checked == true)
                        {
                            decurt = "si";

                        }

                        //CONTROLLI INTEGRITA DATI
                        //addetto vendite
                        String qControlAddVendite = "SELECT * FROM \"OSLP\" WHERE  \"SlpName\" = \'" + ag + "\' ";
                        oRecordSet.DoQuery(qControlAddVendite);
                        String returnAddVendite = oRecordSet.Fields.Item("SlpName").Value.ToString();
                        String txtSlpCode = oRecordSet.Fields.Item("SlpCode").Value.ToString();
                        int SlpCode = Int32.Parse(txtSlpCode);
                    
                        //CONTROLLO VALIDITA DATE
                        String qControlDate = "SELECT " + Program.SQL_limit1 + " * FROM \"@FC_ANAG\" WHERE \"U_dtEND\" >= \'" + annoIni_confronto + "\' and \"U_SlpCode\" =\'" + SlpCode.ToString() + "\' AND \"U_attivo\" = 'si'  and \"U_ItemCode\" =\'" + item + "\'  and \"U_CardCode\" =\'" + cl + "\' AND \"U_Code\" LIKE \'" + prefix + "%\' " + Program.HANA_limit1 + " ";
                        oRecordSet.DoQuery(qControlDate);
                        String dateCode = oRecordSet.Fields.Item("Code").Value.ToString();
                        if (!dateCode.Equals(""))
                        {
                            updateProvv_b1f activeForm = new updateProvv_b1f(dateCode, annoIni_Ieri, "@FC_ANAG");
                            activeForm.Show();
                            inserisco = 0;
                        }
                        //INSERIMENTO
                        if (inserisco == 1)
                        {

                            //INSERISCO TESTATA ANAG.
                            //RECUPERO E CONVERTO ULTIMO Code anag
                            String qLastCodeA = "SELECT " + Program.SQL_limit1 + " * FROM \"@FC_ANAG\"  ORDER BY \"U_incr\" DESC " + Program.HANA_limit1 + " ";
                            oRecordSet.DoQuery(qLastCodeA);
                            if (oRecordSet.Fields.Item("Code").Value.ToString().Equals(""))
                            {
                                newCodeANAG = 1;
                            }
                            else
                            {
                                newCodeANAG = Int32.Parse(oRecordSet.Fields.Item("Code").Value.ToString());
                                newCodeANAG = newCodeANAG + 1;
                            }

                            String newCodeDbANAG = newCodeANAG.ToString();
                            SAPbobsCOM.UserTable oUserTableANAG;
                            oUserTableANAG = oCompany.UserTables.Item("FC_ANAG");
                            oUserTableANAG.Code = newCodeDbANAG;
                            oUserTableANAG.Name = "testata_" + newCodeDbANAG;
                            oUserTableANAG.UserFields.Fields.Item("U_SlpCode").Value = SlpCode.ToString();
                            oUserTableANAG.UserFields.Fields.Item("U_NomeRegola").Value = "Cliente/Articolo";
                            oUserTableANAG.UserFields.Fields.Item("U_Code").Value = prefix + newCodeDbANAG;
                            oUserTableANAG.UserFields.Fields.Item("U_CardCode").Value = cl;
                            oUserTableANAG.UserFields.Fields.Item("U_ItemCode").Value = item;
                            oUserTableANAG.UserFields.Fields.Item("U_GrpCode").Value = "0";
                            oUserTableANAG.UserFields.Fields.Item("U_GrpOmog").Value = "";
                            oUserTableANAG.UserFields.Fields.Item("U_TipoArt").Value = "";
                            oUserTableANAG.UserFields.Fields.Item("U_SettoreCode").Value = "0";
                            oUserTableANAG.UserFields.Fields.Item("U_BUnit").Value = "";
                            oUserTableANAG.UserFields.Fields.Item("U_percent").Value = perc.Replace(".", ",");
                            oUserTableANAG.UserFields.Fields.Item("U_dtINI").Value = annoIni_db;
                            oUserTableANAG.UserFields.Fields.Item("U_dtEND").Value = annaEnd_db;
                            oUserTableANAG.UserFields.Fields.Item("U_decurt").Value = decurt;
                            oUserTableANAG.UserFields.Fields.Item("U_incrPrezzo").Value = incrementoPrezzo.ToString();
                            oUserTableANAG.UserFields.Fields.Item("U_decrPrezzo").Value = decrementoPrezzo.ToString();
                            oUserTableANAG.UserFields.Fields.Item("U_attivo").Value = "si";
                            oUserTableANAG.UserFields.Fields.Item("U_incr").Value = newCodeANAG;
                            int result = oUserTableANAG.Add();
                            if (result != 0)
                            {
                                Application.SBO_Application.MessageBox(oCompany.GetLastErrorDescription() + " - INSERT", 1, "Ok", "", "");
                            }
                            else
                            {
                                Application.SBO_Application.MessageBox("Provvigione aggiunta correttamente", 1, "Ok", "", "");
                                //SBANCO I CAMPI
                                /*selectAg.Value = "";
                                selectItem.Value = "";
                                selectCl.Value = "";
                                selectPerc.Value = "";
                                dtInizio.Value = "";
                                prezzoIncr.Value = "";
                                prezzoDecr.Value = "";
                                selectDec.Checked = false;*/

                                //NON CHIUDO IL FORM NEL MOMENTO IN CUI INSERISCO LA PROVVIGIONE
                                oForm.Close();
                            }
                        }
                    }

                }
                catch (InvalidCastException e)
                {
                    Application.SBO_Application.MessageBox("Non è stato possibile collegare l'agente al BP", 1, "Ok", "", "");
                }

            }
        }

        //DISMISS
        private void btnStop_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            oForm = (SAPbouiCOM.Form)oApp.Forms.ActiveForm;
            oForm.Close();
        }
        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
            this.oApp = (SAPbouiCOM.Application)Application.SBO_Application;
            //this.oForm = (SAPbouiCOM.Form)oApp.Forms.ActiveForm;this.UIAPIRawForm.
            this.oForm = this.UIAPIRawForm;
        }


        private void OnCustomInitialize()
        {

        }

        private void selectCl_ChooseFromListAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            this.oForm = (SAPbouiCOM.IForm)oApp.Forms.ActiveForm;
            oForm.Items.Item("7").Click(SAPbouiCOM.BoCellClickType.ct_Regular);
            SAPbouiCOM.ISBOChooseFromListEventArg chflarg = (SAPbouiCOM.ISBOChooseFromListEventArg)pVal;

            SAPbouiCOM.DataTable dt = chflarg.SelectedObjects;
            if (dt != null)
            {
                try
                {
                    this.selectCl.Value = dt.GetValue("CardCode", 0).ToString();
                }
                catch (System.Runtime.InteropServices.COMException e)
                {
                    //Application.SBO_Application.MessageBox(e.ToString()+"ehy fonzi", 1, "Ok", "", "");
                }
            }
            else
            {
                if (this.selectAg.Value == "")
                {
                    this.selectAg.Value = "";
                }
            }

        }

        private void selectItem_ChooseFromListAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            this.oForm = (SAPbouiCOM.IForm)oApp.Forms.ActiveForm;
            oForm.Items.Item("7").Click(SAPbouiCOM.BoCellClickType.ct_Regular);
            SAPbouiCOM.ISBOChooseFromListEventArg chflarg = (SAPbouiCOM.ISBOChooseFromListEventArg)pVal;

            SAPbouiCOM.DataTable dt = chflarg.SelectedObjects;
            if (dt != null)
            {
                try
                {
                    this.selectItem.Value = dt.GetValue("ItemCode", 0).ToString();
                }
                catch (System.Runtime.InteropServices.COMException e)
                {
                    //Application.SBO_Application.MessageBox(e.ToString()+"ehy fonzi", 1, "Ok", "", "");
                }
            }
            else
            {
                if (this.selectAg.Value == "")
                {
                    this.selectAg.Value = "";
                }
            }

        }

        //DICHIARAZIZONI SISTEMA
        public SAPbouiCOM.Application oApp;
        public SAPbouiCOM.IForm oForm;
        public SAPbobsCOM.Company oCompany = Program.oCompany;
        public SAPbobsCOM.Recordset oRecordSet;

        //TEMPLATE FORM
        private SAPbouiCOM.EditText selectAg;
        private SAPbouiCOM.EditText selectCl;
        private SAPbouiCOM.EditText selectItem;
        private SAPbouiCOM.EditText selectPerc;
        private SAPbouiCOM.EditText dtInizio;
        private SAPbouiCOM.EditText dtFine;
        private SAPbouiCOM.CheckBox selectDec;
        private SAPbouiCOM.Button btnAdd;
        private SAPbouiCOM.Button btnStop;
        private SAPbouiCOM.EditText prezzoDecr;
        private SAPbouiCOM.EditText prezzoIncr;

    }
}
