
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace Provvigioni
{

    [FormAttribute("Provvigioni.rimuoviArticoliPr_b1f", "params/rimuoviArticoliPr.b1f")]
    class rimuoviArticoliPr_b1f : UserFormBase
    {
        public rimuoviArticoliPr_b1f()
        {
        }

      
        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
            this.ActivateAfter += new SAPbouiCOM.Framework.FormBase.ActivateAfterHandler(this.Form_ActivateAfter);
           

        }


        private void OnCustomInitialize()
        {
            this.oApp = (SAPbouiCOM.Application)Application.SBO_Application;
            //this.oForm = (SAPbouiCOM.Form)oApp.Forms.ActiveForm;this.UIAPIRawForm.
            this.oForm = this.UIAPIRawForm;
            oRecordSet = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            oRecordSetIns = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.StaticText0 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_0").Specific));
            this.EditText0 = ((SAPbouiCOM.EditText)(this.GetItem("Item_1").Specific));
            this.Button0 = ((SAPbouiCOM.Button)(this.GetItem("Item_2").Specific));
            this.Grid0 = ((SAPbouiCOM.Grid)(this.GetItem("Item_3").Specific));
            this.Button0.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.Button1_ClickBefore);
            this.Button3 = ((SAPbouiCOM.Button)(this.GetItem("Item_4").Specific));
            this.Button3.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.Button3_ClickBefore);
            this.Button3.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button4_ClickAfter);
            this.Button0.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button4_ClickAfter);

            elementiForm();
        }
        //INSERISCO ARTICOLO DA ESCLUDERE
        private void Button1_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            this.oForm = (SAPbouiCOM.IForm)oApp.Forms.ActiveForm;
            String artCode = this.EditText0.Value;

            //RECUPERO E CONVERTO ULTIMO Code anag
            String qLastCodeA = "SELECT " + Program.SQL_limit1 + " * FROM \"@FC_PRIT\"  ORDER BY \"U_incr\" DESC " + Program.HANA_limit1 + " ";
            int newCode = 0;
            oRecordSet.DoQuery(qLastCodeA);
            if (oRecordSet.Fields.Item("Code").Value.ToString().Equals(""))
            {
                newCode = 1;
            }
            else
            {
                newCode = Int32.Parse(oRecordSet.Fields.Item("Code").Value.ToString());
                newCode = newCode + 1;
            }

            String newCodeDbANAG = newCode.ToString();

            try
            {
                //INSERISCO
                String q_ins = " INSERT INTO \"@FC_PRIT\" (\"Code\",\"Name\",\"U_startString\",\"U_incr\") VALUES ('" + newCodeDbANAG + "','" + newCodeDbANAG + "','" + artCode + "','" + newCode + "') ";
                oRecordSetIns.DoQuery(q_ins);
                Application.SBO_Application.MessageBox("Articolo/stringa " + artCode + " escluso dal calcolo delle provvigioni", 1, "Ok", "", "");
                EditText0.Value = "";
            }
            catch (System.Runtime.InteropServices.COMException e)
            {
                Application.SBO_Application.MessageBox("Errore nell'inserimento, riavviare i servizi e riprovare.", 1, "Ok", "", "");
            }
            
        }
        //ELIMINA
        private void Button3_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            SAPbouiCOM.DataTable riga = Grid0.DataTable;

            this.UIAPIRawForm.Freeze(true);

            int numRows = Grid0.Rows.Count;

            string[] righeIDS = new string[numRows];

            for (int i = 0; i < numRows; i++)
            {
                String idRiga = Grid0.DataTable.GetValue("Articolo/Stringa", i).ToString();
                String select = Grid0.DataTable.GetValue("Selezione", i).ToString();
                if (select.Equals("Y"))
                {
                    String qDel = "DELETE FROM \"@FC_PRIT\" WHERE \"U_startString\" = \'" + idRiga + "\' ";
                    oRecordSet.DoQuery(qDel);
                }

            }
            this.UIAPIRawForm.Freeze(false);

        }
        //AGGIORNA GRID
        private void aggiornaGriglia()
        {
            this.oForm = (SAPbouiCOM.IForm)oApp.Forms.ActiveForm;
            this.UIAPIRawForm.Freeze(true);
            String queryFiltro = "SELECT \"U_startString\" as \"Articolo/Stringa\",'UCHECK' as \"Selezione\" FROM \"@FC_PRIT\" ";
            //Application.SBO_Application.MessageBox(queryFiltro, 1, "Ok", "", "");
            //AGGIUNGO ID LISTA AL FORM
            try
            {
                oForm.DataSources.DataTables.Add("listaArtRemoveX");
            }
            catch (System.Runtime.InteropServices.COMException e)
            {
                //Application.SBO_Application.MessageBox(e.ToString(), 1, "Ok", "", "");
            }
            oForm.DataSources.DataTables.Item("listaArtRemoveX").ExecuteQuery(queryFiltro);
            //oForm.DataSources.Columns.Add("#", SAPbouiCOM.BoFieldsType.ft_Text, 20);
            Grid0.DataTable = oForm.DataSources.DataTables.Item("listaArtRemoveX");

            //assegno elemtni grid
            elementiForm();

            //RESIZE
            Grid0.AutoResizeColumns();
            this.UIAPIRawForm.Freeze(false);
        }
        //COLONNE GRID
        private void elementiForm()
        {
            //DISABLE COLONNE
            Grid0.Columns.Item(0).Editable = false;

            //COLONNE CUSTOM
            Grid0.Columns.Item(1).TitleObject.Sortable = true;
            Grid0.Columns.Item(1).Type = SAPbouiCOM.BoGridColumnType.gct_CheckBox;
        }

        //DICHIARAZIZONI SISTEMA
        public SAPbouiCOM.Application oApp;
        public SAPbouiCOM.IForm oForm;
        public SAPbobsCOM.Company oCompany = Program.oCompany;
        public SAPbobsCOM.Recordset oRecordSet;
        public SAPbobsCOM.Recordset oRecordSetIns;

        //LAYOUT
        private SAPbouiCOM.StaticText StaticText0;
        private SAPbouiCOM.EditText EditText0;
        private SAPbouiCOM.Button Button0;
        private SAPbouiCOM.Grid Grid0;
        private SAPbouiCOM.Button Button1;
        private SAPbouiCOM.Button Button2;
        private SAPbouiCOM.Button Button3;
        private SAPbouiCOM.Button Button4;

        private void Button4_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            aggiornaGriglia();

        }

        private void Form_ActivateAfter(SAPbouiCOM.SBOItemEventArg pVal)
        {
            

        }


    }
}
