
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace Provvigioni
{

    [FormAttribute("Provvigioni.collegamentoAnagrafica_b1f", "collegamentoAnagrafica.b1f")]
    class collegamentoAnagrafica_b1f : UserFormBase
    {
        public collegamentoAnagrafica_b1f()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.selectAg = ((SAPbouiCOM.EditText)(this.GetItem("selectAg").Specific));
            this.selectAg.ChooseFromListAfter += new SAPbouiCOM._IEditTextEvents_ChooseFromListAfterEventHandler(this.selectAg_ChooseFromListAfter);
            this.selectBp = ((SAPbouiCOM.EditText)(this.GetItem("selectBp").Specific));
            this.btnAdd = ((SAPbouiCOM.Button)(this.GetItem("btnAdd").Specific));
            this.btnStop = ((SAPbouiCOM.Button)(this.GetItem("btnStop").Specific));

            //   PREMO OK PER AGGIUNGERE COLLEGAMENTO
            this.btnAdd.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.btnOk_ClickBefore);
            //   CHIUDO FORM
            this.btnStop.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.btnStop_ClickBefore);
            
            this.OnCustomInitialize();

        }
        
        //AGGIUNGO COLLEGAMENTO AGENTE/BP
        private void btnOk_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            oForm = (SAPbouiCOM.IForm)oApp.Forms.ActiveForm;
            oRecordSet = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            
            int inserisco = 1;
            int newCode = 0;

            if(oForm.Mode == SAPbouiCOM.BoFormMode.fm_ADD_MODE){
                try
                {
                    String bp = this.selectBp.Value;
                    String ag = this.selectAg.Value;

                    //BP
                    String qControlBp = "SELECT * FROM \"OCRD\" WHERE  \"CardCode\" = \'" + bp + "\' ";
                    oRecordSet.DoQuery(qControlBp);
                    String bpName = oRecordSet.Fields.Item("CardName").Value.ToString();
                    String bpCode = oRecordSet.Fields.Item("CardCode").Value.ToString();
                    
                    //AGENTE
                    String qControlAddVendite = "SELECT * FROM \"OSLP\" WHERE  \"SlpName\" = \'" + ag + "\' ";
                    oRecordSet.DoQuery(qControlAddVendite);
                    String agName = oRecordSet.Fields.Item("SlpName").Value.ToString();
                    String agCode = oRecordSet.Fields.Item("SlpCode").Value.ToString();

                    //RECUPERO E CONVERTO ULTIMO Code
                    String qLastCode = "SELECT " + Program.SQL_limit1 + " * FROM \"@FC_OBPA\"  ORDER BY \"U_incr\" DESC " + Program.HANA_limit1 + " ";
                    oRecordSet.DoQuery(qLastCode);
                    if (oRecordSet.Fields.Item("U_incr").Value.ToString().Equals(""))
                    {
                        newCode = 1;
                    }
                    else
                    {
                        newCode = Int32.Parse(oRecordSet.Fields.Item("U_incr").Value.ToString());
                        newCode = newCode + 1;
                    }
                    String newCodeDb = newCode.ToString();

                    //INSERISCO RELAZIONE IN TAB CUSTOM
                    if (inserisco == 1)
                    {
                        SAPbobsCOM.UserTable oUserTable;
                        oUserTable = oCompany.UserTables.Item("FC_OBPA");
                        oUserTable.Code = newCodeDb;
                        oUserTable.Name = "bpag" + newCodeDb;
                        oUserTable.UserFields.Fields.Item("U_SlpCode").Value = agCode;
                        oUserTable.UserFields.Fields.Item("U_SlpName").Value = agName;
                        oUserTable.UserFields.Fields.Item("U_CardCode").Value = bpCode;
                        oUserTable.UserFields.Fields.Item("U_CardName").Value = bpName;
                        oUserTable.UserFields.Fields.Item("U_incr").Value = newCodeDb;
                        int result = oUserTable.Add();
                        if (result != 0)
                        {
                            Application.SBO_Application.MessageBox(oCompany.GetLastErrorDescription() + " - INSERT", 1, "Ok", "", "");
                        }
                        else
                        {
                            //Application.SBO_Application.MessageBox("Collegamento creato.", 1, "Ok", "", "");
                            oForm.Close();
                        }
                    }
                }
                catch (InvalidCastException e)
                {
                    Application.SBO_Application.MessageBox("Non è stato possibile collegare l'agente al BP", 1, "Ok", "", "");
                }

            }
        }

        //DISMISS
        private void btnStop_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            oForm = (SAPbouiCOM.IForm)oApp.Forms.ActiveForm;
            oForm.Close();
        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
            this.LoadAfter += new LoadAfterHandler(this.Form_LoadAfter);
            this.oApp = (SAPbouiCOM.Application)Application.SBO_Application;
            //this.oForm = (SAPbouiCOM.Form)oApp.Forms.ActiveForm;this.UIAPIRawForm.
            this.oForm =  this.UIAPIRawForm;
            
        }

        private void Form_LoadAfter(SAPbouiCOM.SBOItemEventArg pVal)
        {
           // throw new System.NotImplementedException();

        }

        private void OnCustomInitialize()
        {

        }

        //SELEZIONE FORNITORI
        private void selectAg_ChooseFromListAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            this.oForm = (SAPbouiCOM.IForm)oApp.Forms.ActiveForm;
            oForm.Items.Item("7").Click(SAPbouiCOM.BoCellClickType.ct_Regular);
            SAPbouiCOM.ISBOChooseFromListEventArg chflarg = (SAPbouiCOM.ISBOChooseFromListEventArg)pVal;
            
            SAPbouiCOM.DataTable dt = chflarg.SelectedObjects;
            if (dt != null)
            {
                try
                {
                    this.selectAg.Value = dt.GetValue("SlpName", 0).ToString();
                }
                catch (System.Runtime.InteropServices.COMException e)
                {
                    //Application.SBO_Application.MessageBox(e.ToString()+"ehy fonzi", 1, "Ok", "", "");
                }
            }
            else
            {
                if (this.selectAg.Value == "")
                {
                    this.selectAg.Value = "";
                }
            }

        }

        //DICHIARAZIZONI SISTEMA
        public SAPbouiCOM.Application oApp;
        public SAPbouiCOM.IForm oForm;
        public SAPbobsCOM.Company oCompany = Program.oCompany;
        public SAPbobsCOM.Recordset oRecordSet;

        //TEMPLATE FORM
        private SAPbouiCOM.Button btnAdd;
        private SAPbouiCOM.Button btnStop;
        private SAPbouiCOM.EditText selectAg;
        private SAPbouiCOM.EditText selectBp;

    }
}
