
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace Provvigioni
{

    [FormAttribute("Provvigioni.provvElaborate_b1f", "provvElaborate.b1f")]
    class provvElaborate_b1f : UserFormBase
    {
        public provvElaborate_b1f()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            
            this.OnCustomInitialize();

        }
        //DETTAGLIO
        private void Grid2_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            this.oForm = ((SAPbouiCOM.Form)(this.oApp.Forms.ActiveForm));
            /*try
            {
                oForm.DataSources.DataTables.Add("filtroDettaglioLiq");
            }
            catch (System.Runtime.InteropServices.COMException e)
            {
                //Application.SBO_Application.MessageBox(e.ToString(), 1, "Ok", "", "");
            }
            //VISUALIZZO IL DETTAGLIO
            if (pVal.ColUID == "Selezione")
            {
                 if (pVal.Row != -1)
                 {
                     var valore = this.Grid2.DataTable.GetValue(0, pVal.Row).ToString();
                     if (valore == "Y")
                     {
                         
                         String numEla = this.Grid2.DataTable.GetValue(1, pVal.Row).ToString();
                         String qD = "SELECT \"U_numEla\" as \"N° Elaborazione\",\"U_SlpName\" as \"Agente\",\"U_n_fattura\" as \"N° Fattura\",\"U_costo_str\" as \"Costo\" from \"@FC_ANTP\" where \"U_numEla\" = \'" + numEla + "\' ";

                         oForm.DataSources.DataTables.Item("filtroDettaglioLiq").ExecuteQuery(qD);
                         //oForm.DataSources.Columns.Add("#", SAPbouiCOM.BoFieldsType.ft_Text, 20);


                         Grid3.DataTable = oForm.DataSources.DataTables.Item("filtroDettaglioLiq");

                         // DISABLE COLONNE
                         this.Grid3.Columns.Item(0).Editable = false;
                         this.Grid3.Columns.Item(1).Editable = false;
                         this.Grid3.Columns.Item(2).Editable = false;
                         this.Grid3.Columns.Item(3).Editable = false;

                     }
                 }
            }*/

        }
        //GENERA STAMPA DETTAGLIO
        private void Button2_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            this.oForm = ((SAPbouiCOM.Form)(this.oApp.Forms.ActiveForm));
            try
            {
                oForm.DataSources.DataTables.Add("filtroDettaglioLiq");
            }
            catch (System.Runtime.InteropServices.COMException e)
            {
                //Application.SBO_Application.MessageBox(e.ToString(), 1, "Ok", "", "");
            }
            this.UIAPIRawForm.Freeze(true);
            //PIALLO TABELLA STAMPA
            String q = "DELETE FROM \"@FC_PRSL\" ";
            oRecordSetDel.DoQuery(q);

            int numRows = Grid2.Rows.Count;

            string[] righeIDS = new string[numRows];

            for (int i = 0; i < numRows; i++)
            {
                //SCRIVO LE ELABORAZIONI IN STAMPA
                String nEla = Grid2.DataTable.GetValue("N° Elaborazione", i).ToString();
                String select = Grid2.DataTable.GetValue("Selezione", i).ToString();
                try
                {
                    if (select.Equals("Y"))
                    {
                        String qSel = "SELECT * FROM \"@FC_ANTP\" WHERE  \"U_numEla\" = \'" + nEla + "\' ";
                        oRecordSet.DoQuery(qSel);

                        while (!oRecordSet.EoF)
                        {

                            String Code = oRecordSet.Fields.Item("Code").Value.ToString();
                            String Name = oRecordSet.Fields.Item("Name").Value.ToString();
                            String SlpCode = oRecordSet.Fields.Item("U_SlpCode").Value.ToString();
                            String n_fattura = oRecordSet.Fields.Item("U_n_fattura").Value.ToString();
                            String SlpName = oRecordSet.Fields.Item("U_SlpName").Value.ToString();
                            String Tipo = oRecordSet.Fields.Item("U_tipo").Value.ToString();
                            String costo_str = oRecordSet.Fields.Item("U_costo_str").Value.ToString();
                            String dt_inizio = oRecordSet.Fields.Item("U_data_pagamento").Value.ToString();
                            String annoIni = dt_inizio.Substring(6, 4);
                            String meseIni = dt_inizio.Substring(3, 2);
                            String giornoIni = dt_inizio.Substring(0, 2);
                            String annoIni_db = annoIni + meseIni + giornoIni;
                            String data_pagamento = annoIni_db;
                            String incr = oRecordSet.Fields.Item("U_incr").Value.ToString();
                            String numEla = oRecordSet.Fields.Item("U_numEla").Value.ToString();
                            String progEla = oRecordSet.Fields.Item("U_progEla").Value.ToString();
                            String totaleEla = oRecordSet.Fields.Item("U_totaleEla").Value.ToString();
                            String totFattura = oRecordSet.Fields.Item("U_totaleFt").Value.ToString().Replace(",",".");


                            //CALCOLO L'IMPORTO BASE PER LE PROVVIGIONI
                            double imponibileProvv = 0;
                            String qRighe = "";
                            if (Tipo.Equals("Ft"))
                            {
                                qRighe = "SELECT inv1.\"TotalSumSy\",inv1.\"ItemCode\" from \"INV1\" inv1,\"OINV\" oinv WHERE inv1.\"DocEntry\" = oinv.\"DocEntry\" AND oinv.\"DocNum\" = '" + n_fattura + "' ";
                            }
                            else
                            {
                                qRighe = "SELECT inv1.\"TotalSumSy\",inv1.\"ItemCode\" from \"RIN1\" inv1,\"ORIN\" oinv WHERE inv1.\"DocEntry\" = oinv.\"DocEntry\" AND oinv.\"DocNum\" = '" + n_fattura + "' ";
                            }

                            oRecordSetRighe.DoQuery(qRighe);

                            while (!oRecordSetRighe.EoF)
                            {
                                String ItemCode = oRecordSetRighe.Fields.Item("ItemCode").Value.ToString();
                                //A MONTE CONTROLLO SE ARTICOLO ESCLUSO
                                String qEx = "SELECT \"U_startString\" FROM \"@FC_PRIT\" ";
                                oRecordSetGenerale.DoQuery(qEx);
                                int calcoloEx = 0;
                                String artEx = "";
                                while (!oRecordSetGenerale.EoF)
                                {
                                    artEx = oRecordSetGenerale.Fields.Item("U_startString").Value.ToString();
                                    int artLenght = artEx.Length;
                                    if (artEx.Equals(ItemCode.Substring(0, artLenght)))
                                    {
                                        calcoloEx = 1;
                                    }
                                    oRecordSetGenerale.MoveNext();
                                }
                                if (calcoloEx == 0)
                                {
                                    imponibileProvv = imponibileProvv + double.Parse((oRecordSetRighe.Fields.Item("TotalSumSy").Value.ToString()).Replace(".", ","));
                                }
                                oRecordSetRighe.MoveNext();
                            }
                            String str_imponibileProvv = imponibileProvv.ToString().Replace(",", ".");
                            String percentMedia = Math.Round((100 * double.Parse((costo_str).Replace(".", ","))) / imponibileProvv, 2).ToString();
                            String ins = "INSERT INTO \"@FC_PRSL\" (\"Code\",\"Name\",\"U_SlpCode\",\"U_n_fattura\",\"U_SlpName\",\"U_costo_str\",\"U_data_pagamento\",\"U_incr\",\"U_numEla\",\"U_progEla\",\"U_totaleEla\",\"U_totaleFt\",\"U_totaleImpProvv\") values (\'" + Code + "\',\'" + Name + "\',\'" + SlpCode + "\',\'" + n_fattura + "\',\'" + SlpName + "\',\'" + costo_str + "\',\'" + data_pagamento + "\',\'" + incr + "\',\'" + numEla + "\',\'" + progEla + "\',\'" + totaleEla + "\',\'" + totFattura + "\',\'" + str_imponibileProvv + "\') ";
                            oRecordSetIns.DoQuery(ins);

                            oRecordSet.MoveNext();
                        }
                    }

                }
                catch (System.Runtime.InteropServices.COMException e)
                {
                    //Application.SBO_Application.MessageBox(e.ToString(), 1, "Ok", "", "");
                }
            }
            String qD = "SELECT \"U_numEla\" as \"N° Elaborazione\",\"U_SlpName\" as \"Agente\",\"U_n_fattura\" as \"N° Fattura\",\"U_totaleFt\" as \"Tot. Fattura\",\"U_totaleImpProvv\" as \"Imp. Provv.\",\"U_costo_str\" as \"Costo\" from \"@FC_PRSL\"  ";

            oForm.DataSources.DataTables.Item("filtroDettaglioLiq").ExecuteQuery(qD);
            //oForm.DataSources.Columns.Add("#", SAPbouiCOM.BoFieldsType.ft_Text, 20);


            Grid3.DataTable = oForm.DataSources.DataTables.Item("filtroDettaglioLiq");

            // DISABLE COLONNE
            this.Grid3.Columns.Item(0).Editable = false;
            this.Grid3.Columns.Item(1).Editable = false;
            this.Grid3.Columns.Item(2).Editable = false;
            this.Grid3.Columns.Item(3).Editable = false;
            this.Grid3.Columns.Item(4).Editable = false;
            this.Grid3.Columns.Item(5).Editable = false;
            //this.Grid3.Columns.Item(6).Editable = false;
            this.UIAPIRawForm.Freeze(false);

        }
        //FILTRI SELEZIONE
        private void Button5_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            aggiornaGrigliaQuery();
            elementiForm();

            //METTO TUTTI I CHECK A TRUE
            int numRows = Grid2.Rows.Count;

            string[] righeIDS = new string[numRows];
            for (int i = 0; i < numRows; i++)
            {
                this.Grid2.DataTable.SetValue(0, i, "Y");
            }
        }

        //ANNULLA ELABORAZIONE
        private void Button0_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            this.oForm = ((SAPbouiCOM.IForm)(this.oApp.Forms.ActiveForm));
            try
            {
                oForm.DataSources.DataTables.Add("filtroDettaglioLiqFine");
            }
            catch (System.Runtime.InteropServices.COMException e)
            {
                //Application.SBO_Application.MessageBox(e.ToString(), 1, "Ok", "", "");
            }

            this.UIAPIRawForm.Freeze(true);

            //SETTO VALORE INIZIALE UPDATE COSTO
            int numRows = Grid2.Rows.Count;

            string[] righeIDS = new string[numRows];

            for (int i = 0; i < numRows; i++)
            {
                
                
                //elimino
                String nEla = Grid2.DataTable.GetValue("N° Elaborazione", i).ToString();
                String select = Grid2.DataTable.GetValue("Selezione", i).ToString();
                try
                {
                    if (select.Equals("Y"))
                    {
                        String q = "DELETE FROM \"@FC_ANTP\" WHERE  \"U_numEla\" = \'" + nEla + "\' ";
                        oRecordSet.DoQuery(q);
                    }

                }
                catch (System.Runtime.InteropServices.COMException e)
                {
                    //Application.SBO_Application.MessageBox(e.ToString(), 1, "Ok", "", "");
                }

               
                
                //this.Grid3.DataTable.Clear();
            }

            String qDX = "select 'N' as \"Selezione\",\"U_numEla\" as \"N° Elaborazione\",'data_provvigione' as \"Data Liquidato\",'totale_ela' as \"Totale\" from \"@FC_ANTP\" Group by \"U_numEla\" ";
            oForm.DataSources.DataTables.Item("filtroDettaglioLiqFine").ExecuteQuery(qDX);

            Grid2.DataTable = oForm.DataSources.DataTables.Item("filtroDettaglioLiqFine");
            elementiForm();
            this.UIAPIRawForm.Freeze(false);

        }
        //STAMPA LIQUIDATE
        private void Button3_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            //TEST INTERNO
            //Application.SBO_Application.Menus.Item("e74818f915bf4e10b20a3d25f04758a7").Activate();
            //TEST
            //Application.SBO_Application.Menus.Item("3b9c8f2056664bf7a48881c093a0aef7").Activate();
            //PRODUZIONE
            Application.SBO_Application.Menus.Item("66c276e910a244aaa5c97376c4180377").Activate();

        }
        //STAMPA PREFATTURA
        private void Button4_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            //TEST INTERNO
            //Application.SBO_Application.Menus.Item("b3da5d85aaed4db382f28038f8fa40d0").Activate();
            //TEST
            //Application.SBO_Application.Menus.Item("f5ea7fb7b75f4002b2f93462c7f3f589").Activate();
            //PRODUZIONE
            Application.SBO_Application.Menus.Item("bdef32b9f0b0427ca92ce034a6536487").Activate();

        }
        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }

        private void OnCustomInitialize()
        {
            this.oApp = (SAPbouiCOM.Application)Application.SBO_Application;
            //this.oForm = (SAPbouiCOM.Form)oApp.Forms.ActiveForm;this.UIAPIRawForm.
            this.oForm = this.UIAPIRawForm;
            this.Button0 = ((SAPbouiCOM.Button)(this.GetItem("Item_2").Specific));
            this.Button0.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.Button0_ClickBefore);
            this.Button2 = ((SAPbouiCOM.Button)(this.GetItem("Item_0").Specific));
            this.Button2.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.Button2_ClickBefore);
            this.Button3 = ((SAPbouiCOM.Button)(this.GetItem("Item_1").Specific));
            this.Button3.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.Button3_ClickBefore);
            this.preFattura = ((SAPbouiCOM.Button)(this.GetItem("Item_5").Specific));
            this.preFattura.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.Button4_ClickBefore);
            this.oRecordSet = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.oRecordSetIns = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.oRecordSetDel = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.oRecordSetRighe = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.oRecordSetGenerale = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.Grid2 = ((SAPbouiCOM.Grid)(this.GetItem("Item_3").Specific));
            this.Grid2.ClickAfter += new SAPbouiCOM._IGridEvents_ClickAfterEventHandler(this.Grid2_ClickAfter);
            this.Anno = ((SAPbouiCOM.ComboBox)(this.GetItem("Item_10").Specific));
            this.Trim = ((SAPbouiCOM.ComboBox)(this.GetItem("Item_9").Specific));
            this.SelAgente = ((SAPbouiCOM.EditText)(this.GetItem("Item_11").Specific));
            this.OptionTutti = ((SAPbouiCOM.OptionBtn)(this.GetItem("Item_12").Specific));
            this.OptionSingolo = ((SAPbouiCOM.OptionBtn)(this.GetItem("Item_13").Specific));
            this.Seleziona = ((SAPbouiCOM.Button)(this.GetItem("Item_14").Specific));
            this.Seleziona.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.Button5_ClickBefore);
            
           
            // COLONNE CUSTOM
            this.Grid2.Columns.Item(0).TitleObject.Sortable = true;
            Grid2.Columns.Item(0).Type = SAPbouiCOM.BoGridColumnType.gct_CheckBox;
            // DISABLE COLONNE
            this.Grid2.Columns.Item(1).Editable = false;
            //this.Grid2.Columns.Item(2).Editable = false;
            //this.Grid2.Columns.Item(3).Editable = false;
            this.Grid3 = ((SAPbouiCOM.Grid)(this.GetItem("Item_4").Specific));

            //SETTO radio singolo a not checked
            OptionSingolo.GroupWith("Item_12");
            OptionTutti.Selected = true;

            //assegno gli anni
            creaDdlAnni();

            this.elementiForm();
        }

        //ASSEGNA ANNO
        private void creaDdlAnni()
        {
            Anno.ValidValues.Add(anno_base, anno_base);
            int anno = int.Parse(DateTime.Now.ToString("yyyy").ToString());
            int anno_da = anno + 1;

            for (int c = 0; c < 3; c++)
            {
                Anno.ValidValues.Add(anno_da.ToString(), anno_da.ToString());

                anno_da = anno_da - 1;
            }

            Anno.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
            Anno.Select(anno_base, SAPbouiCOM.BoSearchKey.psk_ByValue);
        }

        public void aggiornaGrigliaEl()
        {
            SAPbouiCOM.DataTable riga = Grid2.DataTable;

            int numRows = Grid2.Rows.Count;

            string[] righeIDS = new string[numRows];
            for (int i = 0; i < numRows; i++)
            {
                try
                {
                    if (numRows > 0 && !Grid2.DataTable.GetValue("N° Elaborazione", i).ToString().Equals(""))
                    {
                        
                        String numElab = Grid2.DataTable.GetValue("N° Elaborazione", i).ToString();
                        String qDati = "select " + Program.SQL_limit1 + " \"U_data_pagamento\",\"U_totaleEla\" FROM \"@FC_ANTP\" WHERE \"U_numEla\" = '" + numElab + "' " + Program.HANA_limit1 + " ";
                        oRecordSet.DoQuery(qDati);
                        //DATA
                        Grid2.DataTable.SetValue("Data Liquidato", i, oRecordSet.Fields.Item("U_data_pagamento").Value.ToString().Substring(0,10));
                        //TOTALE
                        Grid2.DataTable.SetValue("Totale", i, oRecordSet.Fields.Item("U_totaleEla").Value.ToString());

                    }
                }
                catch (System.Runtime.InteropServices.COMException e)
                {
                    Application.SBO_Application.MessageBox(e.ToString(), 1, "Ok", "", "");
                }
            }
        }

        public void aggiornaGrigliaQuery()
        {
            SAPbouiCOM.DataTable riga = Grid2.DataTable;

            //AGENTE
            String SlpName = this.SelAgente.Value.Trim();
            String filtro_agente = "";
            if (OptionSingolo.Selected == true)
            {
                filtro_agente = " AND \"U_SlpName\" = '" + SlpName + "' ";
            }

            //ANNO
            String AnnoDB = this.Anno.Value.Trim();
            String filtro_anno = "";
            if (!AnnoDB.Equals(anno_base))
            {
                filtro_anno = " AND \"U_annoEla\" = '" + AnnoDB + "' ";
            }

            //PERIODO
            String PeriodoDB = this.Trim.Value.Trim();
            String filtro_periodo = "";
            if (!PeriodoDB.Equals(periodo_base))
            {
                filtro_periodo = " AND \"U_periodoEla\" = '" + PeriodoDB + "' ";
            }

            String  queryFiltro = " select 'N' as \"Selezione\",\"U_numEla\" as \"N° Elaborazione\",'data_provvigione' as \"Data Liquidato\",'totale_ela' as \"Totale\" ";
                    queryFiltro += " FROM \"@FC_ANTP\" ";
                    queryFiltro += " WHERE \"Code\" <> '' ";
                    queryFiltro += filtro_agente;
                    queryFiltro += filtro_anno;
                    queryFiltro += filtro_periodo;
                    queryFiltro += " Group by \"U_numEla\" ";

            try
            {
                oForm.DataSources.DataTables.Add("filtroELAB");
            }
            catch (System.Runtime.InteropServices.COMException e)
            {
                //Application.SBO_Application.MessageBox(e.ToString(), 1, "Ok", "", "");
            }

            oForm.DataSources.DataTables.Item("filtroELAB").ExecuteQuery(queryFiltro);
            Grid2.DataTable = oForm.DataSources.DataTables.Item("filtroELAB");
        }

        private void elementiForm()
        {
            // COLONNE CUSTOM
            this.Grid2.Columns.Item(0).TitleObject.Sortable = true;
            Grid2.Columns.Item(0).Type = SAPbouiCOM.BoGridColumnType.gct_CheckBox;
            // DISABLE COLONNE
            this.Grid2.Columns.Item(1).Editable = false;
            this.Grid2.Columns.Item(2).Editable = false;
            this.Grid2.Columns.Item(3).Editable = false;


            aggiornaGrigliaEl();
        }

        //DICHIARAZIZONI SISTEMA
        public SAPbouiCOM.Application oApp;
        public SAPbouiCOM.IForm oForm;
        public SAPbobsCOM.Company oCompany = Program.oCompany;
        public SAPbobsCOM.Recordset oRecordSet;
        public SAPbobsCOM.Recordset oRecordSetIns;
        public SAPbobsCOM.Recordset oRecordSetDel;
        public SAPbobsCOM.Recordset oRecordSetPerc;
        public SAPbobsCOM.Recordset oRecordSetFt;
        public SAPbobsCOM.Recordset oRecordSetRighe;
        public SAPbobsCOM.Recordset oRecordSetGenerale;
        public SAPbouiCOM.Conditions oConditions;
        public SAPbouiCOM.Condition oCondition;
        public SAPbouiCOM.DBDataSource oDBDataSource;
        public SAPbouiCOM.DataTable filtroData; 
        private SAPbouiCOM.Grid Grid1;
        private SAPbouiCOM.Button Button0;
        private SAPbouiCOM.Grid Grid2;
        private SAPbouiCOM.Grid Grid3;
        private SAPbouiCOM.Button Button1;
        private SAPbouiCOM.Button Button2;
        private SAPbouiCOM.Button Button3;
        private SAPbouiCOM.Button preFattura;
        private SAPbouiCOM.ComboBox Trim;
        private SAPbouiCOM.ComboBox Anno;
        private SAPbouiCOM.EditText SelAgente;
        private SAPbouiCOM.OptionBtn OptionTutti;
        private SAPbouiCOM.OptionBtn OptionSingolo;
        private SAPbouiCOM.Button Seleziona;
        private SAPbouiCOM.Button Button5;

        String anno_base = "";
        String periodo_base = "";

    }
}
