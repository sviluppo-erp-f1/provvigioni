
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace Provvigioni
{

    [FormAttribute("Provvigioni.provvMaturande_b1f", "provvMaturande.b1f")]
    class provvMaturande_b1f : UserFormBase
    {
        public provvMaturande_b1f()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }


        private void OnCustomInitialize()
        {
            this.oApp = (SAPbouiCOM.Application)Application.SBO_Application;
            this.oForm = this.UIAPIRawForm;
            this.oRecordSet = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.oRecordSetIns = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.oRecordSetDel = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.oRecordSetRighe = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.oRecordSetGenerale = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.oRecordSetTotale = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));

            this.selectAg = ((SAPbouiCOM.EditText)(this.GetItem("Item_4").Specific));
            this.dtInizio = ((SAPbouiCOM.EditText)(this.GetItem("Item_5").Specific));
            this.dtFine = ((SAPbouiCOM.EditText)(this.GetItem("Item_6").Specific));
            this.calcola = ((SAPbouiCOM.Button)(this.GetItem("Item_7").Specific));
            this.listaMaturande = ((SAPbouiCOM.Grid)(this.GetItem("Item_1").Specific));
            this.stampa = ((SAPbouiCOM.Button)(this.GetItem("Item_8").Specific));
            this.totMat = ((SAPbouiCOM.EditText)(this.GetItem("Item_12").Specific));
            this.OptionTutti = ((SAPbouiCOM.OptionBtn)(this.GetItem("Item_9").Specific));
            this.OptionSingolo = ((SAPbouiCOM.OptionBtn)(this.GetItem("Item_10").Specific));
            OptionSingolo.GroupWith("Item_9");

            this.calcola.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.Button3_ClickBefore);
            this.stampa.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.Button4_ClickBefore);

            //SETTO radio singolo a not checked
            OptionTutti.Selected = true;
        }

        //CALCOLA MATURANDE
        private void Button3_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            this.UIAPIRawForm.Freeze(true);

            

            //RIEMPIO TABELLA
            //CALCOLO L'IMPORTO BASE PER LE PROVVIGIONI
            double totaleMaturande = 0;
            String qRighe = qProvv();

            if (!qRighe.Equals(""))
            {
                //PIALLO TABELLA STAMPA
                /*String q = "DELETE FROM \"@FC_ANMA\" ";
                oRecordSetDel.DoQuery(q);*/


                //RECUPERO IL NUM PER elaborazione riga
                int newNumEla = 0;
                String qLastCodeEla = "SELECT " + Program.SQL_limit1 + " \"U_NUM_ELA\" FROM \"@FC_ANRM\"  ORDER BY CAST(\"Code\" AS INT )  DESC " + Program.HANA_limit1 + " ";
                oRecordSetNumEla = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
                oRecordSetNumEla.DoQuery(qLastCodeEla);
                if (oRecordSetNumEla.Fields.Item("U_NUM_ELA").Value.ToString().Equals(""))
                {
                    newNumEla = 1;
                }
                else
                {
                    newNumEla = Int32.Parse(oRecordSetNumEla.Fields.Item("U_NUM_ELA").Value.ToString());
                    newNumEla = newNumEla + 1;
                }

                oRecordSetRighe.DoQuery(qRighe);

                while (!oRecordSetRighe.EoF)
                {

                    //CALCOLO il rapporto tra imponibile e liquidato
                    double Imponibile = double.Parse(oRecordSetRighe.Fields.Item("Imponibile").Value.ToString());
                    double Liquidato = double.Parse(oRecordSetRighe.Fields.Item("Liquidato").Value.ToString());
                    double Residuo = double.Parse(oRecordSetRighe.Fields.Item("Residuo").Value.ToString());

                    double rapporto_pagato = (Residuo * 100) / Imponibile;

                    //CALCOLO IL COSTO
                    String DocNum = oRecordSetRighe.Fields.Item("N° Doc.").Value.ToString();
                    String tipoDoc = oRecordSetRighe.Fields.Item("Tipo").Value.ToString();
                    calcoloMaturande cm = new calcoloMaturande(DocNum, tipoDoc, Residuo);
                    double costo_parziale = cm.provvigioneCalcolata(newNumEla, this.dtInizio.Value, this.dtFine.Value,rapporto_pagato);

                    double costo_finale = ((rapporto_pagato * costo_parziale) / 100);

                    //RECUPERO E CONVERTO ULTIMO Code anag
                    int Code = 0;
                    String qLastCodeA = "SELECT " + Program.SQL_limit1 + " * FROM \"@FC_ANMA\"  ORDER BY CAST(\"Code\" AS INT) DESC " + Program.HANA_limit1 + " ";
                    oRecordSet.DoQuery(qLastCodeA);
                    if (oRecordSet.Fields.Item("Code").Value.ToString().Equals(""))
                    {
                        Code = 1;
                    }
                    else
                    {
                        Code = Int32.Parse(oRecordSet.Fields.Item("Code").Value.ToString());
                        Code = Code + 1;
                    }

                    String Name = Code.ToString() + "maturande";
                    String SlpCode = oRecordSetRighe.Fields.Item("SlpCode").Value.ToString();
                    String n_fattura = DocNum;
                    String SlpName = oRecordSetRighe.Fields.Item("Agente").Value.ToString();
                    String totFattura = Imponibile.ToString().Replace(",", ".");
                    String totResiduo = Residuo.ToString().Replace(",", ".");
                    String str_imponibileProvv = costo_finale.ToString().Replace(",", ".");
                    String dt_inizio = oRecordSetRighe.Fields.Item("Data Incasso").Value.ToString();
                    String annoIni = dt_inizio.Substring(6, 4);
                    String meseIni = dt_inizio.Substring(3, 2);
                    String giornoIni = dt_inizio.Substring(0, 2);
                    String annoIni_db = annoIni + meseIni + giornoIni;
                    String data_pagamento = annoIni_db;

                    //DATE PERIODO
                    String dtInizio = this.dtInizio.Value;
                
                    String dtFine = this.dtFine.Value;
                    if (str_imponibileProvv.Equals("∞"))
                    {
                        str_imponibileProvv = "0";
                    }
                    //INSERISCO IN TABELLA MATURANDE
                    /*String ins = "INSERT INTO \"@FC_ANMA\" (\"Code\",\"Name\",\"U_SlpCode\",\"U_n_fattura\",\"U_SlpName\",\"U_data_pagamento\",\"U_totaleFt\",\"U_residuo\",\"U_totaleImpProvv\",\"U_data_inizio\",\"U_data_fine\") values (\'" + Code + "\',\'" + Name + "\',\'" + SlpCode + "\',\'" + n_fattura + "\',\'" + SlpName + "\',\'" + data_pagamento + "\',\'" + totFattura + "\',\'" + totResiduo + "\',\'" + str_imponibileProvv + "\',\'" + dtInizio + "\',\'" + dtFine + "\') ";
                    oRecordSetIns.DoQuery(ins);*/

                    oRecordSetRighe.MoveNext();
                }

                //VISUALIZZO IN GRIGLIA
                String qGriglia = " Select \"U_SlpName\" as \"Agente\",SUM(\"U_TOTALE_RIGA\") as \"Tot. Riga\",SUM(\"U_RESIDUO\") as \"Provv. Globale\", ";
                       qGriglia += " SUM(\"U_TOTALE_IMP_PROVV\") as \"Tot. Mat.\",\"U_BUnit\" as \"B.Unit\" ";
                       qGriglia += " FROM \"@FC_ANRM\" ";
                       qGriglia += " WHERE \"U_NUM_ELA\" = \'" + newNumEla + "\' ";
                       qGriglia += " GROUP BY \"U_SlpCode\",\"U_SlpName\",\"U_NUM_ELA\",\"U_BUnit\" ";
            
                try
                {
                    oForm.DataSources.DataTables.Add("filtroDettaglioMat");
                }
                catch (System.Runtime.InteropServices.COMException e)
                {
                    //Application.SBO_Application.MessageBox(e.ToString(), 1, "Ok", "", "");
                }

                oForm.DataSources.DataTables.Item("filtroDettaglioMat").ExecuteQuery(qGriglia);
                listaMaturande.DataTable = oForm.DataSources.DataTables.Item("filtroDettaglioMat");

                //ASSEGNO totale
                String qTot = " select SUM(\"U_TOTALE_IMP_PROVV\") as \"TOT\" from (select * from \"@FC_ANRM\" where \"U_NUM_ELA\" = (select MAX(\"U_NUM_ELA\") from \"@FC_ANRM\" AS T0 )) AS T1";
                oRecordSetTotale.DoQuery(qTot);

                totMat.Value = oRecordSetTotale.Fields.Item("TOT").Value.ToString();

                // DISABLE COLONNE
                this.listaMaturande.Columns.Item(0).Editable = false;
                this.listaMaturande.Columns.Item(1).Editable = false;
                this.listaMaturande.Columns.Item(2).Editable = false;
                this.listaMaturande.Columns.Item(3).Editable = false;
                this.listaMaturande.Columns.Item(4).Editable = false;
            }
            //this.Grid3.Columns.Item(6).Editable = false;
            this.UIAPIRawForm.Freeze(false);

        }

        //STAMPA MATURANDE
        private void Button4_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            //TEST INTERNO
            //Application.SBO_Application.Menus.Item("e74818f915bf4e10b20a3d25f04758a7").Activate();
            //TEST
            //Application.SBO_Application.Menus.Item("326cb08a3f6a400da2029a119299ea38").Activate();
            //PRODUZIONE
            Application.SBO_Application.Menus.Item("084c900fbb344a868abe7c1caa41c3a8").Activate();

        }

        //QUERY BASE DATI PROVVIGIONI
        public string qProvv()
        {

            String qP = "";


            //FILTRO AGENTE
            String SlpCode = this.selectAg.Value.Trim();
            if (!SlpCode.Equals("Tutti gli agenti"))
            {
                filtro_agente = SlpCode;
            }
            

            //FILTRO DATA INI
            String dtIni = this.dtInizio.Value.Trim();

            if (!dtIni.Equals(""))
            {
                String annoIni = dtIni.Substring(0, 4);
                String meseIni = dtIni.Substring(4, 2);
                String giornoIni = dtIni.Substring(6, 2);
                String dtIniDB = annoIni + meseIni + giornoIni;
                filtro_dtIni = dtIniDB;
            }
            else
            {
                filtro_dtIni = "";
            }
            //FILTRO DATA END
            dtEnd = this.dtFine.Value.Trim();

            if (!dtEnd.Equals(""))
            {
                String annoEnd = dtEnd.Substring(0, 4);
                String meseEnd = dtEnd.Substring(4, 2);
                String giornoEnd = dtEnd.Substring(6, 2);
                String dtEndDB = annoEnd + meseEnd + giornoEnd;
                filtro_dtEnd = dtEndDB;
            }
            else
            {
                filtro_dtEnd = "";
            }

            //addetto vendite
            String txtSlpCode = "";
            if (OptionTutti.Selected == true)
            {
                txtSlpCode = "%%";
            }
            else
            {
                String qControlAddVendite = "SELECT * FROM \"OSLP\" WHERE  \"SlpName\" = \'" + filtro_agente + "\' ";
                oRecordSet.DoQuery(qControlAddVendite);
                String returnAddVendite = oRecordSet.Fields.Item("SlpName").Value.ToString();
                txtSlpCode = oRecordSet.Fields.Item("SlpCode").Value.ToString();
                //SlpCodeDB = Int32.Parse(txtSlpCode);
            }
            

            if (filtro_dtIni.Equals("") || filtro_dtEnd.Equals("") || (SlpCode.Equals("") && OptionSingolo.Selected==true))
            {
                Application.SBO_Application.MessageBox("Inserire i campi di ricerca", 1, "Ok", "", "");
            }
            else
            {
                
                if (Program.HANA_DB == 1)
                {
                    qP = "call FO_datiTestateMaturande('" + txtSlpCode + "','" + filtro_dtIni + "','" + filtro_dtEnd + "','20150101','" + tipoLiquid + "');";
                }
                if (Program.SQL_DB == 1)
                {
                    qP = "exec FO_datiTestateMaturande @agente = '" + txtSlpCode + "',@dataIni ='" + filtro_dtIni + "',@dataEnd  ='" + filtro_dtEnd + "',@dataDoc  ='20150101',@tipoLiquid = '" + tipoLiquid + "';";
                }
                

            }


            return qP;
        }

        //DICHIARAZIZONI SISTEMA
        public SAPbouiCOM.Application oApp;
        public SAPbouiCOM.IForm oForm;
        public SAPbobsCOM.Company oCompany = Program.oCompany;
        public SAPbobsCOM.Recordset oRecordSet;
        public SAPbobsCOM.Recordset oRecordSetIns;
        public SAPbobsCOM.Recordset oRecordSetDel;
        public SAPbobsCOM.Recordset oRecordSetPerc;
        public SAPbobsCOM.Recordset oRecordSetFt;
        public SAPbobsCOM.Recordset oRecordSetRighe;
        public SAPbobsCOM.Recordset oRecordSetGenerale;
        public SAPbobsCOM.Recordset oRecordSetTotale;
        public SAPbobsCOM.Recordset oRecordSetNumEla;

        //LAYOUT
        private SAPbouiCOM.EditText selectAg;
        private SAPbouiCOM.EditText dtInizio;
        private SAPbouiCOM.EditText dtFine;
        private SAPbouiCOM.Button calcola;
        private SAPbouiCOM.Grid listaMaturande;
        private SAPbouiCOM.Button stampa;
        private SAPbouiCOM.EditText totMat;
        private SAPbouiCOM.OptionBtn OptionTutti;
        private SAPbouiCOM.OptionBtn OptionSingolo;

        //FILTRI
        String queryFiltro = "";
        String filtro_agente = "";
        String filtro_dtIni = "";
        String filtro_dtEnd = "";
        String dtEnd = "";
        String tipoLiquid = "par";
        
    }
}
