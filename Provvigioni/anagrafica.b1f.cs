
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace Provvigioni
{

    [FormAttribute("Provvigioni.NewForm_1_b1f", "anagrafica.b1f")]
    class Anagrafica : UserFormBase
    {
        
        public Anagrafica()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.Declare();
            this.OnCustomInitialize();

        }

        public void Declare()
        {
            this.lblForni = ((SAPbouiCOM.StaticText)(this.GetItem("Item_0").Specific));
            this.txtForni = ((SAPbouiCOM.EditText)(this.GetItem("Item_1").Specific));
            this.txtForni.ChooseFromListAfter += new SAPbouiCOM._IEditTextEvents_ChooseFromListAfterEventHandler(this.EditText0_ChooseFromListAfter);
            this.lblCliente = ((SAPbouiCOM.StaticText)(this.GetItem("Item_2").Specific));
            this.txtCliente = ((SAPbouiCOM.EditText)(this.GetItem("Item_3").Specific));
            this.lblItem = ((SAPbouiCOM.StaticText)(this.GetItem("Item_4").Specific));
            this.txtItem = ((SAPbouiCOM.EditText)(this.GetItem("Item_5").Specific));
            this.lblPerc = ((SAPbouiCOM.StaticText)(this.GetItem("Item_6").Specific));
            this.txtPerc = ((SAPbouiCOM.EditText)(this.GetItem("Item_7").Specific));
            this.lblDec = ((SAPbouiCOM.StaticText)(this.GetItem("Item_8").Specific));
            this.txtDecCheck = ((SAPbouiCOM.CheckBox)(this.GetItem("Item_9").Specific));
            //METTO SELECTED LA DEC DI BASE
            //txtDecSi.Selected = true;

            this.btnOk = ((SAPbouiCOM.Button)(this.GetItem("1").Specific));
            this.btnCancel = ((SAPbouiCOM.Button)(this.GetItem("2").Specific));
            this.lblAddVend = ((SAPbouiCOM.StaticText)(this.GetItem("lblAddVend").Specific));
            this.txtAddVend = ((SAPbouiCOM.EditText)(this.GetItem("txtAddVend").Specific));
            //PREMO OK PER AGGIUNGERE ANAGRAFICA
            btnOk.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(btnOk_ClickBefore);
        }

        private void btnOk_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            oForm = (SAPbouiCOM.Form)oApp.Forms.ActiveForm;
            
            oRecordSet = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
  
            if(oForm.Mode == SAPbouiCOM.BoFormMode.fm_ADD_MODE){
                try
                {
                    int inserisco = 1;
                    int newCode = 0;
                    //FILL DATI DAL FORM
                    String addVendite = this.txtAddVend.Value;
                    String fornitore = this.txtForni.Value;
                    String cliente = this.txtCliente.Value;
                    String articolo = this.txtItem.Value;
                    String percent = this.txtPerc.Value;
                    String decurt = "no";
                    if (txtDecCheck.Checked == true)
                    {
                        decurt = "si";
                    }
                    

                    //RECUPERO E CONVERTO ULTIMO Code
                    String qLastCode = "SELECT " + Program.SQL_limit1 + " * FROM \"@ANAGRAFICA_PROVV\"  ORDER BY \"U_incr\" DESC " + Program.HANA_limit1 + " ";
                    oRecordSet.DoQuery(qLastCode);
                    if (oRecordSet.Fields.Item("U_incr").Value.ToString().Equals(""))
                    {
                        newCode = 1;
                    }else{
                        newCode = Int32.Parse(oRecordSet.Fields.Item("U_incr").Value.ToString());
                        newCode = newCode + 1;
                    }
                    
                    String newCodeDb = newCode.ToString();

                    //CONTROLLI INTEGRITA DATI
                    //addetto vendite
                    String qControlAddVendite = "SELECT * FROM \"OSLP\" WHERE  \"SlpName\" = \'"+addVendite+"\' ";
                    oRecordSet.DoQuery(qControlAddVendite);
                    String returnAddVendite = oRecordSet.Fields.Item("SlpName").Value.ToString();
                    String txtSlpCode = oRecordSet.Fields.Item("SlpCode").Value.ToString();
                    int SlpCode = Int32.Parse(txtSlpCode);
                    String SlpName = oRecordSet.Fields.Item("SlpName").Value.ToString();
                    if (returnAddVendite.Equals(""))
                    {
                        Application.SBO_Application.MessageBox("L'addetto alle vendite inserito non esiste", 1, "Ok", "", "");
                        inserisco = 0;
                    }
                    //fornitore
                    String qControlForni = "SELECT * FROM \"OCRD\" WHERE  \"CardCode\" = \'" + fornitore + "\' OR  \"CardName\" = \'" + fornitore + "\'";
                    oRecordSet.DoQuery(qControlForni);
                    String returnForni = oRecordSet.Fields.Item("CardCode").Value.ToString();
                    if (returnForni.Equals(""))
                    {
                        //Application.SBO_Application.MessageBox("Il fornitore inserito non esiste", 1, "Ok", "", "");
                        //inserisco = 0;
                    }
                    //cliente
                    String qControlCliente = "SELECT * FROM \"OCRD\" WHERE  \"CardCode\" = \'" + cliente + "\' ";
                    oRecordSet.DoQuery(qControlCliente);
                    String returnCliente = oRecordSet.Fields.Item("CardCode").Value.ToString();
                    if (returnAddVendite.Equals(""))
                    {
                        //Application.SBO_Application.MessageBox("Il cliente inserito non esiste", 1, "Ok", "", "");
                        //inserisco = 0;
                    }
                    
                    //Application.SBO_Application.MessageBox(returnAddVendite, 1, "Ok", "", "");

                    //INSERIMENTO
                    if(inserisco == 1){
                        SAPbobsCOM.UserTable oUserTable;
                        oUserTable = oCompany.UserTables.Item("ANAGRAFICA_PROVV");
                        oUserTable.Code = newCodeDb;
                        oUserTable.Name = "provvigione_" + newCodeDb;
                        oUserTable.UserFields.Fields.Item("U_SlpCode").Value = SlpCode;
                        oUserTable.UserFields.Fields.Item("U_SlpName").Value = SlpName;
                        oUserTable.UserFields.Fields.Item("U_CardCodeForni").Value = returnForni;
                        oUserTable.UserFields.Fields.Item("U_CardCodeCliente").Value = returnCliente;
                        oUserTable.UserFields.Fields.Item("U_ItemCode").Value = articolo;
                        oUserTable.UserFields.Fields.Item("U_percentuale").Value = percent;
                        oUserTable.UserFields.Fields.Item("U_decurtazione").Value = decurt;
                        oUserTable.UserFields.Fields.Item("U_incr").Value = newCodeDb;
                        //Application.SBO_Application.MessageBox(newCodeDb, 1, "Ok", "", "");
                        int result = oUserTable.Add();
                        if (result != 0)
                        {
                            Application.SBO_Application.MessageBox(oCompany.GetLastErrorDescription()+" - INSERT", 1, "Ok", "", "");
                        }
                        else
                        {
                            Application.SBO_Application.MessageBox("Provvigione aggiunta correttamente", 1, "Ok", "", "");
                            oForm.Close();
                        }
                    }
                    
                }
                catch (InvalidCastException e)
                {
                } 
                
            }


        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>

        public override void OnInitializeFormEvents()
        {
            this.DataAddAfter += new DataAddAfterHandler(this.Form_DataAddAfter);
            oApp = (SAPbouiCOM.Application)Application.SBO_Application;
            oForm = (SAPbouiCOM.Form)oApp.Forms.ActiveForm;

            
        }

        private void Form_DataAddAfter(ref SAPbouiCOM.BusinessObjectInfo pVal)
        {
            

            Application.SBO_Application.MessageBox("Form_data_After", 1, "Ok", "", "");
        }

        private void OnCustomInitialize()
        {

        }

        
        //SELEZIONE FORNITORI
        private void EditText0_ChooseFromListAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            SAPbouiCOM.ISBOChooseFromListEventArg chflarg = (SAPbouiCOM.ISBOChooseFromListEventArg)pVal;

            SAPbouiCOM.DataTable dt = chflarg.SelectedObjects;

            if (dt != null)
            {
                try
                {
                    this.txtForni.Value = dt.GetValue("CardName", 0).ToString();
                }
                catch (System.Runtime.InteropServices.COMException e)
                {
                    Application.SBO_Application.MessageBox(e.ToString(), 1, "Ok", "", "");
                } 
            }
            else
            {
                if (this.txtForni.Value == "")
                {
                    this.txtForni.Value = "";
                }
            }

        }



        
        public static int ordineClienteArticolo = 1;
        public static int ordineCliente = 2;
        public static int ordineArticolo = 3;
        public static int ordineVenditore = 4;
        SAPbouiCOM.Matrix righe;

        //DICHIARAZIZONI SISTEMA
        public SAPbouiCOM.Application oApp;
        public SAPbouiCOM.Form oForm;
        public SAPbobsCOM.Company oCompany = Program.oCompany;
        public SAPbobsCOM.Recordset oRecordSet;
        //TEMPLATE FORM
        private SAPbouiCOM.Button btnOk;
        private SAPbouiCOM.Button btnCancel;
        private SAPbouiCOM.StaticText lblAddVend;
        private SAPbouiCOM.EditText txtAddVend;
        private SAPbouiCOM.StaticText lblForni;
        private SAPbouiCOM.EditText txtForni;
        private SAPbouiCOM.StaticText lblCliente;
        private SAPbouiCOM.EditText txtCliente;
        private SAPbouiCOM.StaticText lblItem;
        private SAPbouiCOM.EditText txtItem;
        private SAPbouiCOM.StaticText lblPerc;
        private SAPbouiCOM.EditText txtPerc;
        private SAPbouiCOM.StaticText lblDec;
        private SAPbouiCOM.OptionBtn txtDecSi;
        private SAPbouiCOM.OptionBtn txtDecNo;
        private SAPbouiCOM.CheckBox txtDecCheck;

        
    }
}
