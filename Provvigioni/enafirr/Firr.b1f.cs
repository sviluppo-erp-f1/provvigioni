
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace Provvigioni
{

    [FormAttribute("Provvigioni.Firr_b1f", "enafirr/Firr.b1f")]
    class Firr_b1f : UserFormBase
    {
        public Firr_b1f()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }


        private void OnCustomInitialize()
        {
            this.oApp = (SAPbouiCOM.Application)Application.SBO_Application;
            this.oForm = this.UIAPIRawForm;
            this.selectAgente = ((SAPbouiCOM.ComboBox)(this.GetItem("Item_4").Specific));
            this.btnCalcola = ((SAPbouiCOM.Button)(this.GetItem("Item_5").Specific));
            this.selectAnno = ((SAPbouiCOM.ComboBox)(this.GetItem("Item_6").Specific));
            this.btnCalcola.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.Button1_ClickBefore);
            //QUERY
            this.oRecordSet = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.oRecordSetAgList = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.oRecordSetAg = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.oRecordSetFIRR = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.oRecordSetIn = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            //DDL AGENTE
            ddlAgenti();
            //DDL ANNO
            listaAnni();
        }
        //CLICCO CALCOLO
        private void Button1_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            //CONTROLLI INTEGRITA DATI
            //dati obbligatori
            if (this.selectAnno.Value.Equals("Scegli anno..."))
            {
                //dati mancanti
                Application.SBO_Application.MessageBox("Inserire i dati obbligatori", 1, "Ok", "", "");
            }
            else
            {
                
                try
                {
                    //ELABORA DATI
                    elaboraFIRR();

                }
                catch (System.Runtime.InteropServices.COMException e)
                {
                    //Application.SBO_Application.MessageBox(e.ToString(), 1, "Ok", "", "");
                }
                //CHIUDO FORM
                oForm.Close();

                //dati mancanti
                Application.SBO_Application.MessageBox("Elaborazione completata.", 1, "Ok", "", "");
            }

        }


        //ELABORAZIONE
        public void elaboraFIRR()
        {
            this.UIAPIRawForm.Freeze(false);
            filtro_anno = this.selectAnno.Value.ToString();
            String agenteD = this.selectAgente.Value.ToString();
            //FILTRO AGENTE
            if (!agenteD.Equals(agenti_base))
            {
                filtro_agente = agenteD;
            }
            else
            {
                filtro_agente = "%%";
            }
            //CHIAMO LA STORE per il firr
            String qFIRR = "";

            if (Program.HANA_DB == 1)
            {
                qFIRR = "call FO_CALCOLO_FIRR ('" + filtro_agente + "','" + filtro_anno + "','N');";
            }
            if (Program.SQL_DB == 1)
            {
                qFIRR = "exec FO_CALCOLO_FIRR @agente = '" + filtro_agente + "',@anno = '" + filtro_anno + "',@tutti = 'Y';";
            }

            oRecordSetFIRR.DoQuery(qFIRR);
            while (!oRecordSetFIRR.EoF)
            {
                //dataDoc = oRecordSet.Fields.Item("data_ft").Value.ToString();
                //AGGIUNGO OGNI LINEA ELABOARAZIONE (DIVISA PER AGENTE)
                int newCode = 0;
                int incr = 0;

                //RECUPERO E CONVERTO ULTIMO Code

                String qLastCodeB = "SELECT " + Program.SQL_limit1 + " * FROM \"@F1_LIQUIDFIRR\"  ORDER BY \"Code\" DESC " + Program.HANA_limit1 + " ";
                oRecordSetIn.DoQuery(qLastCodeB);
                if (oRecordSetIn.Fields.Item("Code").Value.ToString().Equals(""))
                {
                    newCode = 1000001;
                    incr = 1;
                }
                else
                {
                    newCode = Int32.Parse(oRecordSetIn.Fields.Item("Code").Value.ToString());
                    newCode = newCode + 1;
                }
                String newCodeDb = newCode.ToString();
                String slpCode = oRecordSetFIRR.Fields.Item("U_SlpCode").Value.ToString();
                String oggi = DateTime.Now.ToString("yyyyMMdd");
                String anno = oRecordSetFIRR.Fields.Item("U_annoEla").Value.ToString();
                String importoFIRR = oRecordSetFIRR.Fields.Item("firr dovuto").Value.ToString().Replace(",", ".");
                //SLPCODE
                String qAg = "SELECT \"SlpName\" FROM \"OSLP\"  WHERE \"SlpCode\" = '" + slpCode + "' ";
                oRecordSetAg.DoQuery(qAg);

                
                /*SAPbobsCOM.UserTable oUserTableANAG;
                oUserTableANAG = oCompany.UserTables.Item("TEST_F1_LIQUIDFIRR");
                oUserTableANAG.Code = newCode.ToString();
                oUserTableANAG.Name = "elaborazione_" + slpCode + "_" + anno + "_" + newCode;
                oUserTableANAG.UserFields.Fields.Item("U_SlpCode").Value = slpCode;
                oUserTableANAG.UserFields.Fields.Item("U_SlpName").Value = oRecordSetAg.Fields.Item("SlpName").Value.ToString();
                oUserTableANAG.UserFields.Fields.Item("U_ANNO").Value = anno;
                oUserTableANAG.UserFields.Fields.Item("U_DATALIQUID").Value = oggi;
                oUserTableANAG.UserFields.Fields.Item("U_IMPORTOFIRR").Value = importoFIRR;
                int result = oUserTableANAG.Add();*/
                String nweCodeDB = newCode.ToString();
                String elaborazioneDB = "elaborazione_" + slpCode + "_" + anno + "_" + newCode; 
                String SlpNameDB = oRecordSetAg.Fields.Item("SlpName").Value.ToString();
                String insFIRR = "INSERT INTO \"@F1_LIQUIDFIRR\" (\"Code\",\"Name\",\"U_SlpCode\",\"U_SlpName\",\"U_ANNO\",\"U_DATALIQUID\",\"U_IMPORTOFIRR\") VALUES ('" + nweCodeDB + "','" + elaborazioneDB + "','" + slpCode + "','" + SlpNameDB + "','" + anno + "','" + oggi + "','" + importoFIRR + "') ";
                oRecordSetAg.DoQuery(insFIRR);

                oRecordSetFIRR.MoveNext();
            }
            this.UIAPIRawForm.Freeze(false);
        }

        //DDL AGENTI
        public void ddlAgenti()
        {
            selectAgente.ValidValues.Add(agenti_base, agenti_base);
            String qAgenti = "SELECT * FROM \"OSLP\" WHERE \"U_CodEnasarco\" is not null and \"U_DtFineRapporto\" is not null AND \"Active\" = 'Y' order by \"SlpName\" ASC ";
            oRecordSetAgList.DoQuery(qAgenti);
            while (!oRecordSetAgList.EoF)
            {
                selectAgente.ValidValues.Add(oRecordSetAgList.Fields.Item("SlpCode").Value.ToString(), oRecordSetAgList.Fields.Item("SlpName").Value.ToString());
                oRecordSetAgList.MoveNext();
            }

            selectAgente.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
            selectAgente.Select(agenti_base, SAPbouiCOM.BoSearchKey.psk_ByValue);
        }
        //DDL ANNI
        public void listaAnni()
        {
            String questo_anno = DateTime.Now.Year.ToString();
            selectAnno.ValidValues.Add("Scegli anno...", "Scegli anno...");
            for (int c = 0; c < 5; c++)
            {
                int add_anno = int.Parse(questo_anno) - c;
                selectAnno.ValidValues.Add(add_anno.ToString(), add_anno.ToString());
            }

            selectAnno.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
            selectAnno.Select("Scegli anno...", SAPbouiCOM.BoSearchKey.psk_ByValue);
        }

        private SAPbouiCOM.ComboBox selectAgente;
        private SAPbouiCOM.Button btnCalcola;
        private SAPbouiCOM.ComboBox selectAnno;

        //FILTRI
        String filtro_anno;
        String filtro_agente;
        String agenti_base = "Tutti gli Agenti";

        //DICHIARAZIZONI SISTEMA
        public SAPbouiCOM.Application oApp;
        public SAPbouiCOM.IForm oForm;
        public SAPbobsCOM.Company oCompany = (SAPbobsCOM.Company)Application.SBO_Application.Company.GetDICompany();
        public SAPbobsCOM.Recordset oRecordSet;
        public SAPbobsCOM.Recordset oRecordSetFIRR;
        public SAPbobsCOM.Recordset oRecordSetAgList;
        public SAPbobsCOM.Recordset oRecordSetAg;
        public SAPbobsCOM.Recordset oRecordSetIn;

        //PARAMETRI MODULO
        public string limitSql;
        public string limitHana;
    }
}
