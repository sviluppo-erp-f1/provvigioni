
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace Provvigioni
{

    [FormAttribute("Provvigioni.paramEnasarco_b1f", "enafirr/paramEnasarco.b1f")]
    class paramEnasarco_b1f : UserFormBase
    {
        public paramEnasarco_b1f()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {

            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }

        private void OnCustomInitialize()
        {
            this.oApp = (SAPbouiCOM.Application)Application.SBO_Application;
            this.oForm = this.UIAPIRawForm;
            this.oRecordSet = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.oRecordSetIns = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));

            this.percentEna = ((SAPbouiCOM.EditText)(this.GetItem("Item_4").Specific));
            this.percentAzi = ((SAPbouiCOM.EditText)(this.GetItem("Item_14").Specific));
            this.ddlAnni = ((SAPbouiCOM.ComboBox)(this.GetItem("Item_5").Specific));
            this.ddlRapporto = ((SAPbouiCOM.ComboBox)(this.GetItem("Item_16").Specific));
            this.minimale = ((SAPbouiCOM.EditText)(this.GetItem("Item_12").Specific));
            this.massimale = ((SAPbouiCOM.EditText)(this.GetItem("Item_6").Specific));
            this.monoPluri = ((SAPbouiCOM.ComboBox)(this.GetItem("Item_7").Specific));
            this.btnAdd = ((SAPbouiCOM.Button)(this.GetItem("Item_8").Specific));
            this.gridEna = ((SAPbouiCOM.Grid)(this.GetItem("Item_9").Specific));
            this.btnDel = ((SAPbouiCOM.Button)(this.GetItem("Item_10").Specific));
            this.btnDel.PressedAfter += new SAPbouiCOM._IButtonEvents_PressedAfterEventHandler(this.btnElimina_PressedAfter);
            this.btnDel.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.btnElimina_ClickBefore);
            this.btnDel.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button0_ClickAfter);

            //PREMO OK PER AGGIUNGERE REGOLA
            this.btnAdd.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.btnOk_ClickBefore);

            //assegno gli anni
            creaDdlAnni();

            //ddl manadatari
            ddlMandatari();

            //ddl rapporto
            ddlRap();

            elementiForm();

        }

        //INSERISCO ENASARCO
        private void btnOk_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            oForm = (SAPbouiCOM.Form)oApp.Forms.ActiveForm;
            oRecordSet = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));

            int inserisco = 1;
            int newCode = 0;
            int newCodeANAG = 0;
            if (oForm.Mode == SAPbouiCOM.BoFormMode.fm_ADD_MODE)
            {
                try
                {
                    //CONTROLLI INTEGRITA DATI
                    int controllo_soc = 0;
                    //controllo se perc sono numeriche
                    String perc = this.percentEna.Value;
                    float controllo_perc;
                    bool isNumericFirr = float.TryParse(perc, out controllo_perc);

                    String percMin = this.minimale.Value;
                    float controllo_percMin;
                    bool isNumericMin = float.TryParse(percMin, out controllo_percMin);

                    String percMax = this.massimale.Value;
                    float controllo_percMax;
                    bool isNumericMax = float.TryParse(percMax, out controllo_percMax);

             

                    //dati obbligatori
                    if ( this.monoPluri.Value.Equals("") || this.monoPluri.Value.Equals(manda_base) || this.ddlRapporto.Value.Equals(rapporto_base)  || this.ddlAnni.Value.Equals("") || this.ddlAnni.Value.Equals(anno_base) || isNumericMin == false || isNumericMax == false || isNumericFirr == false)
                    {
                        //dati mancanti
                        Application.SBO_Application.MessageBox("Inserire i dati obbligatori e controllare che la percentuale sia numerica", 1, "Ok", "", "");
                    }
                    else
                    {
                        String percentEnaDB = this.percentEna.Value;
                        String percentEnaAziDB = this.percentAzi.Value;
                        String annoDB = this.ddlAnni.Value;
                        String minimaleDB = this.minimale.Value;
                        String massimaleDB = this.massimale.Value;
                        String monoPluriDB = this.monoPluri.Value;
                        String rapportoDB = this.ddlRapporto.Value;
                      

                        //INSERIMENTO
                        if (inserisco == 1)
                        {
                            //INSERISCO TESTATA ANAG.
                            //RECUPERO E CONVERTO ULTIMO Code anag
                            String qLastCodeA = "SELECT " + Program.SQL_limit1 + " * FROM \"@F1_ENA1\"  ORDER BY CAST(\"Code\" as int) DESC " + Program.HANA_limit1 + " ";
                            oRecordSet.DoQuery(qLastCodeA);
                            if (oRecordSet.Fields.Item("Code").Value.ToString().Equals(""))
                            {
                                newCodeANAG = 1;
                            }
                            else
                            {
                                newCodeANAG = Int32.Parse(oRecordSet.Fields.Item("Code").Value.ToString());
                                newCodeANAG = newCodeANAG + 1;
                            }


                            try
                            {
                                String tipoMono_PluriDB = "";
                                if (monoPluriDB.Equals("Mono"))
                                {
                                    tipoMono_PluriDB = "1";
                                }
                                else
                                {
                                    tipoMono_PluriDB = "2";
                                }
                                
                                String tipoRapporto_DB = "";

                                switch (rapportoDB)
                                {
                                    case "Agente":
                                        tipoRapporto_DB = "1";
                                        break;
                                    case "SocPers":
                                        tipoRapporto_DB = "2";
                                        break;
                                    case "SocCap":
                                        tipoRapporto_DB = "3";
                                        break;
                                    default:
                                        break;
                                }

                                //PARSO le virgole
                                minimaleDB = minimaleDB.Replace(",", ".");
                                massimaleDB = massimaleDB.Replace(",", ".");
                                percentEnaDB = percentEnaDB.Replace(",", ".");
                                percentEnaAziDB = percentEnaAziDB.Replace(",", ".");

                                //INSERISCO
                                String q_ins = " INSERT INTO \"@F1_ENA1\" (\"Code\",\"Name\",\"U_ANNO\",\"U_TIPO\",\"U_CONTR_MIN\",\"U_PROVVMAX\",\"U_ALIQUOTA_AGE\",\"U_ALIQUOTA_AZ\",\"U_TIPOSOC\") VALUES ('" + newCodeANAG + "','" + newCodeANAG + "','" + annoDB + "','" + tipoMono_PluriDB + "','" + minimaleDB + "','" + massimaleDB + "','" + percentEnaDB + "','" + percentEnaAziDB + "','" + tipoRapporto_DB + "') ";
                                oRecordSetIns.DoQuery(q_ins);
                                Application.SBO_Application.MessageBox("Regola inserita correttamente", 1, "Ok", "", "");
                                //EditText0.Value = "";

                                //Carico elementi griglia
                                aggiornaGriglia();

                            }
                            catch (System.Runtime.InteropServices.COMException e)
                            {
                                Application.SBO_Application.MessageBox("Errore nell'inserimento, riavviare i servizi e riprovare.", 1, "Ok", "", "");
                            }


                        }

                    }
                }
                catch (InvalidCastException e)
                {
                    Application.SBO_Application.MessageBox("Non è stato possibile collegare l'agente alla provvigione", 1, "Ok", "", "");
                }

            }
        }

        //BOTTONE ELIMINA
        private void btnElimina_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            SAPbouiCOM.DataTable riga = gridEna.DataTable;

            this.UIAPIRawForm.Freeze(true);

            int numRows = gridEna.Rows.Count;

            string[] righeIDS = new string[numRows];

            for (int i = 0; i < numRows; i++)
            {
                String idRiga = gridEna.DataTable.GetValue("ID", i).ToString();
                String select = gridEna.DataTable.GetValue("Elimina", i).ToString();
                if (select.Equals("Y"))
                {
                    String qDel = "DELETE FROM \"@F1_ENA1\" WHERE \"Code\" = \'" + idRiga + "\' ";
                    oRecordSet.DoQuery(qDel);
                }

            }
            this.UIAPIRawForm.Freeze(false);

        }

        private void Button0_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            aggiornaGriglia();

        }

        private void btnElimina_PressedAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {

            aggiornaGriglia();

        }

        //AGGIORNA GRID
        private void aggiornaGriglia()
        {
            this.oForm = (SAPbouiCOM.IForm)oApp.Forms.ActiveForm;
            this.UIAPIRawForm.Freeze(true);

            queryFiltro = " SELECT ";
            queryFiltro += " f1firr.\"Code\" as \"ID\", ";
            queryFiltro += " f1firr.\"U_ANNO\" as \"Anno\", ";
            queryFiltro += " CASE WHEN f1firr.\"U_TIPO\" = '1' THEN 'Mono'  ";
            queryFiltro += " WHEN f1firr.\"U_TIPO\" <> '1' THEN 'Pluri'  ";
            queryFiltro += " END as \"Tipo\", ";
            queryFiltro += " CASE WHEN f1firr.\"U_TIPOSOC\" = '1' THEN 'Agente'  ";
            queryFiltro += " WHEN f1firr.\"U_TIPOSOC\" = '2' THEN 'Soc. Persone'  ";
            queryFiltro += " WHEN f1firr.\"U_TIPOSOC\" = '3' THEN 'Soc. Capitali'  ";
            queryFiltro += " END as \"Rapporto\",  ";
            queryFiltro += " f1firr.\"U_CONTR_MIN\" as \"Soglia Min.\", ";
            queryFiltro += " f1firr.\"U_PROVVMAX\" as \"Soglia Max.\", ";
            queryFiltro += " f1firr.\"U_ALIQUOTA_AGE\" as \"Aliquota Age.\", ";
            queryFiltro += " f1firr.\"U_ALIQUOTA_AZ\" as \"Aliquota Az.\", ";
            queryFiltro += " 'N' as \"Elimina\" ";
            queryFiltro += " FROM ";
            queryFiltro += " \"@F1_ENA1\" f1firr  ";
            queryFiltro += " WHERE ";
            queryFiltro += " f1firr.\"Code\">0 ";
            queryFiltro += " ORDER BY f1firr.\"U_ANNO\" DESC ";

            //AGGIUNGO ID LISTA AL FORM
            try
            {
                oForm.DataSources.DataTables.Add("listaFilEnaX");

            }
            catch (System.Runtime.InteropServices.COMException e)
            {
                //Application.SBO_Application.MessageBox(e.ToString(), 1, "Ok", "", "");
            }


            oForm.DataSources.DataTables.Item("listaFilEnaX").ExecuteQuery(queryFiltro);
            gridEna.DataTable = oForm.DataSources.DataTables.Item("listaFilEnaX");
            //assegno elemtni grid
            elementiForm();

            //RESIZE
            gridEna.AutoResizeColumns();
            this.UIAPIRawForm.Freeze(false);
        }

        //ASSEGNA ANNO
        private void creaDdlAnni()
        {
            ddlAnni.ValidValues.Add(anno_base, anno_base);
            int anno = int.Parse(DateTime.Now.ToString("yyyy").ToString());
            int anno_da = anno + 1;

            for (int c = 0; c < 3; c++)
            {
                ddlAnni.ValidValues.Add(anno_da.ToString(), anno_da.ToString());

                anno_da = anno_da - 1;
            }

            ddlAnni.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
            ddlAnni.Select(anno_base, SAPbouiCOM.BoSearchKey.psk_ByValue);
        }

        //ASSEGNA MANDATARI
        private void ddlMandatari()
        {
            monoPluri.ValidValues.Add(manda_base, manda_base);
            monoPluri.ValidValues.Add("Mono", "Mono");
            monoPluri.ValidValues.Add("Pluri", "Pluri");

            monoPluri.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
            monoPluri.Select(manda_base, SAPbouiCOM.BoSearchKey.psk_ByValue);
        }

        //ASSEGNA RAPPORTO
        private void ddlRap()
        {
            ddlRapporto.ValidValues.Add(rapporto_base, rapporto_base);
            ddlRapporto.ValidValues.Add("Agente", "Agente");
            ddlRapporto.ValidValues.Add("SocPers", "Società di Persone");
            ddlRapporto.ValidValues.Add("SocCap", "Società di Capitali");

            ddlRapporto.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
            ddlRapporto.Select(rapporto_base, SAPbouiCOM.BoSearchKey.psk_ByValue);
        }

        //COLONNE GRID
        private void elementiForm()
        {
            //DISABLE COLONNE
            gridEna.Columns.Item(0).Editable = false;
            gridEna.Columns.Item(1).Editable = false;
            gridEna.Columns.Item(2).Editable = false;
            gridEna.Columns.Item(3).Editable = false;
            gridEna.Columns.Item(4).Editable = false;
            gridEna.Columns.Item(5).Editable = false;
            gridEna.Columns.Item(6).Editable = false;
            gridEna.Columns.Item(7).Editable = false;

            //COLONNE CUSTOM
            gridEna.Columns.Item(8).TitleObject.Sortable = true;
            gridEna.Columns.Item(8).Type = SAPbouiCOM.BoGridColumnType.gct_CheckBox;
        }

        //DICHIARAZIZONI SISTEMA
        public SAPbouiCOM.Application oApp;
        public SAPbouiCOM.IForm oForm;
        public SAPbobsCOM.Company oCompany = Program.oCompany;
        public SAPbobsCOM.Recordset oRecordSet;
        public SAPbobsCOM.Recordset oRecordSetIns;

        //TEMPLATE FORM
        private SAPbouiCOM.EditText percentEna;
        private SAPbouiCOM.EditText percentAzi;
        private SAPbouiCOM.ComboBox ddlAnni;
        private SAPbouiCOM.ComboBox ddlRapporto;
        private SAPbouiCOM.EditText minimale;
        private SAPbouiCOM.EditText massimale;
        private SAPbouiCOM.ComboBox monoPluri;
        private SAPbouiCOM.Button btnAdd;
        private SAPbouiCOM.Grid gridEna;
        private SAPbouiCOM.Button btnDel;

        //FILTRI
        String queryFiltro = "";
        String anno_base = "Scegli un anno...";
        String manda_base = "Scegli un mandatario...";
        String rapporto_base = "Scegli rapporto...";
    }
}
