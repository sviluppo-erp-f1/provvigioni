
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace Provvigioni
{

    [FormAttribute("Provvigioni.Enasarco_Liquidato_b1f", "enafirr/Enasarco Liquidato.b1f")]
    class Enasarco_Liquidato_b1f : UserFormBase
    {
        public Enasarco_Liquidato_b1f()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {

            this.OnCustomInitialize(); 
            this.elementiForm();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }

        

        private void OnCustomInitialize()
        {
            this.oApp = (SAPbouiCOM.Application)Application.SBO_Application;
            this.oForm = this.UIAPIRawForm;
            this.listaENA = ((SAPbouiCOM.Grid)(this.GetItem("Item_0").Specific));
            this.bntAnnulla = ((SAPbouiCOM.Button)(this.GetItem("Item_1").Specific));
            this.oRecordSet = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));

            this.bntAnnulla.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.Button0_ClickBefore);
        }

        //ANNULLA FIRR
        private void Button0_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            this.oForm = ((SAPbouiCOM.IForm)(this.oApp.Forms.ActiveForm));
            try
            {
                oForm.DataSources.DataTables.Add("filtroDettaglioENASARCOFineX");
            }
            catch (System.Runtime.InteropServices.COMException e)
            {
                //Application.SBO_Application.MessageBox(e.ToString(), 1, "Ok", "", "");
            }

            this.UIAPIRawForm.Freeze(true);

            //SETTO VALORE INIZIALE UPDATE COSTO
            int numRows = listaENA.Rows.Count;

            string[] righeIDS = new string[numRows];

            for (int i = 0; i < numRows; i++)
            {


                //elimino
                String slpcode = listaENA.DataTable.GetValue("U_SLPCODE", i).ToString();
                String agente = listaENA.DataTable.GetValue("Agente", i).ToString();
                String anno = listaENA.DataTable.GetValue("Anno", i).ToString();
                String trimestre = listaENA.DataTable.GetValue("Trimestre", i).ToString();
                String select = listaENA.DataTable.GetValue("Selezione", i).ToString();
                try
                {
                    if (select.Equals("Y"))
                    {
                        String q = "DELETE FROM \"@ENASARCO_STOR\" WHERE  \"U_SLPCODE\" = \'" + slpcode + "\' AND  \"U_ANNO\" = \'" + anno + "\'  AND  \"U_TRIMESTRE\" = \'" + trimestre + "\' ";
                        oRecordSet.DoQuery(q);
                    }

                }
                catch (System.Runtime.InteropServices.COMException e)
                {
                    //Application.SBO_Application.MessageBox(e.ToString(), 1, "Ok", "", "");
                }



                //this.Grid3.DataTable.Clear();
            }

            String qDX = "SELECT 'X' as \"Selezione\",ena.\"U_SLPCODE\",T1.\"SlpName\" as \"Agente\",ena.\"U_ANNO\" as \"Anno\",ena.\"U_TRIMESTRE\" as \"Trimestre\",ena.\"U_TIPO\" as \"Tipo\",ena.\"U_PROVVIGIONE\" as \"Provvigione\",ena.\"U_CONTRIB_AGE\" as \"Contrib.\",ena.\"U_PROVVIGIONE_PAGATA\" as \"Provv. Pagata\" FROM \"@ENASARCO_STOR\" ena INNER JOIN \"OSLP\" T1 ON T1.\"SlpCode\" = ena.\"U_SLPCODE\" LEFT OUTER JOIN \"@F1_AGESOC\" soc on ( ena.\"U_SLPCODE\"=soc.\"U_slpcode\" and ena.\"U_TRIMESTRE\"=soc.\"U_trim\")  ";


            oForm.DataSources.DataTables.Item("filtroDettaglioENASARCOFineX").ExecuteQuery(qDX);

            listaENA.DataTable = oForm.DataSources.DataTables.Item("filtroDettaglioENASARCOFineX");
            elementiForm();
            this.UIAPIRawForm.Freeze(false);

        }

        //COLONNE GRID
        private void elementiForm()
        {
            //DISABLE COLONNE
            listaENA.Columns.Item(0).Type = SAPbouiCOM.BoGridColumnType.gct_CheckBox;
            listaENA.Columns.Item(1).Visible = false;
            listaENA.Columns.Item(2).Editable = false;
            listaENA.Columns.Item(3).Editable = false;
            listaENA.Columns.Item(4).Editable = false;
            listaENA.Columns.Item(5).Editable = false;
            listaENA.Columns.Item(6).Editable = false;
            listaENA.Columns.Item(7).Editable = false;
            listaENA.Columns.Item(8).Editable = false;

            listaENA.AutoResizeColumns();


        }

        private SAPbouiCOM.Grid listaENA;
        private SAPbouiCOM.Button bntAnnulla;

        //PARAMETRI MODULO
        public string limitSql;
        public string limitHana;

        //DICHIARAZIZONI SISTEMA
        public SAPbouiCOM.Application oApp;
        public SAPbouiCOM.IForm oForm;
        public SAPbobsCOM.Company oCompany = (SAPbobsCOM.Company)Application.SBO_Application.Company.GetDICompany();
        public SAPbobsCOM.Recordset oRecordSet;
    }
}
