
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace Provvigioni
{

    [FormAttribute("Provvigioni.Enasarco_b1f", "enafirr/Enasarco.b1f")]
    class Enasarco_b1f : UserFormBase
    {
        public Enasarco_b1f()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }


        private void OnCustomInitialize()
        {
            this.oApp = (SAPbouiCOM.Application)Application.SBO_Application;
            this.oForm = this.UIAPIRawForm;
            this.selectAnno = ((SAPbouiCOM.ComboBox)(this.GetItem("Item_3").Specific));
            this.selectTrimestre = ((SAPbouiCOM.ComboBox)(this.GetItem("Item_4").Specific));
            this.selectAgente = ((SAPbouiCOM.ComboBox)(this.GetItem("Item_5").Specific));
            this.btnCalcola = ((SAPbouiCOM.Button)(this.GetItem("Item_6").Specific));

            this.btnCalcola.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.Button1_ClickBefore);
            //QUERY
            this.oRecordSet = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.oRecordSetAgList = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.oRecordSetAg = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.oRecordSetFIRR = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.oRecordSetIn = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.oRecordSetLISTA = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            //DDL AGENTE
            ddlAgenti();
            //DDL ANNO
            listaAnni();
            //DDL TRIMESTRI
            listaTrimestri();
        }
        //CLICCO CALCOLO
        private void Button1_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            //CONTROLLI INTEGRITA DATI
            //dati obbligatori
            if (this.selectAnno.Value.Equals("Scegli anno...") || this.selectTrimestre.Value.Equals("Scegli trimestre..."))
            {
                //dati mancanti
                Application.SBO_Application.MessageBox("Inserire i dati obbligatori", 1, "Ok", "", "");
            }
            else
            {
                try
                {
                    //ELABORA DATI
                    elaboraENASARCO();

                }
                catch (System.Runtime.InteropServices.COMException e)
                {
                    //Application.SBO_Application.MessageBox(e.ToString(), 1, "Ok", "", "");
                }
                

                //CHIUDO FORM
                oForm.Close();

                //dati mancanti
                Application.SBO_Application.MessageBox("Elaborazione completata.", 1, "Ok", "", "");
            }

        }
        //ELABORAZIONE
        public void elaboraENASARCO()
        {
            this.UIAPIRawForm.Freeze(false);

            filtro_anno = this.selectAnno.Value.ToString();
            filtro_trimestre = this.selectTrimestre.Value.ToString();
            String agenteD = this.selectAgente.Value.ToString();

            //FILTRO AGENTE
            //Sulla base del filtro agente lancio la store su un solo agente o ciclo per tutti
            String qENA = "";

            if (!agenteD.Equals(agenti_base))
            {
                filtro_agente = agenteD;

                if (Program.HANA_DB == 1)
                {
                    qENA = "call FO_CALCOLO_ENASARCO ('" + filtro_agente + "','" + filtro_anno + "', '" + filtro_trimestre + "','N');";
                }
                if (Program.SQL_DB == 1)
                {
                    qENA = "exec FO_CALCOLO_ENASARCO @slpcode = '" + filtro_agente + "',@anno = '" + filtro_anno + "',@trimestre = '" + filtro_trimestre + "',@tutti = 'N';";
                }
                
                
            }
            else
            {
                filtro_agente = "%%";

                if (Program.HANA_DB == 1)
                {
                    qENA = "call FO_CALCOLO_ENASARCO ('" + filtro_agente + "','" + filtro_anno + "', '" + filtro_trimestre + "','Y');";
                }
                if (Program.SQL_DB == 1)
                {
                    qENA = "exec FO_CALCOLO_ENASARCO @slpcode = '" + filtro_agente + "',@anno = '" + filtro_anno + "',@trimestre = '" + filtro_trimestre + "',@tutti = 'Y';";
                }
            }

            oRecordSetFIRR.DoQuery(qENA);

            this.UIAPIRawForm.Freeze(false);
        }
        
        //DDL AGENTI
        public void ddlAgenti()
        {
            selectAgente.ValidValues.Add(agenti_base, agenti_base);
            String qAgenti = "SELECT * FROM \"OSLP\" WHERE \"U_CodEnasarco\" is not null and \"U_DtFineRapporto\" is not null AND\"Active\" = 'Y' order by \"SlpName\" ASC ";
            oRecordSetAgList.DoQuery(qAgenti);
            while (!oRecordSetAgList.EoF)
            {
                selectAgente.ValidValues.Add(oRecordSetAgList.Fields.Item("SlpCode").Value.ToString(), oRecordSetAgList.Fields.Item("SlpName").Value.ToString());
                oRecordSetAgList.MoveNext();
            }

            selectAgente.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
            selectAgente.Select(agenti_base, SAPbouiCOM.BoSearchKey.psk_ByValue);
        }
        //DDL ANNI
        public void listaAnni()
        {
            String questo_anno = DateTime.Now.Year.ToString();
            selectAnno.ValidValues.Add("Scegli anno...", "Scegli anno...");
            for (int c = 0; c < 5; c++)
            {
                int add_anno = int.Parse(questo_anno) - c;
                selectAnno.ValidValues.Add(add_anno.ToString(), add_anno.ToString());
            }

            selectAnno.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
            selectAnno.Select("Scegli anno...", SAPbouiCOM.BoSearchKey.psk_ByValue);
        }
        //DDL TRIMESTRI
        public void listaTrimestri()
        {
            selectTrimestre.ValidValues.Add("Scegli trimestre...", "Scegli trimestre...");
            selectTrimestre.ValidValues.Add("1", "1");
            selectTrimestre.ValidValues.Add("2", "2");
            selectTrimestre.ValidValues.Add("3", "3");
            selectTrimestre.ValidValues.Add("4", "4");

            selectTrimestre.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
            selectTrimestre.Select("Scegli trimestre...", SAPbouiCOM.BoSearchKey.psk_ByValue);
        }
        private SAPbouiCOM.ComboBox selectAnno;
        private SAPbouiCOM.ComboBox selectTrimestre;
        private SAPbouiCOM.ComboBox selectAgente;
        private SAPbouiCOM.Button btnCalcola;

        //FILTRI
        String filtro_anno;
        String filtro_agente;
        String filtro_trimestre;
        String agenti_base = "Tutti gli Agenti";

        //DICHIARAZIZONI SISTEMA
        public SAPbouiCOM.Application oApp;
        public SAPbouiCOM.IForm oForm;
        public SAPbobsCOM.Company oCompany = (SAPbobsCOM.Company)Application.SBO_Application.Company.GetDICompany();
        public SAPbobsCOM.Recordset oRecordSet;
        public SAPbobsCOM.Recordset oRecordSetFIRR;
        public SAPbobsCOM.Recordset oRecordSetAgList;
        public SAPbobsCOM.Recordset oRecordSetAg;
        public SAPbobsCOM.Recordset oRecordSetIn;
        public SAPbobsCOM.Recordset oRecordSetLISTA;

        //PARAMETRI MODULO
        public string limitSql;
        public string limitHana;
    }
}
