
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace Provvigioni
{

    [FormAttribute("Provvigioni.tracciatoFirr_b1f", "enasarcoFirr/tracciatoFirr.b1f")]
    class tracciatoFirr_b1f : UserFormBase
    {
        public tracciatoFirr_b1f()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }


        private void OnCustomInitialize()
        {
            this.oApp = (SAPbouiCOM.Application)Application.SBO_Application;
            this.oForm = this.UIAPIRawForm;
            this.ddlAnni = ((SAPbouiCOM.ComboBox)(this.GetItem("Item_1").Specific));
            this.generaFile = ((SAPbouiCOM.Button)(this.GetItem("Item_2").Specific));
            this.generaFile.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.Button1_ClickBefore);

            this.oRecordSet = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.oRecordSetFile = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.oRecordSetAzi = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));

            //DDL ANNO
            listaAnni();
        }
        //GENERA FILE
        private void Button1_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            //CONTROLLI INTEGRITA DATI
            //dati obbligatori
            if (this.ddlAnni.Value.Equals("Scegli anno..."))
            {
                //dati mancanti
                Application.SBO_Application.MessageBox("Inserire i dati obbligatori", 1, "Ok", "", "");
            }
            else
            {
                try
                {
                    //ELABORA DATI
                    elaboraFileFIRR();

                }
                catch (System.Runtime.InteropServices.COMException e)
                {
                    //Application.SBO_Application.MessageBox(e.ToString(), 1, "Ok", "", "");
                }
                //CHIUDO FORM
                oForm.Close();

                //dati mancanti
                Application.SBO_Application.MessageBox("Elaborazione completata.", 1, "Ok", "", "");
            }

        }

        //ELABORAZIONE
        public void elaboraFileFIRR()
        {
            this.UIAPIRawForm.Freeze(false);
            filtro_anno = this.ddlAnni.Value.ToString();
            //RECUPERO DATI AZIENDA
            String qDatiAzi = "SELECT * FROM \"@F1_PARAZI\"";
            oRecordSetAzi.DoQuery(qDatiAzi);
            String ragione_sociale = oRecordSetAzi.Fields.Item("U_RagioneSociale").Value.ToString();
            String partita_iva = oRecordSetAzi.Fields.Item("U_PartitaIva").Value.ToString();
            String posizione_ditta = oRecordSetAzi.Fields.Item("U_PosizioneDitta").Value.ToString();
            String protocollo = oRecordSetAzi.Fields.Item("U_Protocollo").Value.ToString();

            String qFIRR = "exec \"@F1_TRACCIATO_FIRR\" @anno = '" + filtro_anno + "',@ragione_sociale = '" + ragione_sociale + "',@partita_iva = '" + partita_iva + "',@posizione_ditta = '" + posizione_ditta + "',@protocollo = '" + protocollo + "';";
            oRecordSet.DoQuery(qFIRR);

            //GENERO IL FILE
            //finita l'elaborazione della store su DB scrivo tutto su un file
            generaFileDB();

            this.UIAPIRawForm.Freeze(false);
        }
        //GENERAZIONE FILE
        public void generaFileDB()
        {

            string fileName = @"C:\Firr_" + ddlAnni.Value.ToString() + ".txt";

            try
            {
                // Check if file already exists. If yes, delete it.     
                if (File.Exists(fileName))
                {
                    File.Delete(fileName);
                }

                // Create a new file     
                using (FileStream fs = File.Create(fileName))
                {
                    //CICLO LE LINEE SUL DB da scrivere nel file
                    String qData = "SELECT * FROM \"@F1_FILEENA\" ";
                    oRecordSetFile.DoQuery(qData);
                    while (!oRecordSetFile.EoF)
                    {

                        Byte[] stringaDB = new UTF8Encoding(true).GetBytes(oRecordSetFile.Fields.Item("U_tracciato").Value.ToString());
                        fs.Write(stringaDB, 0, stringaDB.Length);

                        oRecordSetFile.MoveNext();
                    }
                }
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.ToString());
            }
        }
        //DDL ANNI
        public void listaAnni()
        {
            String questo_anno = DateTime.Now.Year.ToString();
            ddlAnni.ValidValues.Add("Scegli anno...", "Scegli anno...");
            for (int c = 0; c < 5; c++)
            {
                int add_anno = int.Parse(questo_anno) - c;
                ddlAnni.ValidValues.Add(add_anno.ToString(), add_anno.ToString());
            }

            ddlAnni.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
            ddlAnni.Select("Scegli anno...", SAPbouiCOM.BoSearchKey.psk_ByValue);
        }

        private SAPbouiCOM.ComboBox ddlAnni;
        private SAPbouiCOM.Button generaFile;

        //FILTRI
        String filtro_anno;

        //DICHIARAZIZONI SISTEMA
        public SAPbouiCOM.Application oApp;
        public SAPbouiCOM.IForm oForm;
        public SAPbobsCOM.Company oCompany = (SAPbobsCOM.Company)Application.SBO_Application.Company.GetDICompany();
        public SAPbobsCOM.Recordset oRecordSet;
        public SAPbobsCOM.Recordset oRecordSetFile;
        public SAPbobsCOM.Recordset oRecordSetAzi;

        //PARAMETRI MODULO
        public string limitSql;
        public string limitHana;

        
    }
}
