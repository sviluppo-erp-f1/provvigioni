
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace Provvigioni
{

    [FormAttribute("Provvigioni.firrLiquidato_b1f", "enafirr/firrLiquidato.b1f")]
    class firrLiquidato_b1f : UserFormBase
    {
        public firrLiquidato_b1f()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            
            this.OnCustomInitialize();
            
        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }

        private void OnCustomInitialize()
        {
            this.oApp = (SAPbouiCOM.Application)Application.SBO_Application;
            this.oForm = this.UIAPIRawForm;
            this.listaFirr = ((SAPbouiCOM.Grid)(this.GetItem("Item_0").Specific));
            this.bntAnnulla = ((SAPbouiCOM.Button)(this.GetItem("Item_1").Specific));
            this.oRecordSet = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));

            this.bntAnnulla.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.Button0_ClickBefore);

            //aggiornaGriglia();
            this.elementiForm();
           
        }

        //ANNULLA FIRR
        private void Button0_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            this.oForm = ((SAPbouiCOM.IForm)(this.oApp.Forms.ActiveForm));
            try
            {
                oForm.DataSources.DataTables.Add("filtroDettaglioFIRRFine");
            }
            catch (System.Runtime.InteropServices.COMException e)
            {
                //Application.SBO_Application.MessageBox(e.ToString(), 1, "Ok", "", "");
            }

            this.UIAPIRawForm.Freeze(true);

            //SETTO VALORE INIZIALE UPDATE COSTO
            int numRows = listaFirr.Rows.Count;

            string[] righeIDS = new string[numRows];

            for (int i = 0; i < numRows; i++)
            {


                //elimino
                String agente = listaFirr.DataTable.GetValue("Agente", i).ToString();
                String anno = listaFirr.DataTable.GetValue("Anno", i).ToString();
                String select = listaFirr.DataTable.GetValue("Selezione", i).ToString();
                try
                {
                    if (select.Equals("Y"))
                    {
                        String q = "DELETE FROM \"@F1_LIQUIDFIRR\" WHERE  \"U_SlpName\" = \'" + agente + "\' AND  \"U_ANNO\" = \'" + anno + "\' ";
                        oRecordSet.DoQuery(q);
                    }

                }
                catch (System.Runtime.InteropServices.COMException e)
                {
                    //Application.SBO_Application.MessageBox(e.ToString(), 1, "Ok", "", "");
                }



                //this.Grid3.DataTable.Clear();
            }

            String qDX = "select 'X' as \"Selezione\",\"U_SlpName\" as \"Agente\",\"U_ANNO\" as \"Anno\",\"U_DATALIQUID\" as \"Data Liquid.\",sum(\"U_IMPORTOFIRR\") as \"FIRR Dovuto\" FROM \"@F1_LIQUIDFIRR\" GROUP BY \"U_ANNO\",\"U_SlpCode\",\"U_SlpName\",\"U_DATALIQUID\"  ";
            oForm.DataSources.DataTables.Item("filtroDettaglioFIRRFine").ExecuteQuery(qDX);

            listaFirr.DataTable = oForm.DataSources.DataTables.Item("filtroDettaglioFIRRFine");
            elementiForm();
            this.UIAPIRawForm.Freeze(false);

        }
        
        //COLONNE GRID
        private void elementiForm()
        {
            //DISABLE COLONNE
            listaFirr.Columns.Item(0).Type = SAPbouiCOM.BoGridColumnType.gct_CheckBox;
            listaFirr.Columns.Item(1).Editable = false;
            listaFirr.Columns.Item(2).Editable = false;
            listaFirr.Columns.Item(3).Editable = false;
            
        }

        //AGGIORNA GRID
        private void aggiornaGriglia()
        {
            this.oForm = (SAPbouiCOM.IForm)oApp.Forms.ActiveForm;
            this.UIAPIRawForm.Freeze(true);
            String queryFiltro = "select 'X' as \"Selezione\",\"U_SlpName\" as \"Agente\",\"U_ANNO\" as \"Anno\",\"U_DATALIQUID\" as \"Data Liquid.\",sum(\"U_IMPORTOFIRR\") as \"FIRR Dovuto\" FROM \"@F1_LIQUIDFIRR\" GROUP BY \"U_ANNO\",\"U_SlpCode\",\"U_SlpName\",\"U_DATALIQUID\"  ";

            //AGGIUNGO ID LISTA AL FORM
            try
            {
                oForm.DataSources.DataTables.Add("listaFIRRXX");
            }
            catch (System.Runtime.InteropServices.COMException e)
            {
                //Application.SBO_Application.MessageBox(e.ToString(), 1, "Ok", "", "");
            }

            oForm.DataSources.DataTables.Item("listaFIRRXX").ExecuteQuery(queryFiltro);
            listaFirr.DataTable = oForm.DataSources.DataTables.Item("listaFIRRXX");

            //assegno elemtni grid
            elementiForm();

            //RESIZE
            listaFirr.AutoResizeColumns();
            this.UIAPIRawForm.Freeze(false);
        }

        private SAPbouiCOM.Grid listaFirr;
        private SAPbouiCOM.Button bntAnnulla;

        //PARAMETRI MODULO
        public string limitSql;
        public string limitHana;

        //DICHIARAZIZONI SISTEMA
        public SAPbouiCOM.Application oApp;
        public SAPbouiCOM.IForm oForm;
        public SAPbobsCOM.Company oCompany = (SAPbobsCOM.Company)Application.SBO_Application.Company.GetDICompany();
        public SAPbobsCOM.Recordset oRecordSet;

        
    }
}
