
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace Provvigioni
{

    [FormAttribute("Provvigioni.listaProvvigioni_b1f", "listaProvvigioni.b1f")]
    class listaProvvigioni_b1f : UserFormBase
    {
        public listaProvvigioni_b1f()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
            this.ActivateAfter += new ActivateAfterHandler(this.Form_ActivateAfter);

        }
        private void OnCustomInitialize()
        {
            this.oApp = (SAPbouiCOM.Application)Application.SBO_Application;
            //this.oForm = (SAPbouiCOM.Form)oApp.Forms.ActiveForm;this.UIAPIRawForm.
            this.oForm = this.UIAPIRawForm;
            this.selectAgente = ((SAPbouiCOM.ComboBox)(this.GetItem("selectAg").Specific));
            this.btnCerca = ((SAPbouiCOM.Button)(this.GetItem("Item_2").Specific));
            this.btnCerca.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.btnCerca_ClickBefore);
            this.btnAggiorna = ((SAPbouiCOM.Button)(this.GetItem("Item_0").Specific));
            this.btnAggiorna.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.btnAggiorna_ClickBefore);
            this.listaProvvGrid = ((SAPbouiCOM.Grid)(this.GetItem("Item_1").Specific));
            this.oRecordSet = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.oRecordSetUp = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.oRecordSetAgList = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.oRecordSetProvvList = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            this.btnElimina = ((SAPbouiCOM.Button)(this.GetItem("Item_3").Specific));
            this.btnElimina.PressedAfter += new SAPbouiCOM._IButtonEvents_PressedAfterEventHandler(this.btnElimina_PressedAfter);
            this.btnElimina.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.btnElimina_ClickBefore);
            this.btnElimina.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button0_ClickAfter);

            //POPOLO DDL AGENTI
            selectAgente.ValidValues.Add("Tutti", "Tutti");
            String qAgenti = "SELECT * FROM \"OSLP\" order by \"SlpName\" ASC ";
            oRecordSetAgList.DoQuery(qAgenti);
            while (!oRecordSetAgList.EoF)
            {
                selectAgente.ValidValues.Add(oRecordSetAgList.Fields.Item("SlpCode").Value.ToString(), oRecordSetAgList.Fields.Item("SlpName").Value.ToString());
                oRecordSetAgList.MoveNext();
            }
            selectAgente.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
            selectAgente.Select("Tutti", SAPbouiCOM.BoSearchKey.psk_ByValue);
        }

        //BOTTONE CERCA
        private void btnCerca_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            oForm = (SAPbouiCOM.Form)oApp.Forms.ActiveForm;
            String SlpCode = this.selectAgente.Value.Trim();

           
            //FILTRO AGENTE
            if (!SlpCode.Equals("Tutti"))
            {
                filtro_agente = " AND fcanag.\"U_SlpCode\"= '" + SlpCode + "' ";
            }
            else
            {
                filtro_agente = "";
            }

            aggiornaGriglia();

            
            
        }
        //BOTTONE ELIMINA
        private void btnElimina_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            SAPbouiCOM.DataTable riga = listaProvvGrid.DataTable;
            
            this.UIAPIRawForm.Freeze(true);
            
            int numRows = listaProvvGrid.Rows.Count;

            string[] righeIDS = new string[numRows];

            for (int i = 0; i < numRows; i++)
            {
                String idRiga = listaProvvGrid.DataTable.GetValue("ID", i).ToString();
                String select = listaProvvGrid.DataTable.GetValue("Selezione", i).ToString();
                if (select.Equals("Y"))
                {
                    righeIDS[i] = idRiga;
                }

            }
            eliminaProvvigione_b1f activeForm = new eliminaProvvigione_b1f(righeIDS);
            activeForm.Show();

            oForm = (SAPbouiCOM.Form)oApp.Forms.ActiveForm;
            String SlpCode = this.selectAgente.Value.Trim();


            //FILTRO AGENTE
            if (!SlpCode.Equals("Tutti"))
            {
                filtro_agente = " AND \"U_SlpCode\"= '" + SlpCode + "' ";
            }
            else
            {
                filtro_agente = "";
            }
            this.UIAPIRawForm.Freeze(false);
            

        }
        //AGGIORNO DATI FORM
        private void Button0_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            aggiornaGriglia();

        }
        //BOTTONE AGGIORNA
        private void btnAggiorna_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;

            SAPbouiCOM.DataTable riga = this.listaProvvGrid.DataTable;

            int numRows = listaProvvGrid.Rows.Count;

            for (int i = 0; i < numRows;i++ )
            {
                String idRiga = this.listaProvvGrid.DataTable.GetValue("ID", i).ToString();
                String select = this.listaProvvGrid.DataTable.GetValue("Selezione", i).ToString();
                String percent = this.listaProvvGrid.DataTable.GetValue("% Provv.", i).ToString();
                //String incrPrezzo = this.listaProvvGrid.DataTable.GetValue("*% Incr. Prezzo", i).ToString();
                //String decrPrezzo = this.listaProvvGrid.DataTable.GetValue("*% Decr. Prezzo", i).ToString();
                if(select.Equals("Y")){
                    //String qUp = "UPDATE \"@FC_ANAG\" SET \"U_percent\" = \'" + percent + "\',\"U_incrPrezzo\" = \'" + incrPrezzo + "\',\"U_decrPrezzo\" = \'" + decrPrezzo + "\' WHERE \"Code\" = \'" + idRiga + "\' ";
                    String qUp = "UPDATE \"@FC_ANAG\" SET \"U_percent\" = \'" + percent + "\' WHERE \"Code\" = \'" + idRiga + "\' ";
                   oRecordSetUp.DoQuery(qUp);
                   //TODO CONTROLLO SE AGGIORNATO
                   //Application.SBO_Application.MessageBox(qUp, 1, "Ok", "", "");
                   
                }
               
            }


            this.oForm = (SAPbouiCOM.IForm)oApp.Forms.ActiveForm;
            String SlpCode = this.selectAgente.Value.Trim();


            //FILTRO AGENTE
            if (!SlpCode.Equals("Tutti"))
            {
                filtro_agente = " AND fcanag.\"U_SlpCode\"= '" + SlpCode + "' ";
            }
            else
            {
                filtro_agente = "";
            }

            aggiornaGriglia();

        }
        //AGGIORNA GRID
        private void aggiornaGriglia()
        {
            this.oForm = (SAPbouiCOM.IForm)oApp.Forms.ActiveForm;
            this.UIAPIRawForm.Freeze(true);
            //queryFiltro = "SELECT fcanag.\"Code\" as \"ID\",oslp.\"SlpName\" as \"Agente\",fcanag.\"U_dtINI\" as \"Inizio\",fcanag.\"U_dtEND\" as \"Fine\",fcanag.\"U_CardCode\" as \"CardCode\",fcanag.\"U_ItemCode\" as \"ItemCode\",'N' as \"Selezione\",fcanag.\"U_percent\" as \"% Provv.\",fcanag.\"U_incrPrezzo\" as \"*% Incr. Prezzo\",fcanag.\"U_decrPrezzo\" as \"*% Decr. Prezzo\",fcanag.\"U_decurt\" as \"**Decurtazione\" FROM \"@FC_ANAG\" fcanag ,\"OSLP\" oslp WHERE fcanag.\"Code\">0 AND oslp.\"SlpCode\" = fcanag.\"U_SlpCode\" " + filtro_agente + " ORDER BY \"U_dtEND\" DESC ";

            queryFiltro  = " SELECT ";
            queryFiltro += " fcanag.\"Code\" as \"ID\", ";
            queryFiltro += " fcanag.\"U_NomeRegola\" as \"Regola\", ";
            queryFiltro += " oslp.\"SlpName\" as \"Agente\", ";
            queryFiltro += " fcanag.\"U_dtINI\" as \"Inizio\", ";
            queryFiltro += " fcanag.\"U_dtEND\" as \"Fine\", ";
            queryFiltro += " 'N' as \"Selezione\", ";
            queryFiltro += " fcanag.\"U_percent\" as \"% Provv.\", ";
            queryFiltro += " fcanag.\"U_CardCode\" as \"CardCode\", ";
            queryFiltro += " fcanag.\"U_ItemCode\" as \"ItemCode\", ";
            //queryFiltro += " oitb.\"ItmsGrpNam\" as \"Gruppo Articoli\", ";
            queryFiltro += " fcanag.\"U_GrpOmog\" as \"Gruppo Omog.\", ";
            queryFiltro += " fcanag.\"U_TipoArt\" as \"Tipo Art.\", ";
            queryFiltro += " oond.\"IndName\" as \"Settore\", ";
            queryFiltro += " fcanag.\"U_BUnit\" as \"B.Unit\" ";
            //queryFiltro += " fcanag.\"U_incrPrezzo\" as \"*% Incr. Prezzo\", ";
            //queryFiltro += " fcanag.\"U_decrPrezzo\" as \"*% Decr. Prezzo\", ";
            //queryFiltro += " fcanag.\"U_decurt\" as \"**Decurtazione\" ";
            queryFiltro += " FROM ";
            queryFiltro += " \"@FC_ANAG\" fcanag  ";
            queryFiltro += " INNER JOIN \"OSLP\" oslp ON oslp.\"SlpCode\" = fcanag.\"U_SlpCode\" ";
            queryFiltro += " LEFT JOIN \"OITB\" oitb ON oitb.\"ItmsGrpCod\" = fcanag.\"U_GrpCode\" ";
            queryFiltro += " LEFT JOIN \"OOND\" oond ON CAST(oond.\"IndCode\" as VARCHAR) = fcanag.\"U_SettoreCode\" ";
            queryFiltro += " WHERE ";
            queryFiltro += " fcanag.\"Code\">0 ";
            queryFiltro += " " + filtro_agente + " ORDER BY \"U_dtEND\" DESC ";


            //Application.SBO_Application.MessageBox(queryFiltro, 1, "Ok", "", "");
            //AGGIUNGO ID LISTA AL FORM
            try
            {
                oForm.DataSources.DataTables.Add("listaFil");
            }
            catch (System.Runtime.InteropServices.COMException e)
            {
                //Application.SBO_Application.MessageBox(e.ToString(), 1, "Ok", "", "");
            }
            oForm.DataSources.DataTables.Item("listaFil").ExecuteQuery(queryFiltro);
            //oForm.DataSources.Columns.Add("#", SAPbouiCOM.BoFieldsType.ft_Text, 20);
            listaProvvGrid.DataTable = oForm.DataSources.DataTables.Item("listaFil");

            //assegno elemtni grid
            elementiForm();

            //RESIZE
            listaProvvGrid.AutoResizeColumns();
            this.UIAPIRawForm.Freeze(false);
        }
        //COLONNE GRID
        private void elementiForm()
        {
            //DISABLE COLONNE
            listaProvvGrid.Columns.Item(0).Editable = false;
            listaProvvGrid.Columns.Item(1).Editable = false;
            listaProvvGrid.Columns.Item(2).Editable = false;
            listaProvvGrid.Columns.Item(3).Editable = false;
            listaProvvGrid.Columns.Item(4).Editable = false;
            listaProvvGrid.Columns.Item(7).Editable = false;
            listaProvvGrid.Columns.Item(8).Editable = false;
            listaProvvGrid.Columns.Item(9).Editable = false;
            listaProvvGrid.Columns.Item(10).Editable = false;
            listaProvvGrid.Columns.Item(11).Editable = false;
            listaProvvGrid.Columns.Item(12).Editable = false;

            //COLONNE CUSTOM
            listaProvvGrid.Columns.Item(5).TitleObject.Sortable = true;
            listaProvvGrid.Columns.Item(5).Type = SAPbouiCOM.BoGridColumnType.gct_CheckBox;
        }

        //DICHIARAZIZONI SISTEMA
        public SAPbouiCOM.Application oApp;
        public SAPbouiCOM.IForm oForm;
        public SAPbobsCOM.Company oCompany = Program.oCompany;
        public SAPbobsCOM.Recordset oRecordSet;
        public SAPbobsCOM.Recordset oRecordSetAgList;
        public SAPbobsCOM.Recordset oRecordSetProvvList;
        public SAPbobsCOM.Recordset oRecordSetUp;

        //LAYOUT
        private SAPbouiCOM.ComboBox selectAgente;
        private SAPbouiCOM.Button btnCerca;
        private SAPbouiCOM.Button btnAggiorna;
        private SAPbouiCOM.Button btnElimina;
        private SAPbouiCOM.Grid listaProvvGrid;

        //FILTRI
        String queryFiltro = "";
        String filtro_agente = "";

        private void btnElimina_PressedAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            aggiornaGriglia();

        }

        private void Form_ActivateAfter(SAPbouiCOM.SBOItemEventArg pVal)
        {
            this.UIAPIRawForm.Freeze(true);
            for (int e = 0; e < 5;e++ )
            {
                if(e==0){
                    aggiornaGriglia();
                }
            }
            this.UIAPIRawForm.Freeze(false);

        }



        
        

    }
}
